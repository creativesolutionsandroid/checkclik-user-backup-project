package com.cs.checkclickuser.Activites;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickuser.Adapter.FillterSubAdapter;
import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.CityListResponce;
import com.cs.checkclickuser.Models.FilterSubResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdressActivity   extends AppCompatActivity {

    EditText fullname, streetname, apartment, zip, phonenumber;
    Spinner countrylist, citylist, selettype;
    TextView countryview, cityview, selettypeview;
    Button addadress;
    String TAG = "TAG";
    ImageView back_btn;
    private TextInputLayout inputLayoutfullname, inputLayoutstreetname, inputLayoutapartment, inputLayoutzip, inputLayoutphonenumber;
    private String strfullname, strstreetname, strapartment, strzip, strphonenumber;

    ArrayList<String> countryArray = new ArrayList<>();
    ArrayList<String> cityArray = new ArrayList<>();
    ArrayList<String> typeArray = new ArrayList<>();


    private ArrayAdapter<String> countryAdapter, cityAdapter, addresstypedapter;
    int countryId = 0, citylId = 0;
    int countySelected = 0, citySelected = 0;

    AdressCountryResponce.Data data ;
    private List<CityListResponce> filtercitylist =new ArrayList<>();
//    private ArrayList<AdressCountryResponce.Data> data = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address_activity);

        fullname = (EditText) findViewById(R.id.fullname);
        streetname = (EditText) findViewById(R.id.streetname);
        apartment = (EditText) findViewById(R.id.apartmentname);
        zip = (EditText) findViewById(R.id.zip);
        phonenumber = (EditText) findViewById(R.id.phone);

        countryview = (TextView) findViewById(R.id.countytext1);
        cityview = (TextView) findViewById(R.id.citytext1);
        selettypeview = (TextView) findViewById(R.id.adresstype1);

        inputLayoutfullname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutstreetname = (TextInputLayout) findViewById(R.id.layout_street);
        inputLayoutapartment = (TextInputLayout) findViewById(R.id.layout_apartmentname);
        inputLayoutzip = (TextInputLayout) findViewById(R.id.layout_zip);
        inputLayoutphonenumber = (TextInputLayout) findViewById(R.id.layout_phone);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        countrylist = (Spinner) findViewById(R.id.countrylist);
        citylist = (Spinner) findViewById(R.id.citylist);
        selettype = (Spinner) findViewById(R.id.adresstypelist);
        addadress = (Button) findViewById(R.id.addadress);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addadress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(AddAdressActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        new AddAddressApi().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(AddAdressActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getcountylistApi().execute();
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validation() {
        strfullname = fullname.getText().toString();
        strstreetname = streetname.getText().toString();
        strapartment = apartment.getText().toString();
        strzip = zip.getText().toString();
        strphonenumber = phonenumber.getText().toString();
        if (strfullname.length() == 0) {
            fullname.setError(getResources().getString(R.string.error_fullname_number));
            Constants.requestEditTextFocus(fullname, AddAdressActivity.this);
            return false;
        } else if (strstreetname.length() == 0) {
            streetname.setError(getResources().getString(R.string.error_streetname));
            Constants.requestEditTextFocus(streetname, AddAdressActivity.this);
            return false;
        } else if (strapartment.length() == 0) {
            apartment.setError(getResources().getString(R.string.error_apartment));
            Constants.requestEditTextFocus(apartment, AddAdressActivity.this);
            return false;
        } else if (strzip.length() == 0) {
            zip.setError(getResources().getString(R.string.error_zip));
            Constants.requestEditTextFocus(zip, AddAdressActivity.this);
            return false;
        } else if (strphonenumber.length() == 0) {
            phonenumber.setError(getResources().getString(R.string.error_phonenumber));
            Constants.requestEditTextFocus(phonenumber, AddAdressActivity.this);
            return false;
        }
        return true;
    }

    private class getcountylistApi extends AsyncTask<String, String, String> {


        String inputStr;
        String TAG = "TAG";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(AddAdressActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddAdressActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AdressCountryResponce> call = apiService.getcounrylist();
            call.enqueue(new Callback<AdressCountryResponce>() {
                @Override
                public void onResponse(Call<AdressCountryResponce> call, Response<AdressCountryResponce> response) {
                    if (response.isSuccessful()) {
                        data = response.body().getData();
                        setSpinner();
                    } else {
                        Toast.makeText(AddAdressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<AdressCountryResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AddAdressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddAdressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void setSpinner() {
        countryArray.add("-- Select Country --");
        for (int i = 0; i < data.getCountryList().size(); i++)
            countryArray.add(data.getCountryList().get(i).getNameEn());


        cityArray.add("-- Select City --");
        for (int i = 0; i < data.getCityList().size(); i++) {

            cityArray.clear();
            if (data.getCountryList().get(countySelected).getId()==data.getCityList().get(i).getCountryId()){
                cityArray.add(data.getCityList().get(i).getNameEn());
            }

            typeArray.add("-- Select Type --");

        }

        cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, cityArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

        };
        countryAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, countryArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }
        };

        countrylist.setAdapter(countryAdapter);
        citylist.setAdapter(cityAdapter);

        countrylist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                countryview.setText(countryArray.get(i));

                for (int j = 0; j < data.getCityList().size(); j++) {
                    filtercitylist.clear();
                    if (data.getCountryList().get(citySelected).getId() == data.getCityList().get(j).getCountryId()) {
                        CityListResponce cityArry = new CityListResponce();
                        cityArry.setCountryEn(data.getCityList().get(i).getCountryEn());
                        cityArry.setCountryAr(data.getCityList().get(i).getCountryAr());
                        cityArry.setCountryId(data.getCityList().get(i).getCountryId());
                        cityArry.setId(data.getCityList().get(i).getId());
                        cityArry.setLatitude(data.getCityList().get(i).getLatitude());
                        cityArry.setLongitude(data.getCityList().get(i).getLongitude());
                        cityArry.setCountryEn(data.getCityList().get(i).getCountryEn());
                        cityArry.setCountryAr(data.getCityList().get(i).getCountryAr());
                        filtercitylist.add(cityArry);
                    }
                }

                citylist.setAdapter(cityAdapter);
                cityAdapter.notifyDataSetChanged();


                countySelected = i;
                    citylId = data.getCountryList().get(i).getId();
//                    typeArray.clear();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citylist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityview.setText(cityArray.get(i));
                    countySelected = i;
                    citylId = data.getCityList().get(i).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", 1);
            parentObj.put("FullName", strfullname);
            parentObj.put("Address1", strstreetname);
            parentObj.put("Address2", strapartment);
            parentObj.put("CountryId", countryId);
            parentObj.put("CityId", citylId);
            parentObj.put("Zipcode", strzip);
            parentObj.put("PhoneNumber", strphonenumber);
            parentObj.put("AddressType", 17);
            parentObj.put("CreatedBy", 17);
            parentObj.put("ModifiedBy", 17);
            parentObj.put("DeletedBy", 17);
            parentObj.put("FlagId", 17);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

}
