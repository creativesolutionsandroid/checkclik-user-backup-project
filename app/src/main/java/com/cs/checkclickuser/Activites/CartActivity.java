package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.CartDetailsListAdapter;
import com.cs.checkclickuser.Adapter.OrderDetailsListAdapter;
import com.cs.checkclickuser.Adapter.ProductItemGridAdapter;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    private CartDetailsListAdapter mcartAdapter;
    TextView totalprice,deliverycharges,Vat,subtotal,discount,nettotal;
    private CartResponce.Data cartArryLists ;
    int storePos =0;
    private static String TAG = "TAG";
    ListView iteamlist;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String  userId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        new GetCartApi().execute();


        totalprice =(TextView)findViewById(R.id.totalprice);
        deliverycharges =(TextView)findViewById(R.id.deliverycharges);
        Vat =(TextView)findViewById(R.id.Vat);
        subtotal =(TextView)findViewById(R.id.subtotal);
        discount =(TextView)findViewById(R.id.discount);
        nettotal =(TextView)findViewById(R.id.nettotal);
        iteamlist =(ListView)findViewById(R.id.iteamlist);
    }
    private class GetCartApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(CartActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CartResponce> call = apiService.getcart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CartResponce>() {
                @Override
                public void onResponse(Call<CartResponce> call, Response<CartResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            cartArryLists = response.body().getData();
                            Log.d(TAG, "cartArryLists: "+cartArryLists.getCartList().size());

                            mcartAdapter = new CartDetailsListAdapter(CartActivity.this,cartArryLists);
                            iteamlist.setAdapter(mcartAdapter);

                            totalprice.setText(""+cartArryLists.getCartList().get(storePos).getTotalItemPrice());
                            Vat.setText(""+ cartArryLists.getCartList().get(storePos).getVatAmountPerItem());
                            subtotal.setText(""+ cartArryLists.getCartList().get(storePos).getTotalItemPrice());
                            discount.setText(""+ cartArryLists.getCartList().get(storePos).getMaxDiscountAmount());
                            nettotal.setText(""+ cartArryLists.getCartList().get(storePos).getTotalItemPrice());
                            Constants.closeLoadingDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CartActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CartActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(CartActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CartActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId",  17);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }


}
