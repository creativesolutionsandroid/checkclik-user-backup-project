package com.cs.checkclickuser.Activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cs.checkclickuser.R;

public class ChangeLanguage extends AppCompatActivity {

    Button arabic,english,done;
    TextView back_btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_language);

        arabic=(Button)findViewById(R.id.Arabic);
        english=(Button)findViewById(R.id.English);
        done=(Button)findViewById(R.id.done);
        back_btn=(TextView)findViewById(R.id.back_btn);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
