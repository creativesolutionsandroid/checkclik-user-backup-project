package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.ChangeEmailRespomce;
import com.cs.checkclickuser.Models.ChangePasswordResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    TextView tapbarname,back_btn;
    LinearLayout changename,changeemail,changephone;
    private static final String TAG = "TAG";
    Button changedone, emaildone ;
    EditText firstnameed, lastnameed,email;
    String strFirstname,strLastname,stremail;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname,inputLayoutemail;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String  userId,fistname,lastname ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profileactivity);
        tapbarname = (TextView) findViewById(R.id.tapbarname);
        changename= (LinearLayout)findViewById(R.id.changenamelayout);
        changeemail=(LinearLayout)findViewById(R.id.changeemaillayout);
        changephone=(LinearLayout)findViewById(R.id.changephonellayout);
        changedone=(Button)findViewById(R.id.changedone);
        firstnameed =(EditText)findViewById(R.id.fullname);
        lastnameed =(EditText)findViewById(R.id.lastname);
        email =(EditText)findViewById(R.id.signin_input_email);
        back_btn=(TextView)findViewById(R.id.back_btn);
        emaildone=(Button)findViewById(R.id.emaildone);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        fistname=userPrefs.getString("name" ,null);
        lastname=userPrefs.getString("lastname" ,null);

        inputLayoutfirstname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutlastname = (TextInputLayout) findViewById(R.id.layout_last);
        inputLayoutemail = (TextInputLayout) findViewById(R.id.layout_email_input);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        if (intent.getStringExtra("changename").equals("Change Name")) {

            tapbarname.setText("Change Name");
            changename.setVisibility(View.VISIBLE);
            firstnameed.setText(fistname);
            lastnameed.setText(lastname);
        }
      else if (intent.getStringExtra("changename").equals("Change Email")){
            tapbarname.setText("Change Email");
            changeemail.setVisibility(View.VISIBLE);
        }
        changedone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
      emaildone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameValidations()){
                    new ChangeNameApi().execute();
                }
            }
        });
    }

    private boolean nameValidations(){
        strFirstname = firstnameed.getText().toString();
        strLastname = lastnameed.getText().toString();
        stremail=email.getText().toString();

        if(strFirstname.length() == 0){
            inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            Constants.requestEditTextFocus(firstnameed, EditProfileActivity.this);
            return false;
        }
        if(strLastname.length() == 0){
            inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            Constants.requestEditTextFocus(lastnameed, EditProfileActivity.this);
            return false;
        }
        if (stremail.length()==0){
            inputLayoutemail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(email, EditProfileActivity.this);
            return false;
        }
        return true;
    }
    private class ChangeNameApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(EditProfileActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparenameJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeEmailRespomce> call = apiService.getchangeemail(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeEmailRespomce>() {
                @Override
                public void onResponse(Call<ChangeEmailRespomce> call, Response<ChangeEmailRespomce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangeEmailRespomce resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
//                                String userId = resetPasswordResponse.getData().getMobileNo();
//                                userPrefEditor.putString("userId", userId);
//                                userPrefEditor.commit();
                                finish();
                                Toast.makeText(EditProfileActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                            }
                            else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }

                }

                @Override
                public void onFailure(Call<ChangeEmailRespomce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.showLoadingDialog(EditProfileActivity.this);

                }
            });
            return null;
        }
    }
    private String preparenameJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("EmailId", stremail) ;
            parentObj.put("Id", userId) ;
            parentObj.put("FlagId", 2) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}
