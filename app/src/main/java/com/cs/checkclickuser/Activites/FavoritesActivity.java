package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.FavProductsAdapter;
import com.cs.checkclickuser.Adapter.FavServicesAdapter;
import com.cs.checkclickuser.Adapter.FavStoreAdapter;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesActivity extends AppCompatActivity {

    TextView stores,product,services;
    private ImageView back_btn;
    RecyclerView listview;
    View productline,storeline,serviceline;
    private FavStoreAdapter mFavstoreAdapter;
    private FavProductsAdapter mFavproductAdapter;
    private FavServicesAdapter mFavserviceAdapter;
    String TAG = "TAG";
    String  userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    private ArrayList<FavStoresResponce.StoreList> storeArrayList = new ArrayList<>();
    private ArrayList<FavStoresResponce.ServiceList> serviceArrayList = new ArrayList<>();
    private ArrayList<FavStoresResponce.ProductList> productArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_fragmnet);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);


        stores=(TextView)findViewById(R.id.stores);
        product=(TextView)findViewById(R.id.product);
        services=(TextView)findViewById(R.id.services);
        listview=(RecyclerView)findViewById(R.id.list_item) ;
        back_btn=(ImageView)findViewById(R.id.back_btn);
        storeline=(View)findViewById(R.id.storeline);
        productline=(View)findViewById(R.id.productline);
        serviceline=(View)findViewById(R.id.serviline);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        stores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storeArrayList != null) {
                    mFavstoreAdapter = new FavStoreAdapter(FavoritesActivity.this, storeArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FavoritesActivity.this);
                    listview.setLayoutManager(new GridLayoutManager(FavoritesActivity.this,1 ));
                    listview.setAdapter(mFavstoreAdapter);
                    Log.d(TAG, "array1: " + storeArrayList.size());
                    storeline.setVisibility(View.VISIBLE);
                    serviceline.setVisibility(View.INVISIBLE);
                    productline.setVisibility(View.INVISIBLE);
                }
            }
        });

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (productArrayList!=null) {
                    mFavproductAdapter = new FavProductsAdapter(FavoritesActivity.this, productArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FavoritesActivity.this);
                    listview.setLayoutManager(new GridLayoutManager(FavoritesActivity.this,1 ));
                    listview.setAdapter(mFavproductAdapter);
                    productline.setVisibility(View.VISIBLE);
                    storeline.setVisibility(View.INVISIBLE);
                    serviceline.setVisibility(View.INVISIBLE);
                }
            }
        });


        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (storeArrayList!=null) {
                    mFavserviceAdapter = new FavServicesAdapter(FavoritesActivity.this, serviceArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FavoritesActivity.this);
                    listview.setLayoutManager(new GridLayoutManager(FavoritesActivity.this,1 ));
                    listview.setAdapter(mFavserviceAdapter);
                    serviceline.setVisibility(View.VISIBLE);
                    storeline.setVisibility(View.INVISIBLE);
                    productline.setVisibility(View.INVISIBLE);
                }
            }
        });


        new GetstoreApi().execute(userId);
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(FavoritesActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(FavoritesActivity.this);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<FavStoresResponce> call = apiService.getfavorites(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<FavStoresResponce>() {
                @Override
                public void onResponse(Call<FavStoresResponce> call, Response<FavStoresResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);


                    if (response.isSuccessful()) {
                        FavStoresResponce stores = response.body();

                        if (stores.getStatus()) {
                            storeArrayList = stores.getData().getStoreList();
                            serviceArrayList  = stores.getData().getServiceList();
                            productArrayList  = stores.getData().getProductList();
                        }
                        Log.d(TAG, "arry list size 1" + storeArrayList.size());
                    }

                    mFavstoreAdapter = new FavStoreAdapter(FavoritesActivity.this, storeArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FavoritesActivity.this);
                    listview.setLayoutManager(new GridLayoutManager(FavoritesActivity.this,1 ));
                    listview.setAdapter(mFavstoreAdapter);

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<FavStoresResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(FavoritesActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(FavoritesActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FavoritesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();

                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Type", 1) ;
            parentObj.put("StatusId", 0) ;
            parentObj.put("UserId", 17) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

}
