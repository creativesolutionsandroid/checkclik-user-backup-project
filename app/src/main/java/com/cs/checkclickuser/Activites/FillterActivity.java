package com.cs.checkclickuser.Activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.checkclickuser.Adapter.FillterMainAdapter;
import com.cs.checkclickuser.Adapter.FillterSubAdapter;
import com.cs.checkclickuser.Adapter.OrderDetailsListAdapter;
import com.cs.checkclickuser.Models.FilterSubResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class FillterActivity extends AppCompatActivity {
    private static final String TAG = "TAG";
    ListView maincategory,subcategory;

    private ArrayList<ProductlistResponce.FilterMainCategory> mainCategories =new ArrayList<>();
    private ArrayList<ProductlistResponce.FilterSubCategoryCat> subCategories =new ArrayList<>();
    private ArrayList<FilterSubResponce> filteredSubCategories =new ArrayList<>();
    ImageView backbtn;
    Button apply_btn,clear_btn;
    private FillterMainAdapter mMainAdapter;
    private FillterSubAdapter mSubAdapter;
    private int mainFilterPosition = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fillter_activity);
        mainCategories = (ArrayList<ProductlistResponce.FilterMainCategory>) getIntent().getSerializableExtra("Fillterarry");
        subCategories = (ArrayList<ProductlistResponce.FilterSubCategoryCat>) getIntent().getSerializableExtra("Fillterarry1");

        backbtn=(ImageView) findViewById(R.id.back_btn);
        maincategory=(ListView)findViewById(R.id.maincategory);
        subcategory=(ListView)findViewById(R.id.subcategory);
        clear_btn=(Button)findViewById(R.id.clear_btn);
        apply_btn=(Button)findViewById(R.id.apply_btn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mMainAdapter = new FillterMainAdapter(FillterActivity.this,mainCategories);
        maincategory.setAdapter(mMainAdapter);

        fetchSubCategories();


        maincategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected: "+position);
                mainFilterPosition = position;
                fetchSubCategories();
            }
        });

        clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });
        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




            }
        });
    }

    private void fetchSubCategories(){
        filteredSubCategories.clear();
        for (int i = 0; i < subCategories.size(); i++) {
            if(mainCategories.get(mainFilterPosition).getCategoryId() == subCategories.get(i).getCategoryId()) {
                FilterSubResponce subResponce=new FilterSubResponce();
                subResponce.setCategoryId(subCategories.get(i).getCategoryId());
                subResponce.setName(subCategories.get(i).getName());
                subResponce.setType(subCategories.get(i).getType());
                subResponce.setTypeId(subCategories.get(i).getTypeId());
                subResponce.setValue(subCategories.get(i).getValue());
                filteredSubCategories.add(subResponce);
            }
        }

        Log.d(TAG, "subcatsize"+filteredSubCategories.size());
        mSubAdapter = new FillterSubAdapter(FillterActivity.this,filteredSubCategories);
        subcategory.setAdapter(mSubAdapter);
        mSubAdapter.notifyDataSetChanged();

    }

}
