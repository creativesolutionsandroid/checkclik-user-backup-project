package com.cs.checkclickuser.Activites;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.cs.checkclickuser.Fragements.HomeFragment;
import com.cs.checkclickuser.Fragements.OrdersFragment;
import com.cs.checkclickuser.Fragements.Product_fragment;
import com.cs.checkclickuser.Fragements.ProfileFragment;
import com.cs.checkclickuser.R;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        HomeFragment homeScreenFragment = new HomeFragment();
        fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
        fragmentTransaction.commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_product:
                    Fragment product_fragment = new Product_fragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout,product_fragment).commit();
                    return true;
                case R.id.navigation_service:
//

                case R.id.navigation_home:

                    Fragment homeFragment = new HomeFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout,homeFragment).commit();
                    return true;
                case R.id.navigation_order:
                    Fragment ordersFragment = new OrdersFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout,ordersFragment).commit();
                    return true;
                case R.id.navigation_profile:
                    Fragment profirfrgment = new ProfileFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout,profirfrgment).commit();
                    return true;
            }
            return false;
        }
    };

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }


}
