package com.cs.checkclickuser.Activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.FavStoreAdapter;
import com.cs.checkclickuser.Adapter.ManageAdressAdapter;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageAdreesActivity extends AppCompatActivity {

    ImageView back_btn;
    RecyclerView adresslistview;
    RelativeLayout addnew;
    String TAG = "TAG";
    private ArrayList<ManageAdressResponce.Data> addressArrayList = new ArrayList<>();
    private ManageAdressAdapter mAddressAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.managae_adress_activity);

        back_btn=(ImageView)findViewById(R.id.back_btn);
        addnew=(RelativeLayout) findViewById(R.id.addnew);
        adresslistview=(RecyclerView)findViewById(R.id.adresslist);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(ManageAdreesActivity.this,AddAdressActivity.class);
               startActivity(intent);
            }
        });

        new GetstoreApi().execute();
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ManageAdreesActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ManageAdreesActivity.this);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ManageAdressResponce> call = apiService.getaddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ManageAdressResponce>() {
                @Override
                public void onResponse(Call<ManageAdressResponce> call, Response<ManageAdressResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);


                    if (response.isSuccessful()) {
                        ManageAdressResponce stores = response.body();

                        if (stores.getStatus()) {
                            addressArrayList = stores.getData();

                        }
                        Log.d(TAG, "arry list size 1" + addressArrayList.size());
                    }

                    if (addressArrayList != null){
                        mAddressAdapter = new ManageAdressAdapter(ManageAdreesActivity.this, addressArrayList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ManageAdreesActivity.this);
                        adresslistview.setLayoutManager(new GridLayoutManager(ManageAdreesActivity.this,1 ));
                        adresslistview.setAdapter(mAddressAdapter);

                        Constants.closeLoadingDialog();
                    }

                }

                @Override
                public void onFailure(Call<ManageAdressResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ManageAdreesActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ManageAdreesActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ManageAdreesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FlagId", 1) ;
            parentObj.put("UserId", 13) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}
