package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.checkclickuser.R;

public class MyAccountActivity extends AppCompatActivity {

    TextView firstname,lastname,email,mobilenumber,password;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    ImageView back_btn;
    RelativeLayout firstnamelayout,email_layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myaccount_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        firstname=(TextView)findViewById(R.id.firstname);
        lastname=(TextView)findViewById(R.id.lastname);
        email=(TextView)findViewById(R.id.email);
        mobilenumber=(TextView)findViewById(R.id.mobile);
        password=(TextView)findViewById(R.id.password);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        firstnamelayout=(RelativeLayout)findViewById(R.id.firstnamelayout);
        email_layout=(RelativeLayout)findViewById(R.id.email_layout);




        firstname.setText(userPrefs.getString("name",null));
        lastname.setText(userPrefs.getString("lastname",null));
        email.setText(userPrefs.getString("email",null));
        mobilenumber.setText(userPrefs.getString("mobile",null));

        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccountActivity.this, ChangeaPasswordActivity.class);
                startActivity(i);
            }
        });
        firstnamelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Name");
                startActivity(intent);
            }
        });

        lastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Name");
                startActivity(intent);
            }
        });

        email_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Email");
                startActivity(intent);
            }
        });

        mobilenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Mobile Number");
                startActivity(intent);
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
