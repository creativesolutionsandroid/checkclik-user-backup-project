package com.cs.checkclickuser.Activites;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.ManageAdressAdapter;
import com.cs.checkclickuser.Adapter.OrderDetailsListAdapter;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class OrderDetailsActivity extends AppCompatActivity {


    TextView oredernumber, storename, placedon, seheduled, oredertotal, paymentinfo, deliveryadress, total, deliverycharges, vat, subtotal, discount, nettotal;
    EditText remarks;
    ImageView storeimage,backbtn,phone;
    Button cancal_Btn;
    int storePos,productPos,historypos;

    String Language = "En";
    ListView iteamlist;
    public LayoutInflater inflater;
    public static final String TAG = "TAG";
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    private ArrayList<OrderPendingResponce.Data> processingArrayList = new ArrayList<>();
    private ArrayList<OrderPendingResponce.Data> histoyArrayList = new ArrayList<>();
    public OrderDetailsListAdapter mlistadapter;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    AlertDialog customDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details_activity);
        this.inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        oredernumber = (TextView) findViewById(R.id.ordernumer);
        storename = (TextView) findViewById(R.id.storename);
        placedon = (TextView) findViewById(R.id.placeon);
        seheduled = (TextView) findViewById(R.id.seheduled);
        oredertotal = (TextView) findViewById(R.id.ordertotal);
        paymentinfo = (TextView) findViewById(R.id.paymenttype);
        deliveryadress = (TextView) findViewById(R.id.adress);
        total = (TextView) findViewById(R.id.totalprice);
        deliverycharges = (TextView) findViewById(R.id.deliverycharges);
        vat = (TextView) findViewById(R.id.Vat);
        subtotal = (TextView) findViewById(R.id.subtotal);
        discount = (TextView) findViewById(R.id.discount);
        nettotal = (TextView) findViewById(R.id.nettotal);
        remarks = (EditText) findViewById(R.id.remarkedittext);
        storeimage = (ImageView) findViewById(R.id.storeimage);
        iteamlist = (ListView) findViewById(R.id.iteamlist);
        backbtn=(ImageView)findViewById(R.id.back_btn);
        phone=(ImageView)findViewById(R.id.phone);
        cancal_Btn=(Button) findViewById(R.id.cancelbtn);


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+"09550588834"));
                        if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                }
            }
        });
        cancal_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new  GetcancelApi().execute();
            }
        });



        pendingArrayList = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("Order");
        storePos = getIntent().getIntExtra("Orderpos", 0);

        histoyArrayList = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("history");
        historypos = getIntent().getIntExtra("historypos", 0);

        processingArrayList = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("processsing");
        productPos = getIntent().getIntExtra("processsingpos", 0);


//     oredernumber.setText("" + pendingArrayList.get(productPos).getOrderId());
        storename.setText(pendingArrayList.get(storePos).getStoreNameEn());
        oredertotal.setText("SAR " + pendingArrayList.get(storePos).getGrandTotal() + " (" + pendingArrayList.get(storePos).getItemsCount() + "Iteams )");

        if (pendingArrayList.get(storePos).getPaymentMode().equals("CC")) {

            paymentinfo.setText("Credit Card" + " (" + pendingArrayList.get(storePos).getPaymentType() + ")");
        } else {
            paymentinfo.setText("Cash on Delivery");
        }
        Glide.with(OrderDetailsActivity.this)
                .load(STORE_IMAGE_URL+pendingArrayList.get(storePos).getBranchLogoImage())
                .into(storeimage);
        Log.d(TAG, "detailsimage: "+STORE_IMAGE_URL+pendingArrayList.get(storePos).getBranchLogoImage());

//        if (pendingArrayList.get(productPos).getOrderStatus()==1||pendingArrayList.get(productPos).getOrderStatus()==2||pendingArrayList.get(productPos).getOrderStatus()==5||pendingArrayList.get(productPos).getOrderStatus()==7||pendingArrayList.get(productPos).getOrderStatus()==8||pendingArrayList.get(productPos).getOrderStatus()==9||pendingArrayList.get(position).getOrderStatus()==10||){
//            cancal_Btn.setVisibility(View.INVISIBLE);
//        }
//        else {
//            if (pendingArrayList.get(productPos).getr)
//        }

        deliveryadress.setText(pendingArrayList.get(storePos).getAddress());
        total.setText("SAR " + pendingArrayList.get(storePos).getAcceptedTotal());
        deliverycharges.setText("SAR " + pendingArrayList.get(storePos).getDeliveryFee());
        vat.setText("SAR " + pendingArrayList.get(storePos).getVAT());
        subtotal.setText("SAR " + pendingArrayList.get(storePos).getSubTotal());
        nettotal.setText("SAR " + pendingArrayList.get(storePos).getGrandTotal());

        mlistadapter = new OrderDetailsListAdapter(OrderDetailsActivity.this,pendingArrayList);
        iteamlist.setAdapter(mlistadapter);


        String date = pendingArrayList.get(storePos).getOrderDate();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        placedon.setText(""+date);

        String date1 = pendingArrayList.get(storePos).getReScheduleDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
        date1.replace(date1,"yyyy-MM-dd");
        try {
            Date datetime = format.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        seheduled.setText(""+date1);
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, perm));
    }
    private class GetcancelApi extends AsyncTask<String, String, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(OrderDetailsActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(OrderDetailsActivity.this);

        }


        @Override
    protected String doInBackground(String... params) {
        final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
        APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<OrderCancelResponce> call = apiService.getcancl(
                RequestBody.create(MediaType.parse("application/json"), inputStr));
        call.enqueue(new Callback<OrderCancelResponce>() {
            @Override
            public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                if (response.isSuccessful()) {
                    OrderCancelResponce SaveuserRatingResponce = response.body();
                    try {
                        if (SaveuserRatingResponce.getStatus()) {
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            //                          status false case
                            String failureResponse = SaveuserRatingResponce.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), OrderDetailsActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Log.d(TAG, "onResponse: "+response.errorBody());
                    Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Constants.closeLoadingDialog();
            }


            @Override
            public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.toString());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                Constants.closeLoadingDialog();

            }
        });

        return null;
    }
}
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", 13) ;
            parentObj.put("Reason", "") ;
            parentObj.put("OrderId", pendingArrayList.get(storePos).getOrderId()) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

}
