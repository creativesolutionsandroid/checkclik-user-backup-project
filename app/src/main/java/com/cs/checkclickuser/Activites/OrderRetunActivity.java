package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class OrderRetunActivity extends AppCompatActivity {


    TextView storename, iteamcount, qty, qtycont, done;
    ImageView productimage, back_btn, increase, decrease,addimage,addimage1,addimage2,addimage3;
    private ArrayList<OrderPendingResponce.Data> retunArrayList = new ArrayList<>();
    int productPos;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    private static final int CAMERA_REQUEST = 1888;
    String imageResponse;
    //    private static final int PICK_IMAGE_FROM_GALLERY = 2;
//    private static final int STORAGE_REQUEST = 5;
//    private static final String[] STORAGE_PERMS = {
//            Manifest.permission.WRITE_EXTERNAL_STORAGE
//    };
    boolean isCamera = false;
    int QTY = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_retun_activity);



        storename = (TextView) findViewById(R.id.storename);
        iteamcount = (TextView) findViewById(R.id.iteamcount);
        qty = (TextView) findViewById(R.id.qty);
        qtycont = (TextView) findViewById(R.id.qtycount);
        done = (TextView) findViewById(R.id.done);
        productimage = (ImageView) findViewById(R.id.storeimage);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        decrease=(ImageView) findViewById(R.id.decrease);
        increase=(ImageView) findViewById(R.id.increase);
        addimage=(ImageView) findViewById(R.id.addpic);
        addimage1=(ImageView) findViewById(R.id.addpic1);
        addimage2=(ImageView) findViewById(R.id.addpic2);
        addimage3=(ImageView) findViewById(R.id.addpic3);
        qtycont=(TextView)findViewById(R.id.qtycount);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });
        addimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });
        addimage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });
        addimage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });



        retunArrayList = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("Order");
        productPos = getIntent().getIntExtra("Orderpos", 0);
        storename.setText(retunArrayList.get(productPos).getItems().get(productPos).getNameEn());
        qty.setText("QTY  "+retunArrayList.get(productPos).getItems().get(productPos).getQty());
        iteamcount.setText(""+retunArrayList.get(productPos).getItems().get(productPos).getQty()+" Pics");

        Glide.with(this)
                .load(Constants.PRODUCTS_IMAGE_URL+retunArrayList.get(productPos).getItems().get(productPos).getImage())

                .into(productimage);


    }
    public void increaseInteger(View view) {
        QTY = QTY + 1;
        display(QTY);

    }public void decreaseInteger(View view) {
        QTY = QTY - 1;
        display(QTY);
    }

    private void display(int number) {
        TextView displayInteger = (TextView) findViewById(
                R.id.qtycount);
        displayInteger.setText("" + number);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderRetunActivity.this, perm));
    }
    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera=true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }
}
