package com.cs.checkclickuser.Activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cs.checkclickuser.Adapter.ProductIteamAdapter;
import com.cs.checkclickuser.Adapter.ProductShopAdapter;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ProductIteamListActivity extends AppCompatActivity {

    ProductstoreResponce.Data storeArrayList;

    int storePos;
    ImageView back_btn;
    public ProductIteamAdapter miteamlistadaper;
    TextView branchname;
    String TAG = "TAG";
    RecyclerView storeslist;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_iteam_activity);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        branchname=(TextView)findViewById(R.id.branchname);
        storeslist=(RecyclerView)findViewById(R.id.list_item);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        storeArrayList = (ProductstoreResponce.Data) getIntent().getSerializableExtra("stores1");
        storePos= getIntent().getIntExtra("pos1",0);
        branchname.setText(storeArrayList.getSubCategory().get(storePos).getNameEn());

        miteamlistadaper = new ProductIteamAdapter(ProductIteamListActivity.this, storeArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductIteamListActivity.this);
        storeslist.setLayoutManager(new GridLayoutManager(ProductIteamListActivity.this, 1));
        storeslist.setAdapter(miteamlistadaper);

    }
}

