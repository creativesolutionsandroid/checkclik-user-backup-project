package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.ProductItemGridAdapter;
import com.cs.checkclickuser.Adapter.ProductItemListAdapter;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductItemListActivity extends AppCompatActivity {

    private ProductstoreResponce.Data storeArrayList;
    private ArrayList<ProductItemResponse.ProductList> productLists = new ArrayList<>();
    int storePos;
    ImageView back_btn, gridViewIcon, listViewIcon;
    TextView branchname, categoryName;
    private static String TAG = "TAG";
    private int pageNumber = 1;
    AlertDialog customDialog;
    ProductItemGridAdapter mGridAdapter;
    ProductItemListAdapter mListAdapter;
    RecyclerView productsList;
    boolean gridSelected = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_items);

        back_btn=(ImageView)findViewById(R.id.back_btn);
        gridViewIcon=(ImageView)findViewById(R.id.gridview_icon);
        listViewIcon=(ImageView)findViewById(R.id.listview_icon);
        branchname=(TextView)findViewById(R.id.store_name);
        categoryName=(TextView)findViewById(R.id.category_name);
        productsList = (RecyclerView) findViewById(R.id.product_listview);

        storeArrayList = (ProductstoreResponce.Data) getIntent().getSerializableExtra("stores");
        storePos= getIntent().getIntExtra("pos",0);

        branchname.setText(storeArrayList.getMainCategory().get(getIntent().getIntExtra("productPos",0)).getNameEn());
        categoryName.setText(storeArrayList.getSubCategory().get(storePos).getNameEn());

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gridViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridSelected = true;
                mGridAdapter = new ProductItemGridAdapter(ProductItemListActivity.this, productLists);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivity.this);
                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivity.this, 2));
                productsList.setAdapter(mGridAdapter);
            }
        });

        listViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridSelected = false;
                mListAdapter = new ProductItemListAdapter(ProductItemListActivity.this, productLists);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivity.this);
                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivity.this, 1));
                productsList.setAdapter(mListAdapter);
            }
        });

        new GetstoreApi().execute();
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductItemListActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductItemResponse> call = apiService.getProductList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductItemResponse>() {
                @Override
                public void onResponse(Call<ProductItemResponse> call, Response<ProductItemResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductlist();

                            if(response.body().getData().getTotalrecords().get(0).getTotalrecords() > 0) {
                                mGridAdapter = new ProductItemGridAdapter(ProductItemListActivity.this, productLists);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivity.this);
                                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivity.this, 2));
                                productsList.setAdapter(mGridAdapter);
                            }
                            Constants.closeLoadingDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductItemListActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductItemResponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductItemListActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductItemListActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductItemListActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PageNumber",  pageNumber);
            parentObj.put("PageSize",  100);
//            parentObj.put("BranchSubCategoryId",  storeArrayList.getSubCategory().get(productPos).getBranchId());
            parentObj.put("BranchSubCategoryId",  1);
//            parentObj.put("SortId",  storeArrayList.getSubCategory().get(productPos).getId());
            parentObj.put("SortId",  1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}

