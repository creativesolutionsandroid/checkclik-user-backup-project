package com.cs.checkclickuser.Activites;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.ProductDealsAdapter;
import com.cs.checkclickuser.Adapter.ProductShopAdapter;
import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ProductMaincatActivity extends AppCompatActivity {
    ImageView stroeback,back_btn;
    CircleImageView storeimage;
    RecyclerView productstorelist;
    CardView dealslist;
    TextView shop,deals;
    int storePos;
    String TAG = "TAG";
    ACProgressFlower dialog;
    public ProductShopAdapter mshopadapter;
    public ProductDealsAdapter mdealsadapter;
    ProductstoreResponce.Data storeArrayList;

    private ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();
    private ArrayList<DealsResponce.Deals> storeDealsList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_main_cat);

        stroeback = (ImageView) findViewById(R.id.storebackimage);
        storeimage = (CircleImageView) findViewById(R.id.ac_storepic);
        shop =(TextView)findViewById(R.id.shop);
        deals =(TextView)findViewById(R.id.deals);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        productstorelist=(RecyclerView)findViewById(R.id.product_listview);


        deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetdealsApi().execute();
                if (!storeArrayList.equals(null)){
                    productstorelist.setVisibility(View.VISIBLE);

                }
                else {
                    productstorelist.setVisibility(View.GONE);
                }

            }
        });
        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetstoreApi().execute();
                if (!storeArrayList.equals(null)){
                    productstorelist.setVisibility(View.GONE);
                }
                else {
                    productstorelist.setVisibility(View.VISIBLE);
                }

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        productArrayList = (ArrayList<ProductlistResponce.Stores>) getIntent().getSerializableExtra("stores");
        storePos= getIntent().getIntExtra("pos",0);

        Glide.with(ProductMaincatActivity.this)
                .load(Constants.STORE_IMAGE_URL + productArrayList.get(storePos).getBranchLogoImage())
                .into(storeimage);
        Glide.with(ProductMaincatActivity.this)
                .load(STORE_IMAGE_URL + productArrayList.get(storePos).getBackgroundImage())
                .into(stroeback);
        Log.d(TAG, "imagesize: " + STORE_IMAGE_URL + productArrayList.get(storePos).getBackgroundImage());

        new GetstoreApi().execute();

    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ProductMaincatActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductMaincatActivity.this);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductstoreResponce> call = apiService.getstorelist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductstoreResponce>() {
                @Override
                public void onResponse(Call<ProductstoreResponce> call, Response<ProductstoreResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);


                    if (response.isSuccessful()) {
                        ProductstoreResponce stores = response.body();

                        if (stores.getStatus()) {
                            storeArrayList = stores.getData();
                        }
                        Log.d(TAG, "arry list size " + storeArrayList);
                    }

                    if (!storeArrayList.equals(null)) {
                        mshopadapter = new ProductShopAdapter(ProductMaincatActivity.this, storeArrayList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivity.this);
                        productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivity.this, 2));
                        productstorelist.setAdapter(mshopadapter);

                    Log.d(TAG, "array: " + storeArrayList);
                }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductstoreResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductMaincatActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductMaincatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BranchId",  productArrayList.get(storePos).getBranchId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetdealsApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetDealsJSON();
            Constants.showLoadingDialog(ProductMaincatActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<DealsResponce> call = apiService.dealsresponce(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DealsResponce>() {
                @Override
                public void onResponse(Call<DealsResponce> call, Response<DealsResponce> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        DealsResponce stores = response.body();

                        if (stores.getStatus()) {
                            storeDealsList = stores.getData().getDeals();
                        }
                        Log.d(TAG, "onResponse " + storeDealsList);

                    }

                    if (storeDealsList.size()>0) {
                        mdealsadapter = new ProductDealsAdapter(ProductMaincatActivity.this, storeDealsList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivity.this);
                        productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivity.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                        productstorelist.setAdapter(mdealsadapter);
                        Log.d(TAG, "array: " + storeDealsList);
                    }


                    Constants.closeLoadingDialog();                }

                @Override
                public void onFailure(Call<DealsResponce> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductMaincatActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductMaincatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }

    }
    private String prepareGetDealsJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BranchId",  productArrayList.get(storePos).getBranchId());
            parentObj.put("Sort",1);
            parentObj.put("PageNumber",1);
            parentObj.put("PageSize",1000);
            parentObj.put("StoreId",productArrayList.get(storePos).getStoreId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}
