package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ProductRatingActivity extends AppCompatActivity {


    final Context context = this;
    TextView workshopid, feedback, textcharactes, workshopname;
    RatingBar ratingbar;
    EditText suggestions;
    Button nothanks, submit;
    ArrayList<OrderPendingResponce.Data> productarrylist = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    AlertDialog loaderDialog;
    String str ,skuid;
    int storePos;
    float givenRating = 1;


    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ImageView backBtn,storeimage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_rating_activity);


        productarrylist = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("history");
        storePos = getIntent().getIntExtra("Orderpos", 0);

        workshopid = (TextView) findViewById(R.id.tv_shopid);
        feedback = (TextView) findViewById(R.id.tv_feedback);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        suggestions = (EditText) findViewById(R.id.ed_command);
        nothanks = (Button) findViewById(R.id.btn_nothanks);
        submit = (Button) findViewById(R.id.btn_submit);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        storeimage = (ImageView) findViewById(R.id.storeimage);

        textcharactes = (TextView) findViewById(R.id.comment_chars);

        suggestions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        suggestions.setRawInputType(InputType.TYPE_CLASS_TEXT);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Glide.with(ProductRatingActivity.this)
                .load(STORE_IMAGE_URL+productarrylist.get(storePos).getBranchLogoImage())
                .into(storeimage);
        workshopid.setText(productarrylist.get(storePos).getItems().get(storePos).getNameEn());

        suggestions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();

                textcharactes.setText((300 - (text.length())) + " characters left");

            }
        });
        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (b) {
                    if (v == 0) {
                        ratingBar.setRating(1);
                    }
                    setComments(v);
                }
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ProductRatingActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new RatingRequst().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void setComments(float rating) {
        givenRating = rating;
        if (rating == 1) {

            feedback.setText("Disappointed you ?");

        } else if (rating == 2) {

            feedback.setText("Disappointed you ?");

        } else if (rating == 3) {

            feedback.setText("What can we improve ?");
        } else if (rating == 4) {

            feedback.setText("What can we improve ?");
        } else if (rating == 5) {

            feedback.setText("What did you like the best ?");
        }

    }

    private boolean validations() {
        str = suggestions.getText().toString();


        return true;
    }

    private class RatingRequst extends AsyncTask<String, String, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(ProductRatingActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparereviwsJSON();
            Constants.showLoadingDialog(ProductRatingActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductRatingActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ReviewResponce> call = apiService.getreview(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ReviewResponce>() {
                @Override
                public void onResponse(Call<ReviewResponce> call, Response<ReviewResponce> response) {
                    if (response.isSuccessful()) {
                        ReviewResponce SaveuserRatingResponce = response.body();
                        try {
                            if (SaveuserRatingResponce.getStatus()) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                //                          status false case
                                String failureResponse = SaveuserRatingResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ProductRatingActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ProductRatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Log.d(TAG, "onResponse: "+response.errorBody());
                        Toast.makeText(ProductRatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                    }


                @Override
                public void onFailure(Call<ReviewResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductRatingActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductRatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();

                }
            });

            return null;
        }
    }
    private String preparereviwsJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", 17) ;
            parentObj.put("OrderId", getIntent().getIntExtra("OrderId",0)) ;
            parentObj.put("Type", getIntent().getIntExtra("Type",0)) ;
            parentObj.put("Rating", givenRating) ;
            parentObj.put("Comments", str) ;
            parentObj.put("StatusId", 1) ;
            parentObj.put("Ids", getIntent().getIntExtra("Ids",0)) ;
            parentObj.put("BranchId",  getIntent().getIntExtra("BranchId",0)) ;


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }


}
