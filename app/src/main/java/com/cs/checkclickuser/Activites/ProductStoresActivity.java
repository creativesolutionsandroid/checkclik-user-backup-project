package com.cs.checkclickuser.Activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.lovejjfg.shadowcircle.CircleImageView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ProductStoresActivity extends AppCompatActivity {

    ImageView backbtn, share, favourite, cart, notifaction, stroeback;
    TextView storetime,mainstore2;
    ImageView card,cash;
    TextView mainstore, storerating, reviews, storestatus, distance, deliverytime,
       minimumorder, dcharge, orderdone, businesssince, pickip, storeservice, about, textview;
    CircleImageView storeimage;
    int storePos;
    String TAG = "TAG";
    Button bt_continue;
    ACProgressFlower dialog;

     public ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_storesdetails);

        backbtn = (ImageView) findViewById(R.id.back_btn);
        share = (ImageView) findViewById(R.id.share_btn);
        favourite = (ImageView) findViewById(R.id.fav);
        cart = (ImageView) findViewById(R.id.cart);
        notifaction = (ImageView) findViewById(R.id.noti);
        stroeback = (ImageView) findViewById(R.id.storebackimage);
        storeimage = (CircleImageView) findViewById(R.id.ac_storepic);
        card=(ImageView)findViewById(R.id.card);
        cash=(ImageView)findViewById(R.id.cash);
        bt_continue=(Button)findViewById(R.id.bt_continue);

        mainstore = (TextView) findViewById(R.id.mainstore);
        mainstore2 = (TextView) findViewById(R.id.mainstore2);
        storerating = (TextView) findViewById(R.id.revies_count);
        distance = (TextView) findViewById(R.id.distance);
        reviews = (TextView) findViewById(R.id.revies);
        storestatus = (TextView) findViewById(R.id.storestatus);
        deliverytime = (TextView) findViewById(R.id.delivery_time);
        minimumorder = (TextView) findViewById(R.id.minimumorder);
        dcharge = (TextView) findViewById(R.id.dcharge);
        orderdone = (TextView) findViewById(R.id.orderdone);
        businesssince = (TextView) findViewById(R.id.business);
        pickip = (TextView) findViewById(R.id.pickip);
        storeservice = (TextView) findViewById(R.id.storeservice);
        about = (TextView) findViewById(R.id.about);
        textview = (TextView) findViewById(R.id.textview);
        storetime = (TextView) findViewById(R.id.storetime);


        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                textview.setText(productArrayList.get(0).);
            }
        });
        bt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ProductStoresActivity.this, ProductMaincatActivity.class);
                i.putExtra("stores", productArrayList);
                i.putExtra("pos",storePos);
                startActivity(i);
            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        productArrayList = (ArrayList<ProductlistResponce.Stores>) getIntent().getSerializableExtra("stores");
        storePos= getIntent().getIntExtra("pos",0);
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        mainstore.setText(productArrayList.get(storePos).getStoreEn());
        mainstore2.setText(productArrayList.get(storePos).getStoreEn());
        storerating.setText("" + productArrayList.get(storePos).getRatings() + ".0");
        distance.setText(priceFormat.format(productArrayList.get(storePos).getDistance()) + " KM");
        reviews.setText("(" + productArrayList.get(storePos).getReviews() + "Reviews )");
        storestatus.setText(productArrayList.get(storePos).getBranchStatus());
        deliverytime.setText(productArrayList.get(storePos).getDeliveryTime());
        minimumorder.setText("" + productArrayList.get(storePos).getMinimumOrderValue()+" SAR");
        dcharge.setText("" + productArrayList.get(storePos).getDeliveryCharges()+" SAR");
        orderdone.setText("" + productArrayList.get(storePos).getOrderDone());
//        pickip.setText(productArrayList.get(productPos).get);
        storeservice.setText("" + productArrayList.get(storePos).getInStoreService());
        storetime.setText("("+productArrayList.get(storePos).getShift()+")");

        if (productArrayList.get(storePos).getInStoreService().equals("false")) {

            storeservice.setText("Unavailable");
        }
        else {
            storeservice.setText("available");
        }
        if (productArrayList.get(storePos).getBranchStatus().equals("Open")){
            storestatus.setTextColor(Color.GREEN);
        }
        else {
            storestatus.setTextColor(Color.RED);
        }

        if (productArrayList.get(storePos).getIsCashAllowed().equals("true")&&(productArrayList.get(storePos).getIsCreditCardAllowed().equals("true"))) {

            card.setVisibility(View.VISIBLE);
            cash.setVisibility(View.VISIBLE);
        }

        else if (productArrayList.get(storePos).getIsCreditCardAllowed().equals("false")){
            card.setVisibility(View.GONE);

        }
        else if (productArrayList.get(storePos).getIsCashAllowed().equals("false")){
            card.setVisibility(View.GONE);

        }

        cash.setVisibility(View.GONE);
            String openDateStr = productArrayList.get(storePos).getBusinessSince();
            openDateStr = openDateStr.replace("T", " ");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");

//        String tt = array[1];
            try {
                Date time = sdf.parse(openDateStr);
                openDateStr = sdf1.format(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            businesssince.setText(openDateStr);


            Glide.with(ProductStoresActivity.this)
                    .load(Constants.STORE_IMAGE_URL + productArrayList.get(storePos).getBackgroundImage())
                    .into(storeimage);
            Glide.with(ProductStoresActivity.this)
                    .load(STORE_IMAGE_URL + productArrayList.get(storePos).getBackgroundImage())
                    .into(stroeback);
            Log.d(TAG, "imagesize: " + STORE_IMAGE_URL + productArrayList.get(storePos).getBranchLogoImage());


        }




}

