package com.cs.checkclickuser.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.ProductItemGridAdapter;
import com.cs.checkclickuser.Adapter.ProductVariantImageAdapter;
import com.cs.checkclickuser.Dialogs.ProductDetailsCartDailog;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductVariantsActivity extends Activity {

    private ImageView backBtn;
    private ViewPager imagesViewPager;
    private TextView itemName, imagesCount;
    private LinearLayout variant1Layout, variant2Layout, variant3Layout;
    private TextView variant1Title, variant2Title, variant3Title;
    private TextView variant1, variant2, variant3;
    private TextView discountedPrice, price, itemQty, stockStatus;
    private String TAG = "TAG";
    SharedPreferences userPrefs;
    private int variant1Pos, variant2Pos, variant3Pos;
    private ProductVariantImageAdapter mAdapter;

    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_variants);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        backBtn = (ImageView) findViewById(R.id.back_btn);

        imagesViewPager = (ViewPager) findViewById(R.id.images_viewpager);

        variant1Layout = (LinearLayout) findViewById(R.id.variant1_layout);
        variant2Layout = (LinearLayout) findViewById(R.id.variant2_layout);
        variant3Layout = (LinearLayout) findViewById(R.id.variant3_layout);

        itemName = (TextView) findViewById(R.id.item_name);
        imagesCount = (TextView) findViewById(R.id.images_count);
        discountedPrice = (TextView) findViewById(R.id.discounted_price);
        price = (TextView) findViewById(R.id.price);
        itemQty = (TextView) findViewById(R.id.item_qty);
        stockStatus = (TextView) findViewById(R.id.stock_status);

        variant1 = (TextView) findViewById(R.id.variant1);
        variant2 = (TextView) findViewById(R.id.variant2);
        variant3 = (TextView) findViewById(R.id.variant3);
        variant1Title = (TextView) findViewById(R.id.variant_title1);
        variant2Title = (TextView) findViewById(R.id.variant_title2);
        variant3Title = (TextView) findViewById(R.id.variant_title3);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        variant1Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductVariantsActivity.this, ProductDetailsCartDailog.class);
                intent.putExtra("stores", variantsLists);

            }
        });
        variant2Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductVariantsActivity.this, ProductDetailsCartDailog.class);
            }
        });
        variant3Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductVariantsActivity.this, ProductDetailsCartDailog.class);
            }
        });

        discountedPrice.setPaintFlags(discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        new GetVariantsApi().execute();
    }

    private class GetVariantsApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductVariantsActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductlist();
                            variantsLists = response.body().getData().getVariantslist();
                            initData();
                            Constants.closeLoadingDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductVariantsActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductVariantsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductVariantsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductVariantsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  getIntent().getIntExtra("ProductInventoryId",1));
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private void initData(){
        mAdapter = new ProductVariantImageAdapter(ProductVariantsActivity.this, productLists.get(variant1Pos).getJimages());
        imagesViewPager.setAdapter(mAdapter);

        itemName.setText(productLists.get(variant1Pos).getProductnameen());
        imagesCount.setText(productLists.get(variant1Pos).getJimages().size() + " Pics");
        discountedPrice.setText("SAR "+Constants.priceFormat1.format(productLists.get(variant1Pos).getSellingprice()));
        price.setText("SAR "+Constants.priceFormat1.format(productLists.get(variant1Pos).getPrice()));

        if(productLists.get(variant1Pos).getProductavailabilityrange() == 1) {
            stockStatus.setText(getString(R.string.str_in_stock));
        }
        else {
            stockStatus.setText(getString(R.string.str_out_of_stock));
        }

        if(productLists.get(variant1Pos).getPrice() == productLists.get(variant1Pos).getSellingprice()){
            discountedPrice.setVisibility(View.GONE);
        }

        if(variantsLists.size() == 1) {
            variant2Layout.setVisibility(View.INVISIBLE);
            variant3Layout.setVisibility(View.GONE);
            variant1Title.setText(variantsLists.get(0).getVariantnameen());
            variant1.setText(variantsLists.get(0).getJvariants().get(variant1Pos).getValueen());
        }
        else if(variantsLists.size() == 2) {
            variant3Layout.setVisibility(View.GONE);
            variant1Title.setText(variantsLists.get(0).getVariantnameen());
            variant1.setText(variantsLists.get(0).getJvariants().get(variant1Pos).getValueen());
            variant2Title.setText(variantsLists.get(1).getVariantnameen());
            variant2.setText(variantsLists.get(1).getJvariants().get(variant2Pos).getValueen());
        }
        else if(variantsLists.size() == 3) {
            variant1Title.setText(variantsLists.get(0).getVariantnameen());
            variant1.setText(variantsLists.get(0).getJvariants().get(variant1Pos).getValueen());
            variant2Title.setText(variantsLists.get(1).getVariantnameen());
            variant2.setText(variantsLists.get(1).getJvariants().get(variant2Pos).getValueen());
            variant3Title.setText(variantsLists.get(2).getVariantnameen());
            variant3.setText(variantsLists.get(2).getJvariants().get(variant3Pos).getValueen());
        }
    }
}
