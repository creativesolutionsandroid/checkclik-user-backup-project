package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.PushNotificationResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PushNotificationActivity extends AppCompatActivity {

    Switch order, customer, subscription, chatting, alert;
    Button done;
    SharedPreferences userPrefs;
    TextView back_btn;


    SharedPreferences.Editor userPrefEditor;
    String strUserid;
     Boolean st1,st2,st3,st4,st5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification);
        userPrefs = PushNotificationActivity.this.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        strUserid = userPrefs.getString("userId", null);

        order = (Switch) findViewById(R.id.orderswitch);
        customer = (Switch) findViewById(R.id.customerswitch);
        subscription = (Switch) findViewById(R.id.Subscription);
        chatting = (Switch) findViewById(R.id.Chattingswitch);
        alert = (Switch) findViewById(R.id.alertswitch);
        done =(Button)findViewById(R.id.done);
        back_btn=(TextView)findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (order.isChecked())
                    st1 = true;
                else
                    st1 = false;

                if (customer.isChecked())
                    st2 =true;
                else
                    st2 = false;
                if (subscription.isChecked())
                    st3 = true;
                else
                    st3 = false;
                if (chatting.isChecked())
                    st4 =true;
                else
                    st4 = false;
                if (alert.isChecked())
                    st5 = true;
                else
                    st5 = false;
                new Pushnotificationapi().execute();
            }


        });


    }
    private class Pushnotificationapi extends AsyncTask<String, Integer, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(PushNotificationActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparepushnotifactionJson();
            Constants.showLoadingDialog(PushNotificationActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PushNotificationResponce> call = apiService.getpushnotification(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PushNotificationResponce>() {
                @Override
                public void onResponse(Call<PushNotificationResponce> call, Response<PushNotificationResponce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        PushNotificationResponce PushNotificationResponce = response.body();
                        try {
                            if(PushNotificationResponce.getStatus()){
                                finish();
                                Toast.makeText(PushNotificationActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();

                            }
                            else {

                                String failureResponse = PushNotificationResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), PushNotificationActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
                @Override
                public void onFailure(Call<PushNotificationResponce> call, Throwable t) {
                    Log.d("TAG", "on failur: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(PushNotificationActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
    private String preparepushnotifactionJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id",34);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray jPNConfigarry = new JSONArray();
        JSONObject orderObj=new JSONObject();
        try {
            orderObj.put("Id",6 );
            orderObj.put("PNTypeId",1 );
            orderObj.put("Name","Order" );
            orderObj.put("Status",st1 );

            JSONObject customerObj=new JSONObject();

            customerObj.put("Id",7 );
            customerObj.put("PNTypeId",2 );
            customerObj.put("Name","Customer Review" );
            customerObj.put("Status",st2 );


            JSONObject SubscriptionObj=new JSONObject();

            SubscriptionObj.put("Id",8 );
            SubscriptionObj.put("PNTypeId",3 );
            SubscriptionObj.put("Name","Subscription" );
            SubscriptionObj.put("Status",st3 );

            JSONObject ChattingObj=new JSONObject();

            ChattingObj.put("Id",9 );
            ChattingObj.put("PNTypeId",4 );
            ChattingObj.put("Name","Chatting" );
            ChattingObj.put("Status",st4 );


            JSONObject AlertObj=new JSONObject();

            AlertObj.put("Id",10 );
            AlertObj.put("PNTypeId",4 );
            AlertObj.put("Name","Alert" );
            AlertObj.put("Status",st5 );

            jPNConfigarry.put(AlertObj);
            jPNConfigarry.put(ChattingObj);
            jPNConfigarry.put(SubscriptionObj);
            jPNConfigarry.put(customerObj);
            jPNConfigarry.put(orderObj);


            Log.i("TAG","pushnotifactionJson: "+parentObj.toString());
            Log.i("TAG","orderObj: "+orderObj.toString());
            Log.i("TAG","SubscriptionObj: "+SubscriptionObj.toString());
            Log.i("TAG","ChattingObj: "+ChattingObj.toString());
            Log.i("TAG","AlertObj: "+AlertObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }



}