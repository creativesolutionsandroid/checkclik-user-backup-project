package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

public class SettingActivity extends AppCompatActivity {

    TextView logiut,myaccunt,manageadress,pushnotification,contact,shareapp,tremsconditions,changelanguage;
    ImageView back_Btn;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    AlertDialog customDialog;
    String fistname;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        logiut=(TextView)findViewById(R.id.logout);
        myaccunt=(TextView)findViewById(R.id.myaccount);
        back_Btn=(ImageView)findViewById(R.id.back_btn);
        manageadress=(TextView)findViewById(R.id.manageaccount);
        changelanguage=(TextView)findViewById(R.id.chngelanguage) ;
        tremsconditions=(TextView)findViewById(R.id.treams);
        pushnotification=(TextView)findViewById(R.id.pushnotifaction);

        fistname=userPrefs.getString("name" ,null);

        logiut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });
        myaccunt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, MyAccountActivity.class);
                startActivity(i);
            }
        });
        manageadress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, ManageAdreesActivity.class);
                startActivity(i);
            }
        });
        changelanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, ChangeLanguage.class);
                startActivity(i);
            }
        });

        tremsconditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, TermsAndConditions.class);
                startActivity(i);
            }
        });

        pushnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, PushNotificationActivity.class);
                startActivity(i);
            }
        });


        back_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SettingActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


//            title.setText("CkeckClik");
//            yes.setText(getResources().getString(R.string.yes));
//            no.setText(getResources().getString(R.string.no));
//            desc.setText("Do you want to logout?");

        desc.setText(fistname +"  Do you want to  LOG OUT");


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                customDialog.dismiss();

                Intent i = new Intent(SettingActivity.this, StartScrrenActivity.class);
                i.putExtra("type", "1");
                startActivity(i);
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
