package com.cs.checkclickuser.Activites;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.Models.VerifyEmailResponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends Activity implements View.OnClickListener{
    private int currentStage = 1;
    ImageView backbtn;
    private static final String TAG = "TAG";
    private RelativeLayout nameLayout, emailLayout, phoneLayout;
    private EditText inputFirstname, inputLastname, inputEmail, inputPassword, inputConfirm, inputPhone;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname,inputLayoutEmail, inputLayoutPassword, inputLayoutConfirm;
    private Button btnPhoneNext, btnEmailNext, btnContinue;
    private Context context;
    private String strFirstname, strLastname, strEmail, strPassword, strMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signupactivity);

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputConfirm = (EditText) findViewById(R.id.input_confirm_password);
        inputPhone = (EditText) findViewById(R.id.input_phone);
        inputFirstname = (EditText) findViewById(R.id.input_name);
        inputLastname = (EditText) findViewById(R.id.input_lastname);

        inputLayoutfirstname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutlastname = (TextInputLayout) findViewById(R.id.layout_lastname);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.layout_password);
        inputLayoutConfirm = (TextInputLayout) findViewById(R.id.layout_confirm_password);

        nameLayout = (RelativeLayout) findViewById(R.id.name_layout);
        emailLayout = (RelativeLayout) findViewById(R.id.email_layout);
        phoneLayout = (RelativeLayout) findViewById(R.id.phone_layout);

        btnPhoneNext = (Button) findViewById(R.id.next_name);
        btnEmailNext = (Button) findViewById(R.id.next_email);
        btnContinue = (Button) findViewById(R.id.next_phone);
        backbtn = (ImageView) findViewById(R.id.back_btn);

        btnPhoneNext.setOnClickListener(this);
        btnEmailNext.setOnClickListener(this);
        btnEmailNext.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        btnContinue.setOnClickListener(this);



        inputFirstname.addTextChangedListener(new TextWatcher(inputFirstname));
        inputLastname.addTextChangedListener(new TextWatcher(inputLastname));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirm.addTextChangedListener(new TextWatcher(inputConfirm));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_btn:
                if(currentStage == 1){
                    finish();
                }
                else if(currentStage == 2){
                    currentStage = 1;
                    ((TextView)findViewById(R.id.signup_title)).setVisibility(View.VISIBLE);
                    emailLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    nameLayout.setVisibility(View.VISIBLE);
                    nameLayout.startAnimation(slideUp);
                }
                else if(currentStage == 3){
                    currentStage = 2;
                    phoneLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    emailLayout.setVisibility(View.VISIBLE);
                    emailLayout.startAnimation(slideUp);
                }
                break;

            case R.id.next_name:
                if (nameValidations()) {
                    currentStage = 2;
                    ((TextView) findViewById(R.id.signup_title)).setVisibility(View.GONE);
                    nameLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
                    emailLayout.setVisibility(View.VISIBLE);
                    emailLayout.startAnimation(slideUp);
                }
                break;

            case R.id.next_email:
                if(emailValidations()){
                    currentStage = 3;
//                    emailLayout.setVisibility(View.GONE);
//                    Animation slideUp1 = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
//                    phoneLayout.setVisibility(View.VISIBLE);
//                    phoneLayout.startAnimation(slideUp1);
                    new verifyEmailApi().execute();
                }
                break;

            case R.id.next_phone:
             if(mobileValidations()){
//
                   new verifyMobileApi().execute();

               }
               break;
        }
    }
    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email:
                    clearErrors();
                    break;
                case R.id.input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private void clearErrors() {
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutConfirm.setErrorEnabled(false);
    }

    private boolean mobileValidations(){
        strMobile = inputPhone.getText().toString();
        if(strMobile.length() == 0){
            inputPhone.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        else if(strMobile.length() != 9){
            inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        return true;
    }

    private boolean nameValidations(){
        strFirstname = inputFirstname.getText().toString();
        strLastname = inputLastname.getText().toString();

        if(strFirstname.length() == 0){
            inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            Constants.requestEditTextFocus(inputFirstname, SignUpActivity.this);
            return false;
        }
        if(strLastname.length() == 0){
            inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            Constants.requestEditTextFocus(inputLastname, SignUpActivity.this);
            return false;
        }
        return true;
    }

//    private boolean phonenumberValidations(){
//
//
//        return true;
//    }


    private boolean emailValidations(){

        strEmail = inputEmail.getText().toString();
        strPassword = inputPassword.getText().toString();
        String confirmPwd = inputConfirm.getText().toString();


        if(strEmail.length() == 0){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if(!Constants.isValidEmail(strEmail)){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }

        else if(strPassword.length() == 0){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(strPassword.length() < 4){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
       Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(confirmPwd.length() == 0){
            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        else if(!confirmPwd.equals(strPassword)){
            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_valid_password));
          Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        return true;
    }
    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(SignUpActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(SignUpActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyMobileResponse.getData().getOTP());
                           showVerifyDialog();
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class verifyEmailApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyEmailJson();
            Constants.showLoadingDialog(SignUpActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyEmailResponse> call = apiService.VerifyEmail(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyEmailResponse>() {
                @Override
                public void onResponse(Call<VerifyEmailResponse> call, Response<VerifyEmailResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyEmailResponse VerifyEmailResponse = response.body();
                        try {
                            if (VerifyEmailResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyEmailResponse.getData().getEmail());
                                emailLayout.setVisibility(View.GONE);
                                Animation slideUp1 = AnimationUtils.loadAnimation(SignUpActivity.this, R.anim.enter_from_right);
                                phoneLayout.setVisibility(View.VISIBLE);
                                phoneLayout.startAnimation(slideUp1);
                            } else {
                                String failureResponse = VerifyEmailResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<VerifyEmailResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                }
            });
            return null;
        }
    }

    private String prepareVerifyEmailJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("EmailId", strEmail);
            parentObj.put("FlagId", "4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("MobileNo", "966" + strMobile);
                parentObj.put("FlagId", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
            return parentObj.toString();
        }

        private void showVerifyDialog() {
            Intent intent = new Intent(SignUpActivity.this, VerifyOtpDialog.class);
            intent.putExtra("EmailId", strEmail);
            intent.putExtra("MobileNo", strMobile);
            intent.putExtra("Password", strPassword);
            intent.putExtra("Language", "en");
            intent.putExtra("DeviceToken", -1);
            intent.putExtra("DeviceVersion", SplashScreen.regId);
            intent.putExtra("DeviceType", "ander");
            intent.putExtra("screen", "register");
            startActivity(intent);
        }
}







