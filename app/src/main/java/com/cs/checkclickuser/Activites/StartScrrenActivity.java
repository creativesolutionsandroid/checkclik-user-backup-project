package com.cs.checkclickuser.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.checkclickuser.Fragements.HomeFragment;
import com.cs.checkclickuser.R;

public class StartScrrenActivity extends AppCompatActivity {
    TextView language,skip,signin;
    Button signup;
    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int LOGIN_STATUS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startscrren_activity);
        signup = (Button) findViewById(R.id.sign_btn);
        language =(TextView) findViewById(R.id.tv_en);
        skip =(TextView) findViewById(R.id.tv_skip);
        signin =(TextView) findViewById(R.id.tv_signin);


        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartScrrenActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartScrrenActivity.this, SignInActivity.class);
                startActivityForResult(intent, LOGIN_STATUS);
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment homeFragment = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout,homeFragment).commit();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOGIN_STATUS && resultCode == RESULT_OK){
            Intent intent = new Intent(StartScrrenActivity.this, MainActivity.class);
            startActivity(intent);

        }
//        else if(requestCode == 2 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, InboxFragment.class);
//            Fragment inboxfrahmet = new InboxFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxfrahmet).commit();
//        }
//        else if(requestCode == 3 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, NotificationFragment.class);
//            Fragment notificationfrahmet = new NotificationFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationfrahmet).commit();
//        }
//        else if(requestCode == 4 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, MyProfileFragment.class);
//            Fragment myProfileFragment = new MyProfileFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, myProfileFragment).commit();
//        }
    }

}
