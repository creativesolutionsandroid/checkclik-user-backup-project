package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Dialogs.ResetPasswordDialog;
import com.cs.checkclickuser.Fragements.HomeFragment;
import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

public class VerifyOtpDialog extends AppCompatActivity implements View.OnClickListener {

    private String otpEntered = "", strFirstname, strLastname, strEmail, strPassword, strMobile;

    private TextView description;
    public static boolean isResetSuccessful = false;
    private Button btnNext;
    ImageView back_btn;
    EditText inputotp;
    TextView textMobileNumber, buttonResend;
    private String screen = "";
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        OtpReader.bind((OTPListener) this, "cs-test");
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        userPrefsEditor = userPrefs.edit();
        description = (TextView) findViewById(R.id.verify_desc);
        btnNext = (Button) findViewById(R.id.next_otp);
        buttonResend = (TextView) findViewById(R.id.resend);
        inputotp=(EditText) findViewById(R.id.input_phone);
        back_btn=(ImageView)findViewById(R.id.back_btn);

        strFirstname = getIntent().getStringExtra("FirstName");
        strLastname = getIntent().getStringExtra("LastName");
        strEmail = getIntent().getStringExtra("EmailId");
        strMobile = getIntent().getStringExtra("MobileNo");
        strPassword = getIntent().getStringExtra("Password");
        screen = getIntent().getStringExtra("screen");
//        textMobileNumber.setText(Constants.Country_Code + strMobile);

        description.setText(getResources().getString(R.string.verify_desc)  + "+966 " +
                getIntent().getStringExtra("MobileNo")  + getResources().getString(R.string.verify_desc1));


        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);

        setTimerForResend();
        btnNext.setOnClickListener(this);
        buttonResend.setOnClickListener(this);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });


    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
//                }
//                else {
//                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar) + " in " + timeRemaining);
//                }

//                }
            }

            public void onFinish() {
//                if(getDialog()!=null) {
//                if (language.equalsIgnoreCase("En")){
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
//                }
//                else {
//                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar));
//                }

                buttonResend.setEnabled(true);
                buttonResend.setAlpha(1.0f);
//                }
            }

        }.start();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_otp:
                otpEntered = inputotp.getText().toString();
                if (otpEntered.length() != 4) {

                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                            getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), VerifyOtpDialog.this);

                } else {
                    if (screen.equals("register")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new userRegistrationApi().execute();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (screen.equals("forgot")) {
                        displayResetPasswordDiaolg();
                    }
                }
                break;

            case R.id.resend:
                String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                } else {
                    Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

        private String prepareVerifyMobileJson() {
            JSONObject mobileObj = new JSONObject();
            try {
                mobileObj.put("MobileNo", "966" + strMobile);
                mobileObj.put("FlagId",2);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return mobileObj.toString();
        }

        private String prepareSignUpJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("FirstName", strFirstname);
                parentObj.put("LastName", strLastname);
                parentObj.put("EmailId", strEmail);
                parentObj.put("MobileNo", "966" + strMobile);
                parentObj.put("Password", strPassword);
                parentObj.put("OTPCode", otpEntered);
                parentObj.put("Language", "En");
                parentObj.put("DeviceToken", SplashScreen.regId);
                parentObj.put("DeviceVersion","ander v");
                parentObj.put("DeviceType", "ander");

                Log.d(TAG, "prepareSignUpJson: " + parentObj.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return parentObj.toString();
        }

        private class userRegistrationApi extends AsyncTask<String, String, String> {
            String inputStr;
            final ACProgressFlower dialog = new ACProgressFlower.Builder(VerifyOtpDialog.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();



            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                inputStr = prepareSignUpJson();
                Constants.showLoadingDialog(VerifyOtpDialog.this);

            }

            @Override
            protected String doInBackground(String... params) {
                final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                APIInterface apiService =
                        ApiClient.getClient().create(APIInterface.class);

                Call<Signupresponse> call = apiService.userRegistration(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
                call.enqueue(new Callback<Signupresponse>() {
                    @Override
                    public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                        Log.d("TAG", "onResponse: " + response);
                        if (response.isSuccessful()) {
                            Signupresponse registrationResponse = response.body();
                            if (registrationResponse.getStatus()) {
//                          status true case
                                String userId = registrationResponse.getData().getId();
                                userPrefsEditor.putString("userId", userId);
                                userPrefsEditor.putString("FirstName", registrationResponse.getData().getFirstName());
                                userPrefsEditor.putString("LastName", registrationResponse.getData().getFirstName());
                                userPrefsEditor.putString("EmailId", registrationResponse.getData().getEmailId());
                                userPrefsEditor.putString("MobileNo", registrationResponse.getData().getMobileNo());
//                            userPrefsEditor.putString("Password", registrationResponse.getData().getPhone());
//                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilephoto());
                                userPrefsEditor.commit();
                                Intent intent = new Intent(VerifyOtpDialog.this, HomeFragment.class);
                                intent.putExtra("type", "1");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
//                          status false case
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            }
                        } else {
                            if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                        }

                        Constants.closeLoadingDialog();

                    }

                    @Override
                    public void onFailure(Call<Signupresponse> call, Throwable t) {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }

                        Constants.closeLoadingDialog();

                    }
                });
                return null;
            }
        }

        private class verifyMobileApi extends AsyncTask<String, String, String> {

            String inputStr;
            final ACProgressFlower dialog = new ACProgressFlower.Builder(VerifyOtpDialog.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                inputStr = prepareVerifyMobileJson();
                Constants.showLoadingDialog(VerifyOtpDialog.this);

            }

            @Override
            protected String doInBackground(String... params) {
                final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                APIInterface apiService =
                        ApiClient.getClient().create(APIInterface.class);

                Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
                call.enqueue(new Callback<VerifyMobileResponse>() {
                    @Override
                    public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                        if (response.isSuccessful()) {
                            VerifyMobileResponse VerifyMobileResponse = response.body();
                            try {
                                if (VerifyMobileResponse.getStatus()) {
                                    Log.i(TAG, "otp: " + VerifyMobileResponse.getData().getOTP());
                                } else {
                                    String failureResponse = VerifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VerifyOtpDialog.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }

                        Constants.closeLoadingDialog();
                        buttonResend.setEnabled(false);
                        buttonResend.setAlpha(0.5f);
                        setTimerForResend();
                    }

                    @Override
                    public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }

                        Constants.closeLoadingDialog();
                    }
                });
                return null;
            }
        }

            @Override
            public void onDestroy() {
                super.onDestroy();
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
            }

            @Override
            public void onBackPressed() {

            }

            private void displayResetPasswordDiaolg() {
                Bundle args = new Bundle();
                args.putString("mobile", strMobile);
                args.putString("otp", otpEntered);

                isResetSuccessful = false;

                final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
                newFragment.setCancelable(false);
                newFragment.setArguments(args);
                newFragment.show(getSupportFragmentManager(), "forgot");

                getSupportFragmentManager().executePendingTransactions();
                newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        //do whatever you want when dialog is dismissed
                        if (newFragment != null) {
                            newFragment.dismiss();
                        }
                        if (isResetSuccessful) {
                            Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
                            intent.putExtra("type", "1");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }

        }
