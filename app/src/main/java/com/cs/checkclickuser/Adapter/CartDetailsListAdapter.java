package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OrderRetunActivity;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;

public class CartDetailsListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int QTY = 0;
    CartResponce.Data cartlistArrayList ;

    public CartDetailsListAdapter(Context context, CartResponce.Data orderList) {
        this.context = context;
        this.cartlistArrayList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cartlistArrayList.getCartList().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView storename, iteamcount, qty, price,retunbtn,qtycount;
        ImageView storeimage,decrease,increase;

    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.cart_adapter_list, null);

            holder.storename = (TextView) convertView.findViewById(R.id.storename);
            holder.iteamcount = (TextView) convertView.findViewById(R.id.iteamcount);
            holder.qty = (TextView) convertView.findViewById(R.id.products);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.qtycount = (TextView) convertView.findViewById(R.id.qtycount);
            holder.retunbtn = (TextView) convertView.findViewById(R.id.retun);
            holder.storeimage = (ImageView) convertView.findViewById(R.id.storeimage);
            holder.decrease = (ImageView) convertView.findViewById(R.id.decrease);
            holder.increase = (ImageView) convertView.findViewById(R.id.increase);


        holder.storename.setText(cartlistArrayList.getCartList().get(position).getProductNameEn());
        holder.iteamcount.setText(""+ cartlistArrayList.getCartList().get(position).getCartQuantity()+"Pics");

//        holder.decrease.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                QTY = cartlistArrayList.getCartList().get(position).getCartQuantity();
//                QTY = QTY + 1;
//                display(QTY);
//
//            }
//        });
//        holder.increase.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                QTY = QTY + 1;
//                display(QTY);
//
//            }
//        });
//        holder.qtycount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TextView displayInteger = (TextView) findViewById(
//                        R.id.qtycount);
//                displayInteger.setText("" + number);
//            }
//        });




        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL+ cartlistArrayList.getCartList().get(position).getImage())
                .into(holder.storeimage);
        return convertView;
    }


}
