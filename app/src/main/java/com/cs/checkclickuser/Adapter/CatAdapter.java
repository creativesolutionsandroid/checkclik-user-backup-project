package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.checkclickuser.Models.Catresponce;
import com.cs.checkclickuser.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CatAdapter extends  RecyclerView.Adapter<CatAdapter.MyViewHolder>  {


    private Context context;
    private int selectedPosition = 0;
    private ArrayList<Catresponce>catArrayList = new ArrayList<>();
    private Activity activity;
    int pos = 0;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;

    public CatAdapter(Context context, ArrayList<Catresponce> catArrayList, int pos, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.catArrayList = catArrayList;

    }


    @Override
    public CatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_chaild, parent, false);
        return new CatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CatAdapter.MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.catname.setText(catArrayList.get(position).getData().getMainCategory().get(position).getNameEn());
    }


    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView catname;
        LinearLayout catclick;
//        ImageView storeimage;
        public MyViewHolder(View itemView) {
            super(itemView);
            catname = (TextView) itemView.findViewById(R.id.catname);
            catclick = (LinearLayout)itemView.findViewById(R.id.catclick);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String catId =""+catArrayList.get(pos).getData().getCategoryId();
                    String catname =""+(catArrayList.get(pos).getData().getMainCategory().get(pos).getNameEn());
                    userPrefsEditor.putString("catid", catId);
                    userPrefsEditor.putString("Categoryname", catname);
                    userPrefsEditor.commit();

                }
            });
        }
    }

}
