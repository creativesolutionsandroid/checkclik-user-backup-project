package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.checkclickuser.Models.CouponResponce;
import com.cs.checkclickuser.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CouponAdapter extends RecyclerView.Adapter< CouponAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<CouponResponce.Data> couponArrayList = new ArrayList<>();
    int pos = 0;

    public CouponAdapter(Context context,  ArrayList<CouponResponce.Data> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.couponArrayList = storeArrayList;
    }

    @NonNull
    @Override
    public CouponAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coupon_adapter, parent, false);
        return new CouponAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponAdapter.MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.mainstore.setText(couponArrayList.get(position).getBranchNameEn());
        holder.offerrate.setText(couponArrayList.get(position).getVoucherNameEn());
        holder.code.setText("Code : "+couponArrayList.get(position).getCouponCode());
//        holder.available.setText(couponArrayList.get(position).getData().get(position).get());

        String date = couponArrayList.get(position).getEndDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd,MMM,yyyy",Locale.US); ;

        try {
            Date datetime = format.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "date: "+date);
        holder.validity.setText(""+date);

//        holder.validity.setText(couponArrayList.get(position).getEndDate());

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "couponlistsize: "+couponArrayList.size());
        return couponArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, code, available, validity,offerrate;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.storename);
            code = (TextView) itemView.findViewById(R.id.code);
            available = (TextView) itemView.findViewById(R.id.expirdate);
            validity = (TextView) itemView.findViewById(R.id.date);
            offerrate=(TextView)itemView.findViewById(R.id.offerrate);
        }
    }

}
