package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class FavProductsAdapter  extends RecyclerView.Adapter< FavProductsAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<FavStoresResponce.ProductList> storeArrayList = new ArrayList<>();
    int pos = 0;

    public FavProductsAdapter(Context context, ArrayList<FavStoresResponce.ProductList> storeArrayList) {
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
    }

    @Override
    public FavProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fav_iteam_adapter, parent, false);
        return new FavProductsAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull FavProductsAdapter.MyViewHolder holder, int position) {

        holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.itemname.setText(storeArrayList.get(pos).getFirstName());
        holder.mrpprice.setText("SAR "+ Constants.priceFormat1.format(storeArrayList.get(pos).getPrice()));
        holder.ourprice.setText("SAR "+ Constants.priceFormat1.format(storeArrayList.get(pos).getSellingPrice()));
        holder.discount.setText("" + storeArrayList.get(pos).getDiscountValue() + "% OFF");

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL + storeArrayList.get(position).getImage())
                .into(holder.storeimage);
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "favproductadapter: "+storeArrayList.size());
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemname, mrpprice, ourprice, discount;
        ImageView storeimage;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.iteamname);
            mrpprice = (TextView) itemView.findViewById(R.id.mrp);
            ourprice = (TextView) itemView.findViewById(R.id.ourprice);
            discount = (TextView) itemView.findViewById(R.id.disscount);
            storeimage = (ImageView)itemView.findViewById(R.id.storeimage);
            storelayout =(LinearLayout)itemView.findViewById(R.id.storelayout);



//            storelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(productArrayList.size()> 0) {
//                        Intent intent = new Intent(context, ProductStoresActivity.class);
//                        intent.putExtra("stores", productArrayList);
//                        intent.putExtra("pos",getAdapterPosition());
//                        context.startActivity(intent);
//                    }
//                    else {
//                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

        }
    }
}
