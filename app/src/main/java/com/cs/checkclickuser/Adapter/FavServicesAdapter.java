package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductStoresActivity;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.R;

import java.text.DecimalFormat;
import java.util.ArrayList;


import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class FavServicesAdapter extends RecyclerView.Adapter< FavServicesAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<FavStoresResponce.ServiceList> storeArrayList = new ArrayList<>();
    int pos = 0;

    public FavServicesAdapter(Context context,  ArrayList<FavStoresResponce.ServiceList> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
    }


    @Override
    public FavServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favservices_adapter, parent, false);
        return new FavServicesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavServicesAdapter.MyViewHolder holder,  int position) {

        holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.itemname.setText(storeArrayList.get(position).getServiceNameEn());
        holder.mrpprice.setText("SAR "+storeArrayList.get(position).getPrice());
        holder.ourprice.setText("SAR "+storeArrayList.get(pos).getSellingPrice());
        holder.discount.setText(""+storeArrayList.get(pos).getDiscountValue()+"% OFF");

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL+storeArrayList.get(position).getImage())
                .into(holder.storeimage);
        Log.d(TAG, "serviesimage: "+STORE_IMAGE_URL+storeArrayList.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "adaptersize: "+storeArrayList.size());
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemname, mrpprice, ourprice, discount;
        ImageView storeimage;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.iteamname);
            mrpprice = (TextView) itemView.findViewById(R.id.mrp);
            ourprice = (TextView) itemView.findViewById(R.id.ourprice);
            discount = (TextView) itemView.findViewById(R.id.disscount);
            storeimage = (ImageView)itemView.findViewById(R.id.storeimage);
            storelayout =(LinearLayout)itemView.findViewById(R.id.storelayout);


            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(storeArrayList.size()> 0) {
                        Intent intent = new Intent(context, ProductStoresActivity.class);
                        intent.putExtra("favstores", storeArrayList);
                        intent.putExtra("favpos",getAdapterPosition());
                        context.startActivity(intent);
                    }
                    else {
                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }
}
