package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.provider.CalendarContract;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductStoresActivity;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.checkclickuser.Fragements.CategoryFragment.TAG;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;


public class FavStoreAdapter extends RecyclerView.Adapter< FavStoreAdapter.MyViewHolder>{

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<FavStoresResponce.StoreList> storeArrayList = new ArrayList<>();
    int pos = 0;

    public FavStoreAdapter(Context context,  ArrayList<FavStoresResponce.StoreList> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
    }

    @NonNull
    @Override
    public FavStoreAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favstore_adapter, parent, false);
        return new FavStoreAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavStoreAdapter.MyViewHolder holder,  int position) {


        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.mainstore.setText(storeArrayList.get(position).getStoreEn());
        holder.storerating.setText(""+storeArrayList.get(position).getRatings()+".0");
        holder.distance.setText(priceFormat.format(storeArrayList.get(pos).getDistance())+" KM");
        holder.reviews.setText(""+storeArrayList.get(pos).getReviews()+"Reviews");
        holder.deliverytime.setText(storeArrayList.get(position).getDeliveryTime());
        holder.minoder.setText(""+storeArrayList.get(position).getMinimumOrderValue()+" SAR");
        holder.storestatus.setText(storeArrayList.get(position).getBranchStatus());

        if (storeArrayList.get(position).getIsCashAllowed().equals("true")&&(storeArrayList.get(position).getIsCreditCardAllowed().equals("true"))) {
            holder.paymentcard.setVisibility(View.VISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        }

        else if (storeArrayList.get(position).getIsCreditCardAllowed().equals("false")){
            holder.paymentcard.setVisibility(View.INVISIBLE);

        }
        else if (storeArrayList.get(position).getIsCashAllowed().equals("false")){
            holder.paymentcash.setVisibility(View.INVISIBLE);

        }
        if (storeArrayList.get(position).getBranchStatus().equalsIgnoreCase("open")){
            holder.statusimage.setColorFilter(Color.GREEN);
            holder.storestatus.setTextColor(Color.GREEN);
        }
        else {
            holder.statusimage.setColorFilter(Color.RED);
            holder.storestatus.setTextColor(Color.RED);
        }

        Glide.with(context)
                .load(STORE_IMAGE_URL+storeArrayList.get(0).getStoreImage())
                .into(holder.storeimage);

    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, storerating, reviews, storestatus, distance,minoder,deliverytime;
        ImageView storeimage,paymentcard,paymentcash,statusimage;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.mainstore);
            storerating = (TextView) itemView.findViewById(R.id.revies_count);
            distance = (TextView) itemView.findViewById(R.id.distance);
            reviews = (TextView) itemView.findViewById(R.id.revies);
            storestatus = (TextView)itemView.findViewById(R.id.storestatus);
            minoder = (TextView)itemView.findViewById(R.id.minmodr);
            deliverytime = (TextView)itemView.findViewById(R.id.delivery_time);
            storeimage = (ImageView)itemView.findViewById(R.id.storeimage);
            storelayout =(LinearLayout)itemView.findViewById(R.id.storelayout);
            paymentcard=(ImageView)itemView.findViewById(R.id.payment_card);
            paymentcash=(ImageView)itemView.findViewById(R.id.payment_cash);
            statusimage=(ImageView)itemView.findViewById(R.id.storestatusimage) ;


//            storelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(productArrayList.size()> 0) {
//                        Intent intent = new Intent(context, ProductStoresActivity.class);
//                        intent.putExtra("stores", productArrayList);
//                        intent.putExtra("pos",getAdapterPosition());
//                        context.startActivity(intent);
//                    }
//                    else {
//                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

        }
    }
}
