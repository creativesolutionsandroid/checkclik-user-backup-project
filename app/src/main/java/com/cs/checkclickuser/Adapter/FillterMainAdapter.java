package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.cs.checkclickuser.Activites.FillterActivity;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

import static com.cs.checkclickuser.Adapter.OrderProcessingAdapter.TAG;

public class FillterMainAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    private ArrayList<ProductlistResponce.FilterMainCategory> mainCategoriesArrayList = new ArrayList<>();
    FillterSubAdapter FillterSubAdapter;

    public FillterMainAdapter(Context context, ArrayList<ProductlistResponce.FilterMainCategory> orderList) {
        this.context = context;
        this.mainCategoriesArrayList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mainCategoriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView carmainname;

    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.maincat_adapter, null);
            holder.carmainname = (TextView) convertView.findViewById(R.id.carmainname1);
            holder.carmainname.setVisibility(View.VISIBLE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.carmainname.setText(mainCategoriesArrayList.get(position).getTypeName());


        return convertView;
    }
}
