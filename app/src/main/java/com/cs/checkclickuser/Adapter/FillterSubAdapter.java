package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.cs.checkclickuser.Fragements.Product_fragment;
import com.cs.checkclickuser.Models.FilterSubResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

import static com.cs.checkclickuser.Adapter.OrderProcessingAdapter.TAG;

public class FillterSubAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    private ArrayList<FilterSubResponce> subCategoriesArrayList = new ArrayList<>();

    public FillterSubAdapter(Context context, ArrayList<FilterSubResponce> orderList) {
        this.context = context;
        this.subCategoriesArrayList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return subCategoriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        RadioButton catmainname;

    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.maincat_adapter, null);
            holder.catmainname = (RadioButton) convertView.findViewById(R.id.carmainname);
            holder.catmainname.setVisibility(View.VISIBLE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.catmainname.setText(subCategoriesArrayList.get(position).getName());

        if (Product_fragment.filterid.contains(subCategoriesArrayList.get(position).getCategoryId())) {
            holder.catmainname.setChecked(true);

        } else {
            holder.catmainname.setChecked(false);
        }

//        holder.catmainname.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Product_fragment.filterid.contains(subCategoriesArrayList.get(position).getCategoryId())) {
//                    holder.catmainname.setChecked(false);
//                    for (int j = 0; j < Product_fragment.filterid.size(); j++) {
//                        if (Product_fragment.filterid.get(j) == subCategoriesArrayList.get(position).getCategoryId()) {
//                            Product_fragment.filterid.remove(j);
//                            break;
//                        }
//                    }
//                } else {
//                    holder.catmainname.setChecked(true);
//                    Product_fragment.filterid.add(subCategoriesArrayList.get(position).getCategoryId());
//                }
//            }
//
//            });

        return convertView;
    }
}
