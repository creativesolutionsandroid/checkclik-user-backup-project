package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class HomeScreenBannersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<HomePageResponse.Homebanners> bannersList;
    Context context;

    public HomeScreenBannersAdapter(Context context, ArrayList<HomePageResponse.Homebanners> bannersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannersList = bannersList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return (bannersList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_homescreen_banners, container, false);

        ImageView banner_image = (ImageView) itemView.findViewById(R.id.banner_image);
        TextView discount_value = (TextView) itemView.findViewById(R.id.discount_value);

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + bannersList.get(position).getBannerimageen())
                .into(banner_image);

        if(bannersList.get(position).getOffertype() == 1) {
            discount_value.setText(bannersList.get(position).getPercentage() + "%\nOff");
        }
        else {
            discount_value.setText(bannersList.get(position).getPercentage() + "\nOff");
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
