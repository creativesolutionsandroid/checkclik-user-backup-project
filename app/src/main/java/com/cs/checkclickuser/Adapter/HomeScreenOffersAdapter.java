package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class HomeScreenOffersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<HomePageResponse.Offers> productOffersList;
    ArrayList<HomePageResponse.Offers2> servicesOffersList;
    Context context;

    public HomeScreenOffersAdapter(Context context, ArrayList<HomePageResponse.Offers> productOffersList,
                                   ArrayList<HomePageResponse.Offers2> servicesOffersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.productOffersList = productOffersList;
        this.servicesOffersList = servicesOffersList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 3f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: "+(productOffersList.size() + servicesOffersList.size()));
        return (productOffersList.size() + servicesOffersList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((android.support.v7.widget.CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_homescreen_offers, container, false);

        ImageView productImage = (ImageView) itemView.findViewById(R.id.image);
        TextView discount_percent = (TextView) itemView.findViewById(R.id.discount_percent);
        TextView title = (TextView) itemView.findViewById(R.id.title);
        TextView discountedPrice = (TextView) itemView.findViewById(R.id.discounted_price);
        TextView price = (TextView) itemView.findViewById(R.id.price);

        discountedPrice.setPaintFlags(discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if(position < productOffersList.size()) {
            if(productOffersList.get(position).getDiscounttype() == 1) {
                discount_percent.setText((int)productOffersList.get(position).getDiscountvalue()+"% Off");
            }
            else {
                discount_percent.setText(productOffersList.get(position).getDiscountvalue()+" Off");
            }
            title.setText(productOffersList.get(position).getNameen());
            discountedPrice.setText("SAR "+Constants.priceFormat1.format(productOffersList.get(position).getSellingprice()));
            price.setText("SAR "+Constants.priceFormat1.format(productOffersList.get(position).getPrice()));

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + productOffersList.get(position).getImage())
                    .into(productImage);
        }
        else {
            int pos = position - productOffersList.size();
            if(servicesOffersList.get(pos).getDiscounttype() == 1) {
                discount_percent.setText((int)servicesOffersList.get(pos).getDiscountvalue()+"% Off");
            }
            else {
                discount_percent.setText(servicesOffersList.get(pos).getDiscountvalue()+" Off");
            }
            title.setText(servicesOffersList.get(pos).getServicenameen());
            discountedPrice.setText("SAR "+Constants.priceFormat1.format(servicesOffersList.get(pos).getSellingprice()));
            price.setText("SAR "+Constants.priceFormat1.format(servicesOffersList.get(pos).getPrice()));

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + servicesOffersList.get(pos).getImage())
                    .into(productImage);

            Log.d("TAG", "image: "+servicesOffersList.get(pos).getImage());
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((android.support.v7.widget.CardView) object);
    }
}
