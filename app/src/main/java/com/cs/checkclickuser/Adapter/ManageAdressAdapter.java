package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ManageAdressAdapter extends RecyclerView.Adapter< ManageAdressAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<ManageAdressResponce.Data> addressArrayList = new ArrayList<>();
    int pos = 0;

    public ManageAdressAdapter(Context context,  ArrayList<ManageAdressResponce.Data> addressArrayList){
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
    }

    @NonNull
    @Override
    public ManageAdressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.manageadress_adapter, parent, false);
        return new ManageAdressAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull ManageAdressAdapter.MyViewHolder holder, int position) {

        holder.buildingname.setText(addressArrayList.get(position).getFullName());
        holder.adressline.setText(addressArrayList.get(position).getAddress1()+","+ addressArrayList.get(position).getAddress2()+","+ addressArrayList.get(position).getZipcode());
        holder.phonenumber.setText("Phone Number : "+addressArrayList.get(position).getPhoneNumber());

        if (addressArrayList.get(position).getAddressType()==1){
            holder.adresstype.setText("Home");
        }
        else {
            holder.adresstype.setText("Office");
        }



    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView buildingname, adressline, phonenumber, adresstype;
        Button edit,delete;


        public MyViewHolder(View itemView) {
            super(itemView);
            buildingname = (TextView) itemView.findViewById(R.id.housename);
            adressline = (TextView) itemView.findViewById(R.id.address);
            phonenumber = (TextView) itemView.findViewById(R.id.phonenumber);
            adresstype = (TextView) itemView.findViewById(R.id.addresstype);
            edit=(Button)itemView.findViewById(R.id.edit_btn);
            delete=(Button)itemView.findViewById(R.id.delet_btn);


        }
    }
}
