package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Activites.OrderRetunActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class OrderDetailsListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();

    public OrderDetailsListAdapter(Context context, ArrayList<OrderPendingResponce.Data> orderList) {
        this.context = context;
        this.pendingArrayList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pendingArrayList.get(0).getItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView storename, iteamcount, qty, price,retunbtn;
        ImageView storeimage;
    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.details_adapter_list, null);
            holder.storename = (TextView) convertView.findViewById(R.id.storename);
            holder.iteamcount = (TextView) convertView.findViewById(R.id.iteamcount);
            holder.qty = (TextView) convertView.findViewById(R.id.products);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.retunbtn = (TextView) convertView.findViewById(R.id.retun);
            holder.storeimage = (ImageView) convertView.findViewById(R.id.storeimage);

        holder.storename.setText(pendingArrayList.get(position).getItems().get(0).getNameEn());
        holder.iteamcount.setText(""+pendingArrayList.get(position).getItems().get(0).getQty());

        holder.retunbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderRetunActivity.class);
                intent.putExtra("Order", pendingArrayList);
                intent.putExtra("Orderpos",position);
                context.startActivity(intent);            }
        });

            Glide.with(context)
                .load(PRODUCTS_IMAGE_URL+pendingArrayList.get(position).getItems().get(0).getImage())
                .into(holder.storeimage);
        holder.qty.setText("Qty   "+pendingArrayList.get(position).getItems().get(0).getQty());
        return convertView;
    }
}
