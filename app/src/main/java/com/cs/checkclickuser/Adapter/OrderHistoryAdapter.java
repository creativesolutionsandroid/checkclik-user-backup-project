package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Activites.ProductRatingActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OrderHistoryAdapter extends RecyclerView.Adapter< OrderHistoryAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> histoyArrayList = new ArrayList<>();
    int pos = 0;

    public OrderHistoryAdapter(Context context, ArrayList<OrderPendingResponce.Data> histoyArrayList) {
        this.context = context;
        this.activity = activity;
        this.histoyArrayList = histoyArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @NonNull
    @Override
    public OrderHistoryAdapter.MyViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_history_list, parent, false);
        return new OrderHistoryAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull OrderHistoryAdapter.MyViewHolder holder, final int position) {
        holder.orderno.setText("Order No :"+histoyArrayList.get(position).getInvoiceNo());

        holder.list_item.removeAllViews();
        for (int i = 0; i < histoyArrayList.get(position).getItems().size(); i++) {
            View v = inflater.inflate(R.layout.history_adapter_list, null);
            ImageView imageView = v.findViewById(R.id.storeimage);
            TextView productname = v.findViewById(R.id.storename);
            TextView qty = v.findViewById(R.id.products);
            TextView ratingtext = v.findViewById(R.id.productrate);
            LinearLayout layout = v.findViewById(R.id.layout);
            RatingBar ratingBar = v.findViewById(R.id.ratingbar);
            qty.setText(histoyArrayList.get(position).getExpectingDelivery());
            productname.setText(histoyArrayList.get(position).getItems().get(i).getNameEn());

            ratingBar.setRating(histoyArrayList.get(position).getItems().get(i).getRating());

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL+histoyArrayList.get(position).getItems().get(i).getImage())
                    .into(imageView);

            if (histoyArrayList.get(position).getItems().get(i).getRating()==0){
                ratingtext.setVisibility(View.VISIBLE);
            }
            else {
                ratingBar.setVisibility(View.VISIBLE);
            }
            if (histoyArrayList.get(position).getOrderStatus()==11){
                holder.status.setText("Delivered");
            }
            else if (histoyArrayList.get(position).getOrderStatus()==16){
                holder.status.setText("Cancelled");
                ratingtext.setVisibility(View.INVISIBLE);
            }
            else if (histoyArrayList.get(position).getOrderType()==11){
                holder.status.setText("Rejected");
                ratingtext.setVisibility(View.INVISIBLE);
            }
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("history", histoyArrayList);
                    intent.putExtra("historypos",position);
                    context.startActivity(intent);
                }
            });

            String date1 = histoyArrayList.get(position).getExpectingDelivery();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
            SimpleDateFormat sdf3 = new SimpleDateFormat("MMM-dd-yyyy",Locale.US);
            date1.replace(date1,"yyyy-MM-dd");
            try {
                Date datetime = format.parse(date1);
                date1 = sdf3.format(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            qty.setText(""+date1);

            ratingtext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductRatingActivity.class);
                    intent.putExtra("history",histoyArrayList);
                    intent.putExtra("historypos",pos);

                    intent.putExtra("Type", histoyArrayList.get(position).getOrderType());
                    intent.putExtra("OrderId", histoyArrayList.get(position).getOrderId());
                    intent.putExtra("Ids", histoyArrayList.get(position).getItems().get(position).getSKUId());
                    intent.putExtra("BranchId", histoyArrayList.get(position).getBranchId());

                    context.startActivity(intent);
                }
            });

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("histore", histoyArrayList);
                    intent.putExtra("historepos",pos);
                    context.startActivity(intent);
                }
            });
            holder.list_item.addView(v);
        }
    }
    @Override
    public int getItemCount() {
        return histoyArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView status, orderno;
        ImageView orderstatus;
        RelativeLayout list_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            status = (TextView) itemView.findViewById(R.id.pendingstatus);
            orderno = (TextView) itemView.findViewById(R.id.orderno);
            orderstatus=(ImageView)itemView.findViewById(R.id.greenimage);
            list_item=(RelativeLayout)itemView.findViewById(R.id.relativelist);

        }
    }
}
