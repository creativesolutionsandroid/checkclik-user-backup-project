package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.CamcorderProfile.get;

public class OrderPendingAdapter extends RecyclerView.Adapter< OrderPendingAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    int pos = 0;

    public OrderPendingAdapter(Context context, ArrayList<OrderPendingResponce.Data> pendingArrayList) {
        this.context = context;
        this.activity = activity;
        this.pendingArrayList = pendingArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @NonNull
    @Override
    public OrderPendingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.oredr_pending_adapter, parent, false);
        return new OrderPendingAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull OrderPendingAdapter.MyViewHolder holder, final int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.orderno.setText("Order No :"+pendingArrayList.get(position).getInvoiceNo());
        holder.orderstatus.setImageResource(R.drawable.status_red);

        holder.list_item.removeAllViews();
        for (int i = 0; i < pendingArrayList.get(position).getItems().size(); i++) {
            Log.d(TAG, "i: "+i);
            View v = inflater.inflate(R.layout.pending_adapter_list, null);
            ImageView imageView = v.findViewById(R.id.storeimage);
            TextView productname = v.findViewById(R.id.storename);
            TextView qty = v.findViewById(R.id.products);
            TextView accepetedstatus=v.findViewById(R.id.ordestatustext);
            RelativeLayout rejectlayout=v.findViewById(R.id.rejectlayout);
            TextView numberof_orders=v.findViewById(R.id.scheduldate);
            LinearLayout layout = v.findViewById(R.id.layout);
            Button yes_btn= v.findViewById(R.id.yes_btn);
            Button no_btn= v.findViewById(R.id.no_btn);
            qty.setText("Qty : "+pendingArrayList.get(position).getItemsCount());
            productname.setText(pendingArrayList.get(position).getItems().get(i).getNameEn());
            Log.d(TAG, "productame"+pendingArrayList.get(position).getOrderId());

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL+pendingArrayList.get(position).getItems().get(i).getImage())
                    .into(imageView);

            if (pendingArrayList.get(position).getOrderStatus()==1){
                accepetedstatus.setVisibility(View.VISIBLE);
            }
            else {
                numberof_orders.setText(pendingArrayList.get(position).getRejectedItemsCount()+" out of "+pendingArrayList.get(position).getItemsCount()+" order have been ");
                rejectlayout.setVisibility(View.VISIBLE);
            }
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", pendingArrayList);
                    intent.putExtra("Orderpos",position);
                    context.startActivity(intent);
                }
            });

            yes_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new getaccepetApi().execute();
                }
            });
            no_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new getcancelApi().execute();
                }
            });
            holder.list_item.addView(v);
        }

    }
    @Override
    public int getItemCount() {
//        Log.d(TAG, "getItemCount: "+cartlistArrayList.size());
        return pendingArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView status, orderno ;
        ImageView orderstatus;
        RelativeLayout list_item;
        LinearLayout datelayout,buttonlayout;

        public MyViewHolder(View itemView) { super(itemView);

            status = (TextView) itemView.findViewById(R.id.pendingstatus);
            orderno = (TextView) itemView.findViewById(R.id.orderno);
            orderstatus=(ImageView)itemView.findViewById(R.id.greenimage);
            list_item=(RelativeLayout)itemView.findViewById(R.id.relativelist);
            datelayout=(LinearLayout)itemView.findViewById(R.id.datelayout);
            buttonlayout=(LinearLayout)itemView.findViewById(R.id.buttonlayout);

        }
    }

    private String prepareJson(int pos){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", pendingArrayList.get(pos).getUserId());
            parentObj.put("OrderId", pendingArrayList.get(pos).getOrderId());
            parentObj.put("Message", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareOrderCancelJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class getcancelApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getcancl(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if(response.isSuccessful()){
                        OrderCancelResponce requestsResponse = response.body();
                        Log.d(TAG, "onsucces: "+response);
                        try {
                            if(requestsResponse.getStatus()){
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }
                            else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
    private class getaccepetApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getaccepet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if(response.isSuccessful()){
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if(requestsResponse.getStatus()){
//                                Intent intent = new Intent("cancel_request");
//                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }
                            else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
