package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ManageAdreesActivity;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Activites.ProductRatingActivity;
import com.cs.checkclickuser.Activites.SignInActivity;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class OrderProcessingAdapter  extends RecyclerView.Adapter< OrderProcessingAdapter.MyViewHolder>{
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    int pos = 0;

    public OrderProcessingAdapter(Context context, ArrayList<OrderPendingResponce.Data> pendingArrayList) {
        this.context = context;
        this.activity = activity;
        this.pendingArrayList = pendingArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @NonNull
    @Override
    public OrderProcessingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.oredr_pending_adapter, parent, false);
        return new OrderProcessingAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull OrderProcessingAdapter.MyViewHolder holder, final int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.orderno.setText("Order No :" + pendingArrayList.get(position).getInvoiceNo());
        holder.pendingmaintext.setText("Processing");
        holder.orderstatus.setColorFilter(context.getResources().getColor(R.color.blue));

        holder.list_item.removeAllViews();

        for (int i = 0; i < pendingArrayList.get(position).getItems().size(); i++) {

            View v = inflater.inflate(R.layout.processing_adapter_list, null);
            ImageView imageView = v.findViewById(R.id.storeimage);
            TextView productname = v.findViewById(R.id.storename);
            TextView orderstatustext = v.findViewById(R.id.ordestatustext);

            LinearLayout deliveylayout = v.findViewById(R.id.deliveylayout);
            TextView scheduldate = v.findViewById(R.id.scheduldate);

            ImageView accepetedimage = v.findViewById(R.id.accepetedimage);
            ImageView packedimage = v.findViewById(R.id.packedimage);
            ImageView dispatchimage = v.findViewById(R.id.dispatchimage);
            ImageView deliviedimage = v.findViewById(R.id.deliviedimage);

            Button accept_btn = v.findViewById(R.id.accept_btn);
            Button reject_btn = v.findViewById(R.id.reject_btn);
            TextView packedtext =v.findViewById(R.id.packedtext);

            TextView accepetdate = v.findViewById(R.id.accepetdate);
            TextView packeddate = v.findViewById(R.id.packeddate);
            TextView dispatcheddate = v.findViewById(R.id.dispatcheddate);
            TextView delivereddate = v.findViewById(R.id.delivereddate);

            LinearLayout layout = v.findViewById(R.id.layout);
            RelativeLayout scheduledlayout = v.findViewById(R.id.scheduledlayout);
            LinearLayout orderstatuslayout = v.findViewById(R.id.orderstatuslayout);
            LinearLayout acceptedlayout = v.findViewById(R.id.accepetlayout);
            LinearLayout packedlayout = v.findViewById(R.id.packedlayout);
            LinearLayout dispatchlayout = v.findViewById(R.id.dispatchlayout);
            LinearLayout deliverylayout = v.findViewById(R.id.deliverylayout);

            orderstatuslayout.setVisibility(View.VISIBLE);

            if (pendingArrayList.get(position).getOrderType()==2){
                packeddate.setVisibility(View.GONE);
                packedlayout.setVisibility(View.GONE);
                packedtext.setVisibility(View.GONE);
            }
            TextView qty = v.findViewById(R.id.products);
            qty.setText("Qty  " + pendingArrayList.get(position).getItemsCount());
            productname.setText(pendingArrayList.get(position).getItems().get(i).getNameEn());
            scheduldate.setText("Your order has been Re-Scheduled for  " + pendingArrayList.get(position).getReScheduleDate());

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("processsing", pendingArrayList);
                    intent.putExtra("processsingpos",position);
                    context.startActivity(intent);
                }
            });


            Glide.with(context)
                    .load(PRODUCTS_IMAGE_URL + pendingArrayList.get(position).getItems().get(i).getImage())
                    .into(imageView);
//            Log.d(TAG, "storeimage: " + PRODUCTS_IMAGE_URL + cartlistArrayList.get(position).getItems().get(i).getImage());
            holder.list_item.addView(v);


            if (pendingArrayList.get(position).getOrderStatus() == 2 || pendingArrayList.get(position).getOrderStatus() == 5) {

                accepetedimage.setImageResource(R.drawable.acceptedgreen);
            } else if (pendingArrayList.get(position).getOrderStatus() == 7) {

                accepetedimage.setImageResource(R.drawable.rightmark_gray2x);
                packedimage.setImageResource(R.drawable.acceptedgreen);
            } else if (pendingArrayList.get(i).getOrderStatus() == 9 || pendingArrayList.get(i).getOrderStatus() == 10 || pendingArrayList.get(i).getOrderStatus() == 15 || pendingArrayList.get(i).getOrderStatus() == 17) {

                accepetedimage.setImageResource(R.drawable.rightmark_gray2x);
                packedimage.setImageResource(R.drawable.rightmark_gray2x);
                dispatchimage.setImageResource(R.drawable.acceptedgreen);
            } else if (pendingArrayList.get(i).getOrderStatus() == 11) {

                accepetedimage.setImageResource(R.drawable.rightmark_gray2x);
                packedimage.setImageResource(R.drawable.rightmark_gray2x);
                dispatchimage.setImageResource(R.drawable.rightmark_gray2x);
                deliviedimage.setImageResource(R.drawable.acceptedgreen);
            } else if (pendingArrayList.get(i).getOrderStatus() == 19) {

                accepetedimage.setImageResource(R.drawable.rightmark_gray2x);
                packedimage.setImageResource(R.drawable.acceptedgreen);

            }

            if (pendingArrayList.get(position).getOrderStatus() == 18) {

                scheduledlayout.setVisibility(View.VISIBLE);
            } else {
                deliveylayout.setVisibility(View.VISIBLE);
            }

            for (int j = 0; j < pendingArrayList.size(); j++) {

            }

            accept_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });
            reject_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new getcancelApi().execute();

                }
            });

        }
    }

    @Override
    public int getItemCount() {
//        Log.d(TAG, "getItemCount: "+cartlistArrayList.size());
        return pendingArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView orderno,pendingmaintext ;
        ImageView orderstatus;
        RelativeLayout list_item;
        LinearLayout datelayout,buttonlayout;

        public MyViewHolder(View itemView) { super(itemView);

            orderno = (TextView) itemView.findViewById(R.id.orderno);
            orderstatus=(ImageView)itemView.findViewById(R.id.greenimage);
            datelayout=(LinearLayout)itemView.findViewById(R.id.datelayout);
            buttonlayout=(LinearLayout)itemView.findViewById(R.id.buttonlayout);
            pendingmaintext=(TextView)itemView.findViewById(R.id.pendingstatus);
            list_item=(RelativeLayout) itemView.findViewById(R.id.relativelist);

        }
    }

    private String prepareJson(int pos){

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", pendingArrayList.get(pos).getUserId());
            parentObj.put("OrderId", pendingArrayList.get(pos).getOrderId());
            parentObj.put("Reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareOrderCancelJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class getcancelApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getcancl(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if(response.isSuccessful()){
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if(requestsResponse.getStatus()){
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }
                            else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }



                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }


                    Constants.closeLoadingDialog();                }
            });
            return null;
        }
    }
}
