package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductStoresActivity;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.checkclickuser.Fragements.CategoryFragment.TAG;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

/**
 * Created by PULi on 18-4-2018.
 */

public class ProductAdapter extends  RecyclerView.Adapter<ProductAdapter.MyViewHolder> {


    private Context context;
    private int selectedPosition = 0;
    private ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();
    private Activity activity;
    int pos = 0;


    public ProductAdapter(Context context, ArrayList<ProductlistResponce.Stores> productArrayList) {
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.productArrayList = productArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.mainstore.setText(productArrayList.get(position).getStoreEn());
        holder.storerating.setText(Constants.priceFormat1.format(productArrayList.get(position).getRatings()));
        holder.distance.setText(priceFormat.format(productArrayList.get(pos).getDistance())+" KM");
//        holder.reviews.setText("("+productArrayList.get(pos).getReviews()+" Reviews)");
        holder.deliverytime.setText(productArrayList.get(position).getDeliveryTime());
        holder.minoder.setText(""+productArrayList.get(position).getMinimumOrderValue()+" SAR");
        holder.storestatus.setText(productArrayList.get(position).getBranchStatus());

        if (productArrayList.get(position).getBranchStatus().equals("Open")){
            holder.storestatus.setTextColor(Color.GREEN);
        }
        else {
            holder.storestatus.setTextColor(Color.RED);
        }

//        if (productArrayList.get(pos).getReviews()==0){
//            holder.reviews.setText("("+productArrayList.get(pos).getReviews()+" No Reviews)");
//        }else {
//            holder.reviews.setText("("+productArrayList.get(pos).getReviews()+" Reviews)");
//        }

        if (productArrayList.get(position).getIsCashAllowed().equals("true")&&(productArrayList.get(position).getIsCreditCardAllowed().equals("true"))) {
            holder.paymentcard.setVisibility(View.VISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        }
        else if (productArrayList.get(position).getIsCreditCardAllowed().equals("false")){
            holder.paymentcard.setVisibility(View.INVISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        }
        else if (productArrayList.get(position).getIsCashAllowed().equals("false")){
            holder.paymentcash.setVisibility(View.INVISIBLE);
            holder.paymentcard.setVisibility(View.VISIBLE);
        }

        Glide.with(context)
                .load(STORE_IMAGE_URL +productArrayList.get(0).getStoreImage())
                .into(holder.storeimage);

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: "+productArrayList.size());
        return productArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, storerating, reviews, storestatus, distance,minoder,deliverytime;
        ImageView storeimage,paymentcard,paymentcash,statusimage;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.mainstore);
            storerating = (TextView) itemView.findViewById(R.id.revies_count);
            distance = (TextView) itemView.findViewById(R.id.distance);
            reviews = (TextView) itemView.findViewById(R.id.revies);
            storestatus = (TextView)itemView.findViewById(R.id.storestatus);
            minoder = (TextView)itemView.findViewById(R.id.minmodr);
            deliverytime = (TextView)itemView.findViewById(R.id.delivery_time);
            storeimage = (ImageView)itemView.findViewById(R.id.storeimage);
            storelayout =(LinearLayout)itemView.findViewById(R.id.storelayout);
            paymentcard=(ImageView)itemView.findViewById(R.id.payment_card);
            paymentcash=(ImageView)itemView.findViewById(R.id.payment_cash);
            statusimage=(ImageView)itemView.findViewById(R.id.storestatusimage) ;


            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(productArrayList.size()> 0) {
                        Intent intent = new Intent(context, ProductStoresActivity.class);
                        intent.putExtra("stores", productArrayList);
                        intent.putExtra("pos",getAdapterPosition());
                        context.startActivity(intent);
                    }
                    else {
                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }
}
