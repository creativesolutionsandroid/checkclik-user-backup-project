package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class  ProductDealsAdapter extends RecyclerView.Adapter< ProductDealsAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    //    private int selectedPosition = 0;

    private ArrayList<DealsResponce.Deals> storeArrayList = new ArrayList<>();

    private Activity activity;

    public ProductDealsAdapter(Context context, ArrayList<DealsResponce.Deals> storesArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dealsadapter, parent, false);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        DealsResponce.Deals storeDetails = storeArrayList.get(position);
        holder.iteamname.setText(storeDetails.getNameEn());
        holder.discount.setText(""+storeDetails.getDiscountValue()+"% OFF");
        holder.mrpprice.setText(""+"SAR  "+priceFormat.format(storeDetails.getSellingPrice()));
        holder.ourprice.setText(""+"SAR  "+priceFormat.format(storeDetails.getPrice()));



        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL +storeArrayList.get(position).getImage())
                .into(holder.store_image);
        Log.d(TAG, "onBindViewHolder "+Constants.STORE_IMAGE_URL +storeArrayList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView discount,iteamname,mrpprice,ourprice;
        ImageView store_image;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);


            discount = (TextView) itemView.findViewById(R.id.offer);
            iteamname = (TextView) itemView.findViewById(R.id.iteamname);
            mrpprice = (TextView) itemView.findViewById(R.id.mrp);
            ourprice = (TextView) itemView.findViewById(R.id.ourtprice);
            store_image = (ImageView) itemView.findViewById(R.id.iteamimage);
            storelayout=(RelativeLayout)itemView.findViewById(R.id.storelayout) ;


//            storelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(storeArrayList.size()> 0) {
//                        Intent intent = new Intent(context, ProductItemListActivity.class);
//                        intent.putExtra("stores", storeArrayList);
//                        intent.putExtra("pos",getAdapterPosition());
//                        context.startActivity(intent);
//                    }
//                    else {
//                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

        }
    }
}

