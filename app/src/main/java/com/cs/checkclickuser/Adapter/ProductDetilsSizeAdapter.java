package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ProductDetilsSizeAdapter extends  RecyclerView.Adapter<ProductDetilsSizeAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<ProductVariantResponse.VariantsList> colorArrayList = new ArrayList<>();
    private Activity activity;
    int pos = 0;


    public ProductDetilsSizeAdapter(Context context, ArrayList<ProductVariantResponse.VariantsList> colorArrayList) {
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.colorArrayList = colorArrayList;
    }


    @Override
    public ProductDetilsSizeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_dailog_adapter, parent, false);
        return new ProductDetilsSizeAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ProductDetilsSizeAdapter.MyViewHolder holder, int position) {
        holder.red.setText(colorArrayList.get(position).getVariants());

    }

    @Override
    public int getItemCount() {
        return colorArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView red;


        public MyViewHolder(View itemView) {
            super(itemView);
            red = (TextView) itemView.findViewById(R.id.color);
        }
    }
}