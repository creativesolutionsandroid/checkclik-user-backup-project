package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Activites.ProductIteamListActivity;

import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductIteamAdapter extends RecyclerView.Adapter< ProductIteamAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    //    private int selectedPosition = 0;
    ProductstoreResponce.Data storeArrayList;
    private Activity activity;

    public ProductIteamAdapter(Context context,ProductstoreResponce.Data storesArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_iteam_list, parent, false);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        ProductstoreResponce.Data storeDetails = storeArrayList;
        holder.store_name.setText(storeDetails.getSubCategory().get(position).getNameEn());
        holder.products.setText(""+storeDetails.getSubCategory().get(position).getProductCount());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL+storeArrayList.getSubCategory().get(position).getImage())
                .into(holder.store_image);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.getSubCategory().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name,products;
        ImageView store_image;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            store_name = (TextView) itemView.findViewById(R.id.storename);
            products=(TextView)itemView.findViewById(R.id.products);
            store_image = (ImageView) itemView.findViewById(R.id.storeimage);
            storelayout= (LinearLayout)itemView.findViewById(R.id.layout);



        }
    }
}