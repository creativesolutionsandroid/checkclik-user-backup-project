package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Activites.ProductVariantsActivity;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class ProductItemListAdapter extends RecyclerView.Adapter< ProductItemListAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<ProductItemResponse.ProductList> storeArrayList = new ArrayList<>();
    private Activity activity;

    public ProductItemListAdapter(Context context, ArrayList<ProductItemResponse.ProductList> storesArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_product_items, parent, false);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.discountedPrice.setPaintFlags(holder.discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if(storeArrayList.get(position).getDiscounttype() == 1) {
            holder.discount_percent.setText((int)storeArrayList.get(position).getDiscountvalue()+"% Off");
        }
        else {
            holder.discount_percent.setText(storeArrayList.get(position).getDiscountvalue()+" Off");
        }
        holder.title.setText(storeArrayList.get(position).getProductnameen());
        holder.discountedPrice.setText("SAR "+Constants.priceFormat1.format(storeArrayList.get(position).getSellingprice()));
        holder.price.setText("SAR "+Constants.priceFormat1.format(storeArrayList.get(position).getPrice()));

        if(storeArrayList.get(position).getSellingprice() == storeArrayList.get(position).getPrice()){
            holder.discountLayout.setVisibility(View.INVISIBLE);
            holder.discountedPrice.setVisibility(View.INVISIBLE);
        }
        else {
            holder.discountLayout.setVisibility(View.VISIBLE);
            holder.discountedPrice.setVisibility(View.VISIBLE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + storeArrayList.get(position).getImage())
                .into(holder.productImage);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView discount_percent;
        TextView title;
        TextView discountedPrice;
        TextView price;
        RelativeLayout discountLayout, mainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            productImage = (ImageView) itemView.findViewById(R.id.image);
            discount_percent = (TextView) itemView.findViewById(R.id.discount_percent);
            title = (TextView) itemView.findViewById(R.id.title);
            discountedPrice = (TextView) itemView.findViewById(R.id.discounted_price);
            price = (TextView) itemView.findViewById(R.id.price);
            discountLayout = (RelativeLayout) itemView.findViewById(R.id.discount_layout);
            mainLayout = (RelativeLayout) itemView.findViewById(R.id.main_layout);

            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductVariantsActivity.class);
                    intent.putExtra("ProductInventoryId", storeArrayList.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });
        }
    }
}