package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Activites.ProductItemCategoriesActivity;
import com.cs.checkclickuser.Activites.ProductItemListActivity;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class  ProductShopAdapter extends RecyclerView.Adapter< ProductShopAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ProductstoreResponce.Data storeArrayList;

    public ProductShopAdapter(Context context, ProductstoreResponce.Data storesArrayList){
        this.context = context;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shopadapter, parent, false);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        ProductstoreResponce.MainCategory storeDetails = storeArrayList.getMainCategory().get(position);
        holder.store_name.setText(storeDetails.getNameEn());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL +storeDetails.getImage())
                .into(holder.store_image);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.getMainCategory().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name;
        ImageView store_image;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            store_name = (TextView) itemView.findViewById(R.id.stoename);
            store_image = (ImageView) itemView.findViewById(R.id.storepic);
            storelayout= (RelativeLayout)itemView.findViewById(R.id.storelayout);

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductItemCategoriesActivity.class);
                    intent.putExtra("stores", storeArrayList);
                    intent.putExtra("pos",getAdapterPosition());
                    context.startActivity(intent);
                }
            });


        }
    }
}
