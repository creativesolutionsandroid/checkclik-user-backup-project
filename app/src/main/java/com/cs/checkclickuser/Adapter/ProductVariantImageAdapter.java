package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class ProductVariantImageAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<ProductVariantResponse.JImages> bannersList;
    Context context;

    public ProductVariantImageAdapter(Context context, ArrayList<ProductVariantResponse.JImages> bannersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannersList = bannersList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return (bannersList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_variants_image, container, false);

        ImageView banner_image = (ImageView) itemView.findViewById(R.id.image);

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + bannersList.get(position).getImage())
                .into(banner_image);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
