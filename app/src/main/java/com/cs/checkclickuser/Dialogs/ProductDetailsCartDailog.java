package com.cs.checkclickuser.Dialogs;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;


import com.cs.checkclickuser.Adapter.ProductDetilsSizeAdapter;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ProductDetailsCartDailog extends BottomSheetDialogFragment implements View.OnClickListener {
    View rootView;
    private static String TAG = "TAG";
    RecyclerView listview;
    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    ProductDetilsSizeAdapter mSizeAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.productdetails_dailog, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

//        variantsLists = (ArrayList<ProductVariantResponse.VariantsList>)geta.getSerializableExtra("stores");

        listview=(RecyclerView)rootView.findViewById(R.id.listview);

        mSizeAdapter = new ProductDetilsSizeAdapter(getActivity(), variantsLists);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        listview.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        listview.setAdapter(mSizeAdapter);

              return rootView;
    }

    @Override
    public void onClick(View v) {

    }
}
