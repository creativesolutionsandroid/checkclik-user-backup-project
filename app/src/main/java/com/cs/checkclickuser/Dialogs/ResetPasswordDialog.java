package com.cs.checkclickuser.Dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.cs.checkclickuser.Activites.ForgotPasswordActivity;
import com.cs.checkclickuser.Models.ResetpasswordResponce;
import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    private int currentStage = 1;
    TextInputLayout inputLayoutphone, inputLayoutConfirmPassword,inputLayoutnewPassword;
    EditText  inputPassword,inputConfirmPassword;
    String strPassword, strConfirmPassword, otp;
    Button buttonSend;
    String strMobile;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

            rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getArguments().getString("mobile");
        otp = getArguments().getString("otp");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword= (EditText) rootView.findViewById(R.id.reset_input_password);
        inputConfirmPassword= (EditText) rootView.findViewById(R.id.reset_input_retype_password);
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                if(currentStage == 1){
//                    finish();
                }

            case R.id.reset_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    }
                    else{
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){

            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20){
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutphone.setErrorEnabled(false);
        inputLayoutphone.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutnewPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("MobileNo","966"+strMobile);
            parentObj.put("Password",strPassword);
            parentObj.put("OTPCode", otp);
           parentObj.put( "FlagId",2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ResetpasswordResponce> call = apiService.resetPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ResetpasswordResponce>() {
                @Override
                public void onResponse(Call<ResetpasswordResponce> call, Response<ResetpasswordResponce> response) {
                    Log.d(TAG, "resetonResponse: "+response);
                    if(response.isSuccessful()){
                        ResetpasswordResponce resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getMobileNo();
                                userPrefsEditor.putString("userId", userId);
                                userPrefsEditor.putString("MobileNo", resetPasswordResponse.getData().getMobileNo());
                                userPrefsEditor.commit();
                                Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                ForgotPasswordActivity.isResetSuccessful = true;
                                getDialog().dismiss();
                            }
                            else {
//                                status false case
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResetpasswordResponce> call, Throwable t) {
                    Log.d(TAG, "resetonFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
}

