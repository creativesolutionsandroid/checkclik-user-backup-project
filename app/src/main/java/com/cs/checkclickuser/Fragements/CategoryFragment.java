package com.cs.checkclickuser.Fragements;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.EditProfileActivity;
import com.cs.checkclickuser.Adapter.CatAdapter;
import com.cs.checkclickuser.Models.Catresponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends Fragment {

    private RecyclerView catlist;
    View rootView;
    public static final String TAG = "TAG";
    CatAdapter mCatAdapter;
    private ArrayList<Catresponce> catItemsArrayList = new ArrayList<>();
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    String catId,catname;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.category_fragment, container, false);

        catlist = (RecyclerView)rootView.findViewById(R.id.catlist);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        catlist.setLayoutManager(mLayoutManager);
        catlist.setAdapter(mCatAdapter);

        new categoryApi().execute();


        return rootView;
    }


    private class categoryApi extends AsyncTask<String, String, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Catresponce> call = apiService.catlist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Catresponce>() {
                @Override
                public void onResponse(Call<Catresponce> call, Response<Catresponce> response) {
                    if (response.isSuccessful()) {
                        Catresponce Catresponce = response.body();
                        try {
                            if (Catresponce.getStatus()) {

                            } else {
                                String failureResponse = Catresponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<Catresponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {
//
            parentObj.put("CategoryId", userPrefs.getString("catid"," "));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }


}
