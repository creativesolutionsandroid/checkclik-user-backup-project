package com.cs.checkclickuser.Fragements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Adapter.HomeScreenBannersAdapter;
import com.cs.checkclickuser.Adapter.HomeScreenOffersAdapter;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.github.demono.AutoScrollViewPager;

import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    View rootView;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    ImageView product,service,home,orders,profile,cart ;
    ViewPager offerViewPager;
    AutoScrollViewPager bannersViewPager;
    HomePageResponse homePageResponse;
    HomeScreenOffersAdapter mOfferAdapter;
    HomeScreenBannersAdapter mBannerAdapter;
    AlertDialog customDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        rootView = inflater.inflate(R.layout.home_fragment, container, false);

        offerViewPager = (ViewPager) rootView.findViewById(R.id.home_offers_list);
        bannersViewPager = (AutoScrollViewPager) rootView.findViewById(R.id.home_banner_list);
        cart =(ImageView) rootView.findViewById(R.id.home_cart);

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), CartActivity.class);
                startActivity(intent);
            }
        });

        new GetHomeDataApi().execute();

        return rootView;
    }

    private class GetHomeDataApi extends AsyncTask<String, Integer, String> {

        String inputStr;
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(getActivity())
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
//            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HomePageResponse> call = apiService.getHomePageDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HomePageResponse>() {
                @Override
                public void onResponse(Call<HomePageResponse> call, Response<HomePageResponse> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        homePageResponse = response.body();
                    }

                    if(homePageResponse.getData().getOffers().size() > 0 || homePageResponse.getData().getOffers2().size() > 0) {
                        mOfferAdapter = new HomeScreenOffersAdapter(getContext(), homePageResponse.getData().getOffers(),
                                homePageResponse.getData().getOffers2());
                        offerViewPager.setAdapter(mOfferAdapter);
                    }

                    mBannerAdapter = new HomeScreenBannersAdapter(getContext(), homePageResponse.getData().getHomebanners());
                    bannersViewPager.setAdapter(mBannerAdapter);
                    bannersViewPager.setCycle(true);
                    bannersViewPager.setSlideDuration(5000);
                    bannersViewPager.startAutoScroll();
//                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<HomePageResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
//                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
//        try {
//            parentObj.put("BranchId",  MainCategory.get(storePos).getBranchId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    public void showLoadingDialog(Activity context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.loading_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void closeLoadingDialog(){
        Log.d("TAG", "closeLoadingDialog: ");
        if(customDialog != null) {
            Log.d("TAG", "closeLoadingDialog: "+customDialog);
            customDialog.dismiss();
        }
    }
}
