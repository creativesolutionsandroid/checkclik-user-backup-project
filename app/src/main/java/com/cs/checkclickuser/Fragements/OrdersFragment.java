package com.cs.checkclickuser.Fragements;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Adapter.OrderHistoryAdapter;
import com.cs.checkclickuser.Adapter.OrderPendingAdapter;
import com.cs.checkclickuser.Adapter.OrderProcessingAdapter;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Fragements.CategoryFragment.TAG;

public class OrdersFragment extends Fragment {

    View rootView;
    SharedPreferences languagePrefs;
    RelativeLayout pendinglayout,processinglayout,historylayout;
    View pendingline,processingline,historyline;
    RecyclerView listview ;
    private ArrayList<OrderPendingResponce.Data>orderpendigarraylit=new ArrayList<>();
    private OrderPendingAdapter mPendingAdapter;
    private OrderHistoryAdapter mHistoryAdapter;
    private OrderProcessingAdapter mProcessingAdpter ;
    String  userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        rootView = inflater.inflate(R.layout.order_fragment, container, false);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);


        pendinglayout=(RelativeLayout)rootView.findViewById(R.id.pendinglayout);
        processinglayout=(RelativeLayout)rootView.findViewById(R.id.processinglayout);
        historylayout=(RelativeLayout)rootView.findViewById(R.id.historylayout);
        listview=(RecyclerView)rootView.findViewById(R.id.list_item);


        pendingline=(View)rootView.findViewById(R.id.pendingline);
        processingline=(View)rootView.findViewById(R.id.processingline);
        historyline=(View)rootView.findViewById(R.id.historyline);

        pendinglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                listview.setLayoutManager(mLayoutManager);
                mPendingAdapter = new OrderPendingAdapter(getContext(),orderpendigarraylit);
                listview.setAdapter(mPendingAdapter);

                pendingline.setVisibility(View.VISIBLE);
                processingline.setVisibility(View.INVISIBLE);
                historyline.setVisibility(View.INVISIBLE);
                new GetpendingApi().execute(userId);
            }
        });

        processinglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                listview.setLayoutManager(mLayoutManager);
                mProcessingAdpter = new OrderProcessingAdapter(getContext(),orderpendigarraylit);
                listview.setAdapter(mProcessingAdpter);

                pendingline.setVisibility(View.INVISIBLE);
                processingline.setVisibility(View.VISIBLE);
                historyline.setVisibility(View.INVISIBLE);
                new GetprocessingApi().execute(userId);
            }
        });

        historylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                listview.setLayoutManager(mLayoutManager);
                mHistoryAdapter = new OrderHistoryAdapter(getContext(),orderpendigarraylit);
                listview.setAdapter(mHistoryAdapter);

                pendingline.setVisibility(View.INVISIBLE);
                processingline.setVisibility(View.INVISIBLE);
                historyline.setVisibility(View.VISIBLE);
                new GethistoryApi().execute(userId);
            }
        });

        new GetpendingApi().execute(userId);
        pendingline.setVisibility(View.VISIBLE);
        return rootView;
    }

    private class GetpendingApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<OrderPendingResponce> call = apiService.getorderpending(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderPendingResponce>() {
                @Override
                public void onResponse(Call<OrderPendingResponce> call, Response<OrderPendingResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);
                    OrderPendingResponce SaveuserRatingResponce = response.body();
                    if (response.isSuccessful()) {
                        OrderPendingResponce stores = response.body();
                        if (stores.getStatus()) {
                            orderpendigarraylit = stores.getData();
                        }
                        else {
                            String failureResponse = SaveuserRatingResponce.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());

                        }
                        Log.d(TAG, "arry list size 1" + orderpendigarraylit.size());
                    }

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    listview.setLayoutManager(mLayoutManager);
                    mPendingAdapter = new OrderPendingAdapter(getContext(),orderpendigarraylit);
                    listview.setAdapter(mPendingAdapter);

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<OrderPendingResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();

                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Type", 1) ;
            parentObj.put("Id", 13) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }


    /////////////Processing moudel///////////////

    private class GetprocessingApi extends AsyncTask<String, Integer, String> {
        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareprocessingJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<OrderPendingResponce> call = apiService.getorderpending(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderPendingResponce>() {
                @Override
                public void onResponse(Call<OrderPendingResponce> call, Response<OrderPendingResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);
                    OrderPendingResponce SaveuserRatingResponce = response.body();
                    if (response.isSuccessful()) {
                        OrderPendingResponce stores = response.body();

                        if (stores.getStatus()) {
                            orderpendigarraylit = stores.getData();
                        }
                        Log.d(TAG, "arry list size 1" + orderpendigarraylit.size());
                    }
                    else {
                        String failureResponse = SaveuserRatingResponce.getMessage();
                        Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                getResources().getString(R.string.ok), getActivity());

                    }

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    listview.setLayoutManager(mLayoutManager);
                    mProcessingAdpter = new OrderProcessingAdapter(getContext(),orderpendigarraylit);
                    listview.setAdapter(mProcessingAdpter);

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderPendingResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareprocessingJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Type", 2) ;
            parentObj.put("Id", 13) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }


    /////////////////// History moudel ////////////////

    private class GethistoryApi extends AsyncTask<String, Integer, String> {
        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparehistoryJSON();
           Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<OrderPendingResponce> call = apiService.getorderpending(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderPendingResponce>() {
                @Override
                public void onResponse(Call<OrderPendingResponce> call, Response<OrderPendingResponce> response) {
                    Log.d(TAG, "product servies responce: "+ response);
                    OrderPendingResponce SaveuserRatingResponce = response.body();
                    if (response.isSuccessful()) {
                        OrderPendingResponce stores = response.body();

                        if (stores.getStatus()) {
                            orderpendigarraylit = stores.getData();
                        }
                    }
                    else {
                        String failureResponse = SaveuserRatingResponce.getMessage();
                        Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                getResources().getString(R.string.ok), getActivity());

                    }


                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    listview.setLayoutManager(mLayoutManager);
                    mHistoryAdapter = new OrderHistoryAdapter(getContext(),orderpendigarraylit);
                    listview.setAdapter(mHistoryAdapter);

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderPendingResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String preparehistoryJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Type", 3) ;
            parentObj.put("Id", 13) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

}
