package com.cs.checkclickuser.Fragements;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.FillterActivity;
import com.cs.checkclickuser.Adapter.ProductAdapter;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.GPSTracker;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Product_fragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    View rootView;
    SharedPreferences languagePrefs;
    SupportMapFragment mapFragment;
    private ImageView back_btn,fillter,cart,notification,paymentcash,paymentcard;
    TextView cattext;
    private LinearLayout locationLayout;
    EditText seachbar;
    private RecyclerView productlist;
    private ProductAdapter mProductadapter;

    SharedPreferences userPrefs;
    private static int EDIT_REQUEST = 1;
    SharedPreferences.Editor userPrefsEditor;

    public static Double currentLatitude, currentLongitude;
    ArrayList<ProductlistResponce.Stores> storesArrayList = new ArrayList<>();
    ArrayList<ProductlistResponce.FilterMainCategory> MainCategory = new ArrayList<>();
    ArrayList<ProductlistResponce.FilterSubCategoryCat> SubCategoryArrayList = new ArrayList<>();
    GPSTracker gps;
    String TAG = "TAG";
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private FusedLocationProviderClient mFusedLocationClient;
    public static boolean isCurrentLocationSelected = true;
    private static final int LOCATION_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;

    public static ArrayList<Integer> filterid = new ArrayList<>();
    public static boolean filter = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.product_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        back_btn = (ImageView) rootView.findViewById(R.id.back_btn);
        fillter = (ImageView) rootView.findViewById(R.id.filter);
        cart  =(ImageView)rootView.findViewById(R.id.product_cart);
        notification =(ImageView)rootView.findViewById(R.id.product_noti);
        seachbar =(EditText)rootView.findViewById(R.id.product_search);
        locationLayout = (LinearLayout) rootView.findViewById(R.id.location_layout);
        productlist =(RecyclerView)rootView.findViewById(R.id.product_listview);
        cattext = (TextView)rootView.findViewById(R.id.product_category);
        paymentcash=(ImageView)rootView.findViewById(R.id.payment_cash);
        paymentcard=(ImageView)rootView.findViewById(R.id.payment_card);
        fillter =(ImageView) rootView.findViewById(R.id.filter);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cattext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment categoryFragment = new CategoryFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
            }
        });

        fillter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), FillterActivity.class);
                intent.putExtra("Fillterarry", MainCategory);
                intent.putExtra("Fillterarry1", SubCategoryArrayList);
                startActivity(intent);
            }
        });

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
            else {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            try {
                gps = new GPSTracker(getActivity());
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        locationLayout.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.location_layout:
                Intent intent = new Intent(getContext(), CategoryFragment.class);
                startActivityForResult(intent, ADDRESS_REQUEST);
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getContext(), perm));
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {
                if (isCurrentLocationSelected){
                    currentLatitude = gps.getLatitude();
                    currentLongitude = gps.getLongitude();
                    new GetProductApi().execute();

                    Log.i("TAG","fused lat "+currentLatitude);
                    Log.i("TAG","fused long "+currentLongitude);
                }


                try {

                    mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        currentLatitude = location.getLatitude();
                                        currentLongitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        if (!userPrefs.getString("catid", "").equals("")) {
//
//            FragmentManager fragmentManager = getFragmentManager();
//            Fragment categoryFragment = new CategoryFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
//
//            cattext.setText(userPrefs.getString("Categoryname","catname"));
//        }
//
//        else {
//
//            }
//
//
//        }


    private class GetProductApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductlistResponce> call = apiService.getstorslist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductlistResponce>() {
                @Override
                public void onResponse(Call<ProductlistResponce> call, Response<ProductlistResponce> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        ProductlistResponce stores = response.body();
                        if (stores.getStatus()) {
                            MainCategory =stores.getData().getFilterMainCategory();
                            SubCategoryArrayList=stores.getData().getFilterSubCategoryCat();
                            storesArrayList = stores.getData().getStores();
                            Log.d(TAG, "onResponse: "+storesArrayList.size());
                        }

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        productlist.setLayoutManager(mLayoutManager);
                        mProductadapter = new ProductAdapter(getContext(),storesArrayList);
                        productlist.setAdapter(mProductadapter);
                    }
                    else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductlistResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Latitude",  17.3918333);
            parentObj.put("Longitude",78.4644621);
            parentObj.put("CategoryId",2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }
}







