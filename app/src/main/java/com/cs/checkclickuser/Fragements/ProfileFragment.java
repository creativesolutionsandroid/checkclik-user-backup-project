package com.cs.checkclickuser.Fragements;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.FavoritesActivity;
import com.cs.checkclickuser.Activites.SettingActivity;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.checkclickuser.Fragements.CategoryFragment.TAG;
import static com.cs.checkclickuser.Utils.Constants.USERS_IMAGE_URL;

public class ProfileFragment extends Fragment {

    private static final int CAMERA_REQUEST = 1888;
    View rootView;
    ImageView settings;
    RelativeLayout favuorites,coupon;
    CircleImageView profilepic;
    TextView customername;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String strMobile, strUsername,strUserid;
    String pictureName = "";
    AlertDialog customDialog;

    String imageResponse;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    Bitmap thumbnail;
    Boolean isImageChanged = false;
    private DefaultHttpClient mHttpClient11;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile_fragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        strMobile = userPrefs.getString("mobile", "");
        pictureName = userPrefs.getString("pic", "");
        strUsername= userPrefs.getString("name","");


        settings=(ImageView)rootView.findViewById(R.id.settings);
        favuorites=(RelativeLayout)rootView.findViewById(R.id.favourites);
        profilepic = (CircleImageView)rootView. findViewById(R.id.ac_profilepic);
        coupon=(RelativeLayout)rootView.findViewById(R.id.couponicon);
        customername=(TextView)rootView.findViewById(R.id.customername);

        customername.setText(userPrefs.getString("name","  "  )+" "+userPrefs.getString("lastname",   " "));

        if(!userPrefs.getString("pic","").equalsIgnoreCase("")) {
            Glide.with(ProfileFragment.this)
                    .load(USERS_IMAGE_URL + userPrefs.getString("pic", ""))
                    .into(profilepic);
            Log.d(TAG, "profilepic: "+USERS_IMAGE_URL + userPrefs.getString("pic", ""));
        }
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
            }
        });

        favuorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FavoritesActivity.class);
                startActivity(intent);
            }
        });

        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new CouponVoucherFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_layout, mFragment).commit();
            }

        });
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });

        return rootView;
    }
    public void showtwoButtonsAlertDialog () {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog_camera;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel =(TextView) dialogView.findViewById(R.id.cancel_btn);

        title.setText("CheckClik");
        no.setText("Gallery");
        yes.setText("Camera");
        desc.setText("Do you want to upload form");
        cancel.setText("Cancel");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                customDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera=false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);

            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);

            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);

        }
    }


    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera=true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    private class uploadImagesApi extends AsyncTask<String, String, String> {

        String inputStr;
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... str) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());

            HttpResponse httpResponse = null;
            int count = 0, usedCount = 0;

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);
            HttpPost httppost = new HttpPost(USERS_IMAGE_URL);

            try{
                String imagePath1 = "";
                File file=null;
                if (thumbnail != null) {
                    imagePath1 = StoreByteImage(thumbnail, "back");
                }
                if (imagePath1 != null) {
                    file = new File(imagePath1);
                }
                if (null != file && file.exists()) {
                    multipartEntity.addPart("image", new FileBody(file));
                }
                httppost.setEntity(multipartEntity);
                httpResponse=  mHttpClient11.execute(httppost);
            }catch(Exception e){
                e.printStackTrace();
            }

            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO Auto-generated method stub
            super.onPostExecute(s);
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    new uploadImagesApi().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{

                Toast.makeText(getActivity(), "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        pictureName = "PIC_" + strUserid + "_" + timeStamp + ".jpg";

        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }


}
