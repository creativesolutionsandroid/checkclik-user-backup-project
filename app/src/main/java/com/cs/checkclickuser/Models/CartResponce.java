package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class CartResponce implements Serializable {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("ProductInventoryId")
    private int ProductInventoryId;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getProductInventoryId() {
        return ProductInventoryId;
    }

    public void setProductInventoryId(int ProductInventoryId) {
        this.ProductInventoryId = ProductInventoryId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("CouponList")
        private List<String> CouponList;
        @Expose
        @SerializedName("CartList")
        private List<CartList> CartList;

        public List<String> getCouponList() {
            return CouponList;
        }

        public void setCouponList(List<String> CouponList) {
            this.CouponList = CouponList;
        }

        public List<CartList> getCartList() {
            return CartList;
        }

        public void setCartList(List<CartList> CartList) {
            this.CartList = CartList;
        }
    }

    public static class CartList {
        @Expose
        @SerializedName("VoucherDiscount")
        private int VoucherDiscount;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private int MaxDiscountAmount;
        @Expose
        @SerializedName("CouponId")
        private int CouponId;
        @Expose
        @SerializedName("CheckOutAmount")
        private int CheckOutAmount;
        @Expose
        @SerializedName("BranchAddress")
        private String BranchAddress;
        @Expose
        @SerializedName("ServiceType")
        private int ServiceType;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("CartStatus")
        private int CartStatus;
        @Expose
        @SerializedName("AvailableQuantity")
        private int AvailableQuantity;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("VatAmountPerItem")
        private int VatAmountPerItem;
        @Expose
        @SerializedName("TotalVatAmount")
        private int TotalVatAmount;
        @Expose
        @SerializedName("TotalItemPrice")
        private double TotalItemPrice;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("NotReturnable")
        private boolean NotReturnable;
        @Expose
        @SerializedName("AllowedQty")
        private int AllowedQty;
        @Expose
        @SerializedName("MaxCartQty")
        private int MaxCartQty;
        @Expose
        @SerializedName("MinCartQty")
        private int MinCartQty;
        @Expose
        @SerializedName("ProductAvailabilityRange")
        private int ProductAvailabilityRange;
        @Expose
        @SerializedName("LowStockActivity")
        private int LowStockActivity;
        @Expose
        @SerializedName("DisplayStockQuantity")
        private boolean DisplayStockQuantity;
        @Expose
        @SerializedName("ReservedQuantity")
        private int ReservedQuantity;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("CartQuantity")
        private int CartQuantity;
        @Expose
        @SerializedName("Date")
        private String Date;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CartId")
        private int CartId;

        public int getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(int VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public int getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(int MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public int getCouponId() {
            return CouponId;
        }

        public void setCouponId(int CouponId) {
            this.CouponId = CouponId;
        }

        public int getCheckOutAmount() {
            return CheckOutAmount;
        }

        public void setCheckOutAmount(int CheckOutAmount) {
            this.CheckOutAmount = CheckOutAmount;
        }

        public String getBranchAddress() {
            return BranchAddress;
        }

        public void setBranchAddress(String BranchAddress) {
            this.BranchAddress = BranchAddress;
        }

        public int getServiceType() {
            return ServiceType;
        }

        public void setServiceType(int ServiceType) {
            this.ServiceType = ServiceType;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public int getCartStatus() {
            return CartStatus;
        }

        public void setCartStatus(int CartStatus) {
            this.CartStatus = CartStatus;
        }

        public int getAvailableQuantity() {
            return AvailableQuantity;
        }

        public void setAvailableQuantity(int AvailableQuantity) {
            this.AvailableQuantity = AvailableQuantity;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public int getVatAmountPerItem() {
            return VatAmountPerItem;
        }

        public void setVatAmountPerItem(int VatAmountPerItem) {
            this.VatAmountPerItem = VatAmountPerItem;
        }

        public int getTotalVatAmount() {
            return TotalVatAmount;
        }

        public void setTotalVatAmount(int TotalVatAmount) {
            this.TotalVatAmount = TotalVatAmount;
        }

        public double getTotalItemPrice() {
            return TotalItemPrice;
        }

        public void setTotalItemPrice(double TotalItemPrice) {
            this.TotalItemPrice = TotalItemPrice;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public boolean getNotReturnable() {
            return NotReturnable;
        }

        public void setNotReturnable(boolean NotReturnable) {
            this.NotReturnable = NotReturnable;
        }

        public int getAllowedQty() {
            return AllowedQty;
        }

        public void setAllowedQty(int AllowedQty) {
            this.AllowedQty = AllowedQty;
        }

        public int getMaxCartQty() {
            return MaxCartQty;
        }

        public void setMaxCartQty(int MaxCartQty) {
            this.MaxCartQty = MaxCartQty;
        }

        public int getMinCartQty() {
            return MinCartQty;
        }

        public void setMinCartQty(int MinCartQty) {
            this.MinCartQty = MinCartQty;
        }

        public int getProductAvailabilityRange() {
            return ProductAvailabilityRange;
        }

        public void setProductAvailabilityRange(int ProductAvailabilityRange) {
            this.ProductAvailabilityRange = ProductAvailabilityRange;
        }

        public int getLowStockActivity() {
            return LowStockActivity;
        }

        public void setLowStockActivity(int LowStockActivity) {
            this.LowStockActivity = LowStockActivity;
        }

        public boolean getDisplayStockQuantity() {
            return DisplayStockQuantity;
        }

        public void setDisplayStockQuantity(boolean DisplayStockQuantity) {
            this.DisplayStockQuantity = DisplayStockQuantity;
        }

        public int getReservedQuantity() {
            return ReservedQuantity;
        }

        public void setReservedQuantity(int ReservedQuantity) {
            this.ReservedQuantity = ReservedQuantity;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public int getCartQuantity() {
            return CartQuantity;
        }

        public void setCartQuantity(int CartQuantity) {
            this.CartQuantity = CartQuantity;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }
    }
}
