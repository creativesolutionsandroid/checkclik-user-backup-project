package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CityListResponce implements Serializable {

    @Expose
    @SerializedName("Longitude")
    private String Longitude;
    @Expose
    @SerializedName("Latitude")
    private String Latitude;
    @Expose
    @SerializedName("CountryAr")
    private String CountryAr;
    @Expose
    @SerializedName("CountryEn")
    private String CountryEn;
    @Expose
    @SerializedName("CountryId")
    private int CountryId;
    @Expose
    @SerializedName("NameAr")
    private String NameAr;
    @Expose
    @SerializedName("NameEn")
    private String NameEn;
    @Expose
    @SerializedName("Id")
    private int Id;

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getCountryAr() {
        return CountryAr;
    }

    public void setCountryAr(String countryAr) {
        CountryAr = countryAr;
    }

    public String getCountryEn() {
        return CountryEn;
    }

    public void setCountryEn(String countryEn) {
        CountryEn = countryEn;
    }

    public int getCountryId() {
        return CountryId;
    }

    public void setCountryId(int countryId) {
        CountryId = countryId;
    }

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String nameAr) {
        NameAr = nameAr;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String nameEn) {
        NameEn = nameEn;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
