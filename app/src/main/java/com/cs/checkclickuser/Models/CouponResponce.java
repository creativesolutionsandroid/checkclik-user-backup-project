package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public  class CouponResponce {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("RegionsAr")
        private String RegionsAr;
        @Expose
        @SerializedName("RegionsEn")
        private String RegionsEn;
        @Expose
        @SerializedName("CouponRegionsCount")
        private int CouponRegionsCount;
        @Expose
        @SerializedName("SelectedRegionsCount")
        private int SelectedRegionsCount;
        @Expose
        @SerializedName("PlanPrice")
        private int PlanPrice;
        @Expose
        @SerializedName("SelectedUsersCount")
        private int SelectedUsersCount;
        @Expose
        @SerializedName("CouponUserCount")
        private int CouponUserCount;
        @Expose
        @SerializedName("CouponNoOfRegions")
        private int CouponNoOfRegions;
        @Expose
        @SerializedName("CouponNoOfUsers")
        private int CouponNoOfUsers;
        @Expose
        @SerializedName("CouponDuration")
        private int CouponDuration;
        @Expose
        @SerializedName("NoOfCoupons")
        private int NoOfCoupons;
        @Expose
        @SerializedName("FlagId")
        private int FlagId;
        @Expose
        @SerializedName("IsDeleted")
        private boolean IsDeleted;
        @Expose
        @SerializedName("DeletedBy")
        private int DeletedBy;
        @Expose
        @SerializedName("ModifiedBy")
        private int ModifiedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("VoucherAmount")
        private int VoucherAmount;
        @Expose
        @SerializedName("NTimes")
        private int NTimes;
        @Expose
        @SerializedName("DiscountLimitation")
        private int DiscountLimitation;
        @Expose
        @SerializedName("CouponCode")
        private String CouponCode;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("IsCouponCodeRequires")
        private boolean IsCouponCodeRequires;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private int MaxDiscountAmount;
        @Expose
        @SerializedName("IsUptoApplicable")
        private boolean IsUptoApplicable;
        @Expose
        @SerializedName("VoucherDiscount")
        private int VoucherDiscount;
        @Expose
        @SerializedName("VoucherNameAr")
        private String VoucherNameAr;
        @Expose
        @SerializedName("VoucherNameEn")
        private String VoucherNameEn;
        @Expose
        @SerializedName("UptoUser")
        private int UptoUser;
        @Expose
        @SerializedName("UserSlabID")
        private int UserSlabID;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getRegionsAr() {
            return RegionsAr;
        }

        public void setRegionsAr(String RegionsAr) {
            this.RegionsAr = RegionsAr;
        }

        public String getRegionsEn() {
            return RegionsEn;
        }

        public void setRegionsEn(String RegionsEn) {
            this.RegionsEn = RegionsEn;
        }

        public int getCouponRegionsCount() {
            return CouponRegionsCount;
        }

        public void setCouponRegionsCount(int CouponRegionsCount) {
            this.CouponRegionsCount = CouponRegionsCount;
        }

        public int getSelectedRegionsCount() {
            return SelectedRegionsCount;
        }

        public void setSelectedRegionsCount(int SelectedRegionsCount) {
            this.SelectedRegionsCount = SelectedRegionsCount;
        }

        public int getPlanPrice() {
            return PlanPrice;
        }

        public void setPlanPrice(int PlanPrice) {
            this.PlanPrice = PlanPrice;
        }

        public int getSelectedUsersCount() {
            return SelectedUsersCount;
        }

        public void setSelectedUsersCount(int SelectedUsersCount) {
            this.SelectedUsersCount = SelectedUsersCount;
        }

        public int getCouponUserCount() {
            return CouponUserCount;
        }

        public void setCouponUserCount(int CouponUserCount) {
            this.CouponUserCount = CouponUserCount;
        }

        public int getCouponNoOfRegions() {
            return CouponNoOfRegions;
        }

        public void setCouponNoOfRegions(int CouponNoOfRegions) {
            this.CouponNoOfRegions = CouponNoOfRegions;
        }

        public int getCouponNoOfUsers() {
            return CouponNoOfUsers;
        }

        public void setCouponNoOfUsers(int CouponNoOfUsers) {
            this.CouponNoOfUsers = CouponNoOfUsers;
        }

        public int getCouponDuration() {
            return CouponDuration;
        }

        public void setCouponDuration(int CouponDuration) {
            this.CouponDuration = CouponDuration;
        }

        public int getNoOfCoupons() {
            return NoOfCoupons;
        }

        public void setNoOfCoupons(int NoOfCoupons) {
            this.NoOfCoupons = NoOfCoupons;
        }

        public int getFlagId() {
            return FlagId;
        }

        public void setFlagId(int FlagId) {
            this.FlagId = FlagId;
        }

        public boolean getIsDeleted() {
            return IsDeleted;
        }

        public void setIsDeleted(boolean IsDeleted) {
            this.IsDeleted = IsDeleted;
        }

        public int getDeletedBy() {
            return DeletedBy;
        }

        public void setDeletedBy(int DeletedBy) {
            this.DeletedBy = DeletedBy;
        }

        public int getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(int ModifiedBy) {
            this.ModifiedBy = ModifiedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public int getVoucherAmount() {
            return VoucherAmount;
        }

        public void setVoucherAmount(int VoucherAmount) {
            this.VoucherAmount = VoucherAmount;
        }

        public int getNTimes() {
            return NTimes;
        }

        public void setNTimes(int NTimes) {
            this.NTimes = NTimes;
        }

        public int getDiscountLimitation() {
            return DiscountLimitation;
        }

        public void setDiscountLimitation(int DiscountLimitation) {
            this.DiscountLimitation = DiscountLimitation;
        }

        public String getCouponCode() {
            return CouponCode;
        }

        public void setCouponCode(String CouponCode) {
            this.CouponCode = CouponCode;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public boolean getIsCouponCodeRequires() {
            return IsCouponCodeRequires;
        }

        public void setIsCouponCodeRequires(boolean IsCouponCodeRequires) {
            this.IsCouponCodeRequires = IsCouponCodeRequires;
        }

        public int getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(int MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public boolean getIsUptoApplicable() {
            return IsUptoApplicable;
        }

        public void setIsUptoApplicable(boolean IsUptoApplicable) {
            this.IsUptoApplicable = IsUptoApplicable;
        }

        public int getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(int VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public String getVoucherNameAr() {
            return VoucherNameAr;
        }

        public void setVoucherNameAr(String VoucherNameAr) {
            this.VoucherNameAr = VoucherNameAr;
        }

        public String getVoucherNameEn() {
            return VoucherNameEn;
        }

        public void setVoucherNameEn(String VoucherNameEn) {
            this.VoucherNameEn = VoucherNameEn;
        }

        public int getUptoUser() {
            return UptoUser;
        }

        public void setUptoUser(int UptoUser) {
            this.UptoUser = UptoUser;
        }

        public int getUserSlabID() {
            return UserSlabID;
        }

        public void setUserSlabID(int UserSlabID) {
            this.UserSlabID = UserSlabID;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
