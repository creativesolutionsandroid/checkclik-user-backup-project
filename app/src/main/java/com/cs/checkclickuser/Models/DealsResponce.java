package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public  class DealsResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("TotalCount")
        private ArrayList<TotalCount> TotalCount;
        @Expose
        @SerializedName("Deals")
        private ArrayList<Deals> Deals;
        @Expose
        @SerializedName("Branch")
        private int Branch;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;

        public ArrayList<TotalCount> getTotalCount() {
            return TotalCount;
        }

        public void setTotalCount(ArrayList<TotalCount> TotalCount) {
            this.TotalCount = TotalCount;
        }

        public ArrayList<Deals> getDeals() {
            return Deals;
        }

        public void setDeals(ArrayList<Deals> Deals) {
            this.Deals = Deals;
        }

        public int getBranch() {
            return Branch;
        }

        public void setBranch(int Branch) {
            this.Branch = Branch;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }
    }

    public static class TotalCount {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }
    }

    public static class Deals {
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<JProductVariant> JProductVariant;
        @Expose
        @SerializedName("ProductVariant")
        private String ProductVariant;
        @Expose
        @SerializedName("Condition")
        private int Condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String UPCBarcode;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("DiscountValue")
        private double DiscountValue;
        @Expose
        @SerializedName("DiscountType")
        private int DiscountType;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("SellingPrice")
        private int SellingPrice;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int BranchSubCategoryId;
        @Expose
        @SerializedName("BranchCategoryId")
        private int BranchCategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("rowNum")
        private int rowNum;

        public ArrayList<JProductVariant> getJProductVariant() {
            return JProductVariant;
        }

        public void setJProductVariant(ArrayList<JProductVariant> JProductVariant) {
            this.JProductVariant = JProductVariant;
        }

        public String getProductVariant() {
            return ProductVariant;
        }

        public void setProductVariant(String ProductVariant) {
            this.ProductVariant = ProductVariant;
        }

        public int getCondition() {
            return Condition;
        }

        public void setCondition(int Condition) {
            this.Condition = Condition;
        }

        public String getUPCBarcode() {
            return UPCBarcode;
        }

        public void setUPCBarcode(String UPCBarcode) {
            this.UPCBarcode = UPCBarcode;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public double getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(double DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getDiscountType() {
            return DiscountType;
        }

        public void setDiscountType(int DiscountType) {
            this.DiscountType = DiscountType;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public int getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(int SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public int getBranchSubCategoryId() {
            return BranchSubCategoryId;
        }

        public void setBranchSubCategoryId(int BranchSubCategoryId) {
            this.BranchSubCategoryId = BranchSubCategoryId;
        }

        public int getBranchCategoryId() {
            return BranchCategoryId;
        }

        public void setBranchCategoryId(int BranchCategoryId) {
            this.BranchCategoryId = BranchCategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }
    }

    public static class JProductVariant {
        @Expose
        @SerializedName("ValueAr")
        private String ValueAr;
        @Expose
        @SerializedName("ValueEn")
        private String ValueEn;
        @Expose
        @SerializedName("OptionId")
        private int OptionId;
        @Expose
        @SerializedName("ProductInventoryId")
        private int ProductInventoryId;

        public String getValueAr() {
            return ValueAr;
        }

        public void setValueAr(String ValueAr) {
            this.ValueAr = ValueAr;
        }

        public String getValueEn() {
            return ValueEn;
        }

        public void setValueEn(String ValueEn) {
            this.ValueEn = ValueEn;
        }

        public int getOptionId() {
            return OptionId;
        }

        public void setOptionId(int OptionId) {
            this.OptionId = OptionId;
        }

        public int getProductInventoryId() {
            return ProductInventoryId;
        }

        public void setProductInventoryId(int ProductInventoryId) {
            this.ProductInventoryId = ProductInventoryId;
        }
    }
}
