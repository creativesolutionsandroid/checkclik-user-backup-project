package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.ArrayList;


public  class FavStoresResponce implements Serializable {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data  implements Serializable  {
        @Expose
        @SerializedName("StoreList")
        private ArrayList<StoreList> StoreList;
        @Expose
        @SerializedName("ServiceList")
        private ArrayList<ServiceList> ServiceList;
        @Expose
        @SerializedName("ProductList")
        private ArrayList<ProductList> ProductList;

        public ArrayList<StoreList> getStoreList() {
            return StoreList;
        }

        public void setStoreList(ArrayList<StoreList> StoreList) {
            this.StoreList = StoreList;
        }

        public ArrayList<ServiceList> getServiceList() {
            return ServiceList;
        }

        public void setServiceList(ArrayList<ServiceList> ServiceList) {
            this.ServiceList = ServiceList;
        }

        public ArrayList<ProductList> getProductList() {
            return ProductList;
        }

        public void setProductList(ArrayList<ProductList> ProductList) {
            this.ProductList = ProductList;
        }
    }

    public static class StoreList  implements Serializable  {
        @Expose
        @SerializedName("rn")
        private int rn;
        @Expose
        @SerializedName("Shift")
        private String Shift;
        @Expose
        @SerializedName("Distance")
        private int Distance;
        @Expose
        @SerializedName("FavoriteId")
        private int FavoriteId;
        @Expose
        @SerializedName("VendorType")
        private int VendorType;
        @Expose
        @SerializedName("StoreType")
        private int StoreType;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Weekday")
        private int Weekday;
        @Expose
        @SerializedName("EndTime")
        private String EndTime;
        @Expose
        @SerializedName("StartTime")
        private String StartTime;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("Background")
        private String Background;
        @Expose
        @SerializedName("Maroof")
        private String Maroof;
        @Expose
        @SerializedName("Youtube")
        private String Youtube;
        @Expose
        @SerializedName("Snapchat")
        private String Snapchat;
        @Expose
        @SerializedName("Instagram")
        private String Instagram;
        @Expose
        @SerializedName("LinkedIn")
        private String LinkedIn;
        @Expose
        @SerializedName("TwitterHandle")
        private String TwitterHandle;
        @Expose
        @SerializedName("Facebook")
        private String Facebook;
        @Expose
        @SerializedName("BusinessSince")
        private String BusinessSince;
        @Expose
        @SerializedName("IsDelivery")
        private boolean IsDelivery;
        @Expose
        @SerializedName("InStoreService")
        private boolean InStoreService;
        @Expose
        @SerializedName("ShippingProvider")
        private int ShippingProvider;
        @Expose
        @SerializedName("OrderDone")
        private int OrderDone;
        @Expose
        @SerializedName("BranchColor")
        private String BranchColor;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("IsCreditCardAllowed")
        private String IsCreditCardAllowed;
        @Expose
        @SerializedName("IsCashAllowed")
        private String IsCashAllowed;
        @Expose
        @SerializedName("DeliveryTime")
        private String DeliveryTime;
        @Expose
        @SerializedName("Ratings")
        private int Ratings;
        @Expose
        @SerializedName("ReviewsCount")
        private int ReviewsCount;
        @Expose
        @SerializedName("Reviews")
        private int Reviews;
        @Expose
        @SerializedName("CategoryAr")
        private String CategoryAr;
        @Expose
        @SerializedName("CategoryEn")
        private String CategoryEn;
        @Expose
        @SerializedName("TermsAndConditionsAr")
        private String TermsAndConditionsAr;
        @Expose
        @SerializedName("TermsAndConditionsEn")
        private String TermsAndConditionsEn;
        @Expose
        @SerializedName("StoreImage")
        private String StoreImage;
        @Expose
        @SerializedName("BranchLogoImage")
        private String BranchLogoImage;
        @Expose
        @SerializedName("BackgroundImage")
        private String BackgroundImage;
        @Expose
        @SerializedName("MinimumOrderValue")
        private int MinimumOrderValue;
        @Expose
        @SerializedName("StoreAr")
        private String StoreAr;
        @Expose
        @SerializedName("StoreEn")
        private String StoreEn;
        @Expose
        @SerializedName("BranchAr")
        private String BranchAr;
        @Expose
        @SerializedName("BranchEn")
        private String BranchEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("BranchStatus")
        private String BranchStatus;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("ActualDate")
        private String ActualDate;

        public int getRn() {
            return rn;
        }

        public void setRn(int rn) {
            this.rn = rn;
        }

        public String getShift() {
            return Shift;
        }

        public void setShift(String Shift) {
            this.Shift = Shift;
        }

        public int getDistance() {
            return Distance;
        }

        public void setDistance(int Distance) {
            this.Distance = Distance;
        }

        public int getFavoriteId() {
            return FavoriteId;
        }

        public void setFavoriteId(int FavoriteId) {
            this.FavoriteId = FavoriteId;
        }

        public int getVendorType() {
            return VendorType;
        }

        public void setVendorType(int VendorType) {
            this.VendorType = VendorType;
        }

        public int getStoreType() {
            return StoreType;
        }

        public void setStoreType(int StoreType) {
            this.StoreType = StoreType;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public int getWeekday() {
            return Weekday;
        }

        public void setWeekday(int Weekday) {
            this.Weekday = Weekday;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String Logo) {
            this.Logo = Logo;
        }

        public String getBackground() {
            return Background;
        }

        public void setBackground(String Background) {
            this.Background = Background;
        }

        public String getMaroof() {
            return Maroof;
        }

        public void setMaroof(String Maroof) {
            this.Maroof = Maroof;
        }

        public String getYoutube() {
            return Youtube;
        }

        public void setYoutube(String Youtube) {
            this.Youtube = Youtube;
        }

        public String getSnapchat() {
            return Snapchat;
        }

        public void setSnapchat(String Snapchat) {
            this.Snapchat = Snapchat;
        }

        public String getInstagram() {
            return Instagram;
        }

        public void setInstagram(String Instagram) {
            this.Instagram = Instagram;
        }

        public String getLinkedIn() {
            return LinkedIn;
        }

        public void setLinkedIn(String LinkedIn) {
            this.LinkedIn = LinkedIn;
        }

        public String getTwitterHandle() {
            return TwitterHandle;
        }

        public void setTwitterHandle(String TwitterHandle) {
            this.TwitterHandle = TwitterHandle;
        }

        public String getFacebook() {
            return Facebook;
        }

        public void setFacebook(String Facebook) {
            this.Facebook = Facebook;
        }

        public String getBusinessSince() {
            return BusinessSince;
        }

        public void setBusinessSince(String BusinessSince) {
            this.BusinessSince = BusinessSince;
        }

        public boolean getIsDelivery() {
            return IsDelivery;
        }

        public void setIsDelivery(boolean IsDelivery) {
            this.IsDelivery = IsDelivery;
        }

        public boolean getInStoreService() {
            return InStoreService;
        }

        public void setInStoreService(boolean InStoreService) {
            this.InStoreService = InStoreService;
        }

        public int getShippingProvider() {
            return ShippingProvider;
        }

        public void setShippingProvider(int ShippingProvider) {
            this.ShippingProvider = ShippingProvider;
        }

        public int getOrderDone() {
            return OrderDone;
        }

        public void setOrderDone(int OrderDone) {
            this.OrderDone = OrderDone;
        }

        public String getBranchColor() {
            return BranchColor;
        }

        public void setBranchColor(String BranchColor) {
            this.BranchColor = BranchColor;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public String getIsCreditCardAllowed() {
            return IsCreditCardAllowed;
        }

        public void setIsCreditCardAllowed(String IsCreditCardAllowed) {
            this.IsCreditCardAllowed = IsCreditCardAllowed;
        }

        public String getIsCashAllowed() {
            return IsCashAllowed;
        }

        public void setIsCashAllowed(String IsCashAllowed) {
            this.IsCashAllowed = IsCashAllowed;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String DeliveryTime) {
            this.DeliveryTime = DeliveryTime;
        }

        public int getRatings() {
            return Ratings;
        }

        public void setRatings(int Ratings) {
            this.Ratings = Ratings;
        }

        public int getReviewsCount() {
            return ReviewsCount;
        }

        public void setReviewsCount(int ReviewsCount) {
            this.ReviewsCount = ReviewsCount;
        }

        public int getReviews() {
            return Reviews;
        }

        public void setReviews(int Reviews) {
            this.Reviews = Reviews;
        }

        public String getCategoryAr() {
            return CategoryAr;
        }

        public void setCategoryAr(String CategoryAr) {
            this.CategoryAr = CategoryAr;
        }

        public String getCategoryEn() {
            return CategoryEn;
        }

        public void setCategoryEn(String CategoryEn) {
            this.CategoryEn = CategoryEn;
        }

        public String getTermsAndConditionsAr() {
            return TermsAndConditionsAr;
        }

        public void setTermsAndConditionsAr(String TermsAndConditionsAr) {
            this.TermsAndConditionsAr = TermsAndConditionsAr;
        }

        public String getTermsAndConditionsEn() {
            return TermsAndConditionsEn;
        }

        public void setTermsAndConditionsEn(String TermsAndConditionsEn) {
            this.TermsAndConditionsEn = TermsAndConditionsEn;
        }

        public String getStoreImage() {
            return StoreImage;
        }

        public void setStoreImage(String StoreImage) {
            this.StoreImage = StoreImage;
        }

        public String getBranchLogoImage() {
            return BranchLogoImage;
        }

        public void setBranchLogoImage(String BranchLogoImage) {
            this.BranchLogoImage = BranchLogoImage;
        }

        public String getBackgroundImage() {
            return BackgroundImage;
        }

        public void setBackgroundImage(String BackgroundImage) {
            this.BackgroundImage = BackgroundImage;
        }

        public int getMinimumOrderValue() {
            return MinimumOrderValue;
        }

        public void setMinimumOrderValue(int MinimumOrderValue) {
            this.MinimumOrderValue = MinimumOrderValue;
        }

        public String getStoreAr() {
            return StoreAr;
        }

        public void setStoreAr(String StoreAr) {
            this.StoreAr = StoreAr;
        }

        public String getStoreEn() {
            return StoreEn;
        }

        public void setStoreEn(String StoreEn) {
            this.StoreEn = StoreEn;
        }

        public String getBranchAr() {
            return BranchAr;
        }

        public void setBranchAr(String BranchAr) {
            this.BranchAr = BranchAr;
        }

        public String getBranchEn() {
            return BranchEn;
        }

        public void setBranchEn(String BranchEn) {
            this.BranchEn = BranchEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getBranchStatus() {
            return BranchStatus;
        }

        public void setBranchStatus(String BranchStatus) {
            this.BranchStatus = BranchStatus;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public String getActualDate() {
            return ActualDate;
        }

        public void setActualDate(String ActualDate) {
            this.ActualDate = ActualDate;
        }
    }

    public static class ServiceList  implements Serializable  {
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("CountingNameAr")
        private String CountingNameAr;
        @Expose
        @SerializedName("CountingNameEn")
        private String CountingNameEn;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private String DiscountValue;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("ServiceType")
        private int ServiceType;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("ItemId")
        private String ItemId;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("FavoriteId")
        private int FavoriteId;

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getCountingNameAr() {
            return CountingNameAr;
        }

        public void setCountingNameAr(String CountingNameAr) {
            this.CountingNameAr = CountingNameAr;
        }

        public String getCountingNameEn() {
            return CountingNameEn;
        }

        public void setCountingNameEn(String CountingNameEn) {
            this.CountingNameEn = CountingNameEn;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public String getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(String DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getServiceType() {
            return ServiceType;
        }

        public void setServiceType(int ServiceType) {
            this.ServiceType = ServiceType;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public String getItemId() {
            return ItemId;
        }

        public void setItemId(String ItemId) {
            this.ItemId = ItemId;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getFavoriteId() {
            return FavoriteId;
        }

        public void setFavoriteId(int FavoriteId) {
            this.FavoriteId = FavoriteId;
        }
    }

    public static class ProductList  implements Serializable  {
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<JProductVariant> JProductVariant;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("Condition")
        private int Condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String UPCBarcode;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("DiscountValue")
        private float DiscountValue;
        @Expose
        @SerializedName("DiscountType")
        private int DiscountType;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int BranchSubCategoryId;
        @Expose
        @SerializedName("BranchCategoryId")
        private int BranchCategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("ItemId")
        private String ItemId;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("FavoriteId")
        private int FavoriteId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public ArrayList<JProductVariant> getJProductVariant() {
            return JProductVariant;
        }

        public void setJProductVariant(ArrayList<JProductVariant> JProductVariant) {
            this.JProductVariant = JProductVariant;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public int getCondition() {
            return Condition;
        }

        public void setCondition(int Condition) {
            this.Condition = Condition;
        }

        public String getUPCBarcode() {
            return UPCBarcode;
        }

        public void setUPCBarcode(String UPCBarcode) {
            this.UPCBarcode = UPCBarcode;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public float getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(float DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getDiscountType() {
            return DiscountType;
        }

        public void setDiscountType(int DiscountType) {
            this.DiscountType = DiscountType;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(int SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public int getBranchSubCategoryId() {
            return BranchSubCategoryId;
        }

        public void setBranchSubCategoryId(int BranchSubCategoryId) {
            this.BranchSubCategoryId = BranchSubCategoryId;
        }

        public int getBranchCategoryId() {
            return BranchCategoryId;
        }

        public void setBranchCategoryId(int BranchCategoryId) {
            this.BranchCategoryId = BranchCategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public String getItemId() {
            return ItemId;
        }

        public void setItemId(String ItemId) {
            this.ItemId = ItemId;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getFavoriteId() {
            return FavoriteId;
        }

        public void setFavoriteId(int FavoriteId) {
            this.FavoriteId = FavoriteId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class JProductVariant  implements Serializable  {
        @Expose
        @SerializedName("ValueAr")
        private String ValueAr;
        @Expose
        @SerializedName("ValueEn")
        private String ValueEn;
        @Expose
        @SerializedName("OptionId")
        private int OptionId;
        @Expose
        @SerializedName("ProductInventoryId")
        private int ProductInventoryId;

        public String getValueAr() {
            return ValueAr;
        }

        public void setValueAr(String ValueAr) {
            this.ValueAr = ValueAr;
        }

        public String getValueEn() {
            return ValueEn;
        }

        public void setValueEn(String ValueEn) {
            this.ValueEn = ValueEn;
        }

        public int getOptionId() {
            return OptionId;
        }

        public void setOptionId(int OptionId) {
            this.OptionId = OptionId;
        }

        public int getProductInventoryId() {
            return ProductInventoryId;
        }

        public void setProductInventoryId(int ProductInventoryId) {
            this.ProductInventoryId = ProductInventoryId;
        }
    }
}
