package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterSubResponce implements Serializable {

    @Expose
    @SerializedName("Value")
    private String Value;
    @Expose
    @SerializedName("Name")
    private String Name;
    @Expose
    @SerializedName("TypeName")
    private String TypeName;
    @Expose
    @SerializedName("Type")
    private String Type;
    @Expose
    @SerializedName("CategoryId")
    private int CategoryId;
    @Expose
    @SerializedName("TypeId")
    private int TypeId;

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getTypeId() {
        return TypeId;
    }

    public void setTypeId(int typeId) {
        TypeId = typeId;
    }
}
