package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class OrderPendingResponce implements Serializable {
    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("TrackingDetails")
        private ArrayList<TrackingDetails> TrackingDetails;
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> Items;
        @Expose
        @SerializedName("RejectedItemsCount")
        private int RejectedItemsCount;
        @Expose
        @SerializedName("ItemsCount")
        private int ItemsCount;

        @Expose
        @SerializedName("Rating")
        private float Rating;

        public float getRating() {
            return Rating;
        }

        public void setRating(int rating) {
            Rating = rating;
        }

        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("TimeSlotId")
        private int TimeSlotId;
        @Expose
        @SerializedName("ReScheduleDate")
        private String ReScheduleDate;
        @Expose
        @SerializedName("AcceptedGrandTotal")
        private float AcceptedGrandTotal;
        @Expose
        @SerializedName("AcceptedDeliveryFee")
        private float AcceptedDeliveryFee;
        @Expose
        @SerializedName("AcceptedVAT")
        private float AcceptedVAT;
        @Expose
        @SerializedName("AcceptedTotal")
        private float AcceptedTotal;
        @Expose
        @SerializedName("GrandTotal")
        private float GrandTotal;
        @Expose
        @SerializedName("DeliveryFee")
        private float DeliveryFee;
        @Expose
        @SerializedName("VAT")
        private float VAT;
        @Expose
        @SerializedName("SubTotal")
        private float SubTotal;
        @Expose
        @SerializedName("ExpectingDelivery")
        private String ExpectingDelivery;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("AddressId")
        private int AddressId;
        @Expose
        @SerializedName("OrderStatus")
        private int OrderStatus;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("Comments")
        private String Comments;
        @Expose
        @SerializedName("UserAddress")
        private String UserAddress;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("OrderStatusEn")
        private String OrderStatusEn;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("BranchLogoImage")
        private String BranchLogoImage;
        @Expose
        @SerializedName("BackgroundImage")
        private String BackgroundImage;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;

        public ArrayList<TrackingDetails> getTrackingDetails() {
            return TrackingDetails;
        }

        public void setTrackingDetails(ArrayList<TrackingDetails> TrackingDetails) {
            this.TrackingDetails = TrackingDetails;
        }

        public ArrayList<Items> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Items> Items) {
            this.Items = Items;
        }

        public int getRejectedItemsCount() {
            return RejectedItemsCount;
        }

        public void setRejectedItemsCount(int RejectedItemsCount) {
            this.RejectedItemsCount = RejectedItemsCount;
        }

        public int getItemsCount() {
            return ItemsCount;
        }

        public void setItemsCount(int ItemsCount) {
            this.ItemsCount = ItemsCount;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getTimeSlotId() {
            return TimeSlotId;
        }

        public void setTimeSlotId(int TimeSlotId) {
            this.TimeSlotId = TimeSlotId;
        }

        public String getReScheduleDate() {
            return ReScheduleDate;
        }

        public void setReScheduleDate(String ReScheduleDate) {
            this.ReScheduleDate = ReScheduleDate;
        }

        public float getAcceptedGrandTotal() {
            return AcceptedGrandTotal;
        }

        public void setAcceptedGrandTotal(int AcceptedGrandTotal) {
            this.AcceptedGrandTotal = AcceptedGrandTotal;
        }

        public float getAcceptedDeliveryFee() {
            return AcceptedDeliveryFee;
        }

        public void setAcceptedDeliveryFee(int AcceptedDeliveryFee) {
            this.AcceptedDeliveryFee = AcceptedDeliveryFee;
        }

        public float getAcceptedVAT() {
            return AcceptedVAT;
        }

        public void setAcceptedVAT(int AcceptedVAT) {
            this.AcceptedVAT = AcceptedVAT;
        }

        public float getAcceptedTotal() {
            return AcceptedTotal;
        }

        public void setAcceptedTotal(int AcceptedTotal) {
            this.AcceptedTotal = AcceptedTotal;
        }

        public float getGrandTotal() {
            return GrandTotal;
        }

        public void setGrandTotal(int GrandTotal) {
            this.GrandTotal = GrandTotal;
        }

        public float getDeliveryFee() {
            return DeliveryFee;
        }

        public void setDeliveryFee(int DeliveryFee) {
            this.DeliveryFee = DeliveryFee;
        }

        public float getVAT() {
            return VAT;
        }

        public void setVAT(int VAT) {
            this.VAT = VAT;
        }

        public float getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(int SubTotal) {
            this.SubTotal = SubTotal;
        }

        public String getExpectingDelivery() {
            return ExpectingDelivery;
        }

        public void setExpectingDelivery(String ExpectingDelivery) {
            this.ExpectingDelivery = ExpectingDelivery;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public int getAddressId() {
            return AddressId;
        }

        public void setAddressId(int AddressId) {
            this.AddressId = AddressId;
        }

        public int getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(int OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getComments() {
            return Comments;
        }

        public void setComments(String Comments) {
            this.Comments = Comments;
        }

        public String getUserAddress() {
            return UserAddress;
        }

        public void setUserAddress(String UserAddress) {
            this.UserAddress = UserAddress;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getOrderStatusEn() {
            return OrderStatusEn;
        }

        public void setOrderStatusEn(String OrderStatusEn) {
            this.OrderStatusEn = OrderStatusEn;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getBranchLogoImage() {
            return BranchLogoImage;
        }

        public void setBranchLogoImage(String BranchLogoImage) {
            this.BranchLogoImage = BranchLogoImage;
        }

        public String getBackgroundImage() {
            return BackgroundImage;
        }

        public void setBackgroundImage(String BackgroundImage) {
            this.BackgroundImage = BackgroundImage;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }
    }

    public static class TrackingDetails implements Serializable {
        @Expose
        @SerializedName("Message")
        private String Message;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Items  implements Serializable {
        @Expose
        @SerializedName("Variants")
        private ArrayList<Variants> Variants;
        @Expose
        @SerializedName("NotReturnable")
        private boolean NotReturnable;
        @Expose
        @SerializedName("Reason")
        private String Reason;
        @Expose
        @SerializedName("Manufacturer")
        private String Manufacturer;
        @Expose
        @SerializedName("IsAccepted")
        private boolean IsAccepted;
        @Expose
        @SerializedName("GrandTotal")
        private float GrandTotal;
        @Expose
        @SerializedName("VAT")
        private float VAT;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("Qty")
        private int Qty;
        @Expose
        @SerializedName("SKUId")
        private String SKUId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("OrderItemId")
        private int OrderItemId;
        @Expose
        @SerializedName("Rating")
        private int Rating;

        public int getRating() {
            return Rating;
        }

        public void setRating(int rating) {
            Rating = rating;
        }

        public ArrayList<Variants> getVariants() {
            return Variants;
        }

        public void setVariants(ArrayList<Variants> Variants) {
            this.Variants = Variants;
        }

        public boolean getNotReturnable() {
            return NotReturnable;
        }

        public void setNotReturnable(boolean NotReturnable) {
            this.NotReturnable = NotReturnable;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String Reason) {
            this.Reason = Reason;
        }

        public String getManufacturer() {
            return Manufacturer;
        }

        public void setManufacturer(String Manufacturer) {
            this.Manufacturer = Manufacturer;
        }

        public boolean getIsAccepted() {
            return IsAccepted;
        }

        public void setIsAccepted(boolean IsAccepted) {
            this.IsAccepted = IsAccepted;
        }

        public float getGrandTotal() {
            return GrandTotal;
        }

        public void setGrandTotal(int GrandTotal) {
            this.GrandTotal = GrandTotal;
        }

        public float getVAT() {
            return VAT;
        }

        public void setVAT(float VAT) {
            this.VAT = VAT;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public int getQty() {
            return Qty;
        }

        public void setQty(int Qty) {
            this.Qty = Qty;
        }

        public String getSKUId() {
            return SKUId;
        }

        public void setSKUId(String SKUId) {
            this.SKUId = SKUId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getOrderItemId() {
            return OrderItemId;
        }

        public void setOrderItemId(int OrderItemId) {
            this.OrderItemId = OrderItemId;
        }
    }

    public static class Variants implements Serializable {
        @Expose
        @SerializedName("OptionValueAr")
        private String OptionValueAr;
        @Expose
        @SerializedName("OptionValueEn")
        private String OptionValueEn;
        @Expose
        @SerializedName("OptionAr")
        private String OptionAr;
        @Expose
        @SerializedName("OptionEn")
        private String OptionEn;

        public String getOptionValueAr() {
            return OptionValueAr;
        }

        public void setOptionValueAr(String OptionValueAr) {
            this.OptionValueAr = OptionValueAr;
        }

        public String getOptionValueEn() {
            return OptionValueEn;
        }

        public void setOptionValueEn(String OptionValueEn) {
            this.OptionValueEn = OptionValueEn;
        }

        public String getOptionAr() {
            return OptionAr;
        }

        public void setOptionAr(String OptionAr) {
            this.OptionAr = OptionAr;
        }

        public String getOptionEn() {
            return OptionEn;
        }

        public void setOptionEn(String OptionEn) {
            this.OptionEn = OptionEn;
        }
    }
}
