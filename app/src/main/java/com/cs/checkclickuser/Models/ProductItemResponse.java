package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductItemResponse {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("BranchSubCategoryId")
    private int branchsubcategoryid;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getBranchsubcategoryid() {
        return branchsubcategoryid;
    }

    public void setBranchsubcategoryid(int branchsubcategoryid) {
        this.branchsubcategoryid = branchsubcategoryid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("TotalRecords")
        private ArrayList<TotalRecords> totalrecords;
        @Expose
        @SerializedName("ProductList")
        private ArrayList<ProductList> productlist;

        public ArrayList<TotalRecords> getTotalrecords() {
            return totalrecords;
        }

        public void setTotalrecords(ArrayList<TotalRecords> totalrecords) {
            this.totalrecords = totalrecords;
        }

        public ArrayList<ProductList> getProductlist() {
            return productlist;
        }

        public void setProductlist(ArrayList<ProductList> productlist) {
            this.productlist = productlist;
        }
    }

    public static class TotalRecords {
        @Expose
        @SerializedName("TotalRecords")
        private int totalrecords;

        public int getTotalrecords() {
            return totalrecords;
        }

        public void setTotalrecords(int totalrecords) {
            this.totalrecords = totalrecords;
        }
    }

    public static class ProductList {
        @Expose
        @SerializedName("Condition")
        private int condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String upcbarcode;
        @Expose
        @SerializedName("ProductSkuId")
        private String productskuid;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("StockQuantity")
        private int stockquantity;
        @Expose
        @SerializedName("SellingPrice")
        private int sellingprice;
        @Expose
        @SerializedName("Price")
        private int price;
        @Expose
        @SerializedName("DiscountValue")
        private int discountvalue;
        @Expose
        @SerializedName("DiscountType")
        private int discounttype;
        @Expose
        @SerializedName("Image")
        private String image;
        @Expose
        @SerializedName("ProductNameAr")
        private String productnamear;
        @Expose
        @SerializedName("ProductNameEn")
        private String productnameen;
        @Expose
        @SerializedName("ProductId")
        private int productid;
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("rowNum")
        private int rownum;

        public int getCondition() {
            return condition;
        }

        public void setCondition(int condition) {
            this.condition = condition;
        }

        public String getUpcbarcode() {
            return upcbarcode;
        }

        public void setUpcbarcode(String upcbarcode) {
            this.upcbarcode = upcbarcode;
        }

        public String getProductskuid() {
            return productskuid;
        }

        public void setProductskuid(String productskuid) {
            this.productskuid = productskuid;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getStockquantity() {
            return stockquantity;
        }

        public void setStockquantity(int stockquantity) {
            this.stockquantity = stockquantity;
        }

        public int getSellingprice() {
            return sellingprice;
        }

        public void setSellingprice(int sellingprice) {
            this.sellingprice = sellingprice;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getDiscountvalue() {
            return discountvalue;
        }

        public void setDiscountvalue(int discountvalue) {
            this.discountvalue = discountvalue;
        }

        public int getDiscounttype() {
            return discounttype;
        }

        public void setDiscounttype(int discounttype) {
            this.discounttype = discounttype;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getProductnamear() {
            return productnamear;
        }

        public void setProductnamear(String productnamear) {
            this.productnamear = productnamear;
        }

        public String getProductnameen() {
            return productnameen;
        }

        public void setProductnameen(String productnameen) {
            this.productnameen = productnameen;
        }

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getRownum() {
            return rownum;
        }

        public void setRownum(int rownum) {
            this.rownum = rownum;
        }
    }
}
