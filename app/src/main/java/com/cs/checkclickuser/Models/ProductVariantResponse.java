package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductVariantResponse implements Serializable {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("ProductInventoryId")
    private int productinventoryid;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getProductinventoryid() {
        return productinventoryid;
    }

    public void setProductinventoryid(int productinventoryid) {
        this.productinventoryid = productinventoryid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("VariantsList")
        private ArrayList<VariantsList> variantslist;
        @Expose
        @SerializedName("ProductList")
        private ArrayList<ProductList> productlist;

        public ArrayList<VariantsList> getVariantslist() {
            return variantslist;
        }

        public void setVariantslist(ArrayList<VariantsList> variantslist) {
            this.variantslist = variantslist;
        }

        public ArrayList<ProductList> getProductlist() {
            return productlist;
        }

        public void setProductlist(ArrayList<ProductList> productlist) {
            this.productlist = productlist;
        }
    }

    public static class VariantsList implements Serializable {
        @Expose
        @SerializedName("jvariants")
        private ArrayList<Jvariants> jvariants;
        @Expose
        @SerializedName("variants")
        private String variants;
        @Expose
        @SerializedName("VariantNameEn")
        private String variantnameen;
        @Expose
        @SerializedName("VariantNameAr")
        private String variantnamear;
        @Expose
        @SerializedName("OptionId")
        private int optionid;

        public ArrayList<Jvariants> getJvariants() {
            return jvariants;
        }

        public void setJvariants(ArrayList<Jvariants> jvariants) {
            this.jvariants = jvariants;
        }

        public String getVariants() {
            return variants;
        }

        public void setVariants(String variants) {
            this.variants = variants;
        }

        public String getVariantnameen() {
            return variantnameen;
        }

        public void setVariantnameen(String variantnameen) {
            this.variantnameen = variantnameen;
        }

        public String getVariantnamear() {
            return variantnamear;
        }

        public void setVariantnamear(String variantnamear) {
            this.variantnamear = variantnamear;
        }

        public int getOptionid() {
            return optionid;
        }

        public void setOptionid(int optionid) {
            this.optionid = optionid;
        }
    }

    public static class Jvariants implements Serializable{
        @Expose
        @SerializedName("ValueAr")
        private String valuear;
        @Expose
        @SerializedName("ValueEn")
        private String valueen;
        @Expose
        @SerializedName("OptionId")
        private int optionid;

        public String getValuear() {
            return valuear;
        }

        public void setValuear(String valuear) {
            this.valuear = valuear;
        }

        public String getValueen() {
            return valueen;
        }

        public void setValueen(String valueen) {
            this.valueen = valueen;
        }

        public int getOptionid() {
            return optionid;
        }

        public void setOptionid(int optionid) {
            this.optionid = optionid;
        }
    }

    public static class ProductList implements Serializable {
        @Expose
        @SerializedName("JRelatedProducts")
        private ArrayList<String> jrelatedproducts;
        @Expose
        @SerializedName("JReviews")
        private ArrayList<String> jreviews;
        @Expose
        @SerializedName("JImages")
        private ArrayList<JImages> jimages;
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<JProductVariant> jproductvariant;
        @Expose
        @SerializedName("OrderId")
        private int orderid;
        @Expose
        @SerializedName("UserReviews")
        private int userreviews;
        @Expose
        @SerializedName("PurchaseCount")
        private int purchasecount;
        @Expose
        @SerializedName("RelatedProducts")
        private String relatedproducts;
        @Expose
        @SerializedName("Reviews")
        private String reviews;
        @Expose
        @SerializedName("DescriptionAr")
        private String descriptionar;
        @Expose
        @SerializedName("DescriptionEn")
        private String descriptionen;
        @Expose
        @SerializedName("AvailableQuantity")
        private int availablequantity;
        @Expose
        @SerializedName("CartQuantity")
        private int cartquantity;
        @Expose
        @SerializedName("ProductCountingNo")
        private int productcountingno;
        @Expose
        @SerializedName("Condition")
        private int condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String upcbarcode;
        @Expose
        @SerializedName("SellingPrice")
        private float sellingprice;
        @Expose
        @SerializedName("Price")
        private float price;
        @Expose
        @SerializedName("DiscountValue")
        private int discountvalue;
        @Expose
        @SerializedName("DiscountType")
        private int discounttype;
        @Expose
        @SerializedName("Images")
        private String images;
        @Expose
        @SerializedName("ProductVariants")
        private String productvariants;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean isvatapplicable;
        @Expose
        @SerializedName("ProductNameAr")
        private String productnamear;
        @Expose
        @SerializedName("ProductNameEn")
        private String productnameen;
        @Expose
        @SerializedName("NotReturnable")
        private boolean notreturnable;
        @Expose
        @SerializedName("AllowedQty")
        private int allowedqty;
        @Expose
        @SerializedName("MaxCartQty")
        private int maxcartqty;
        @Expose
        @SerializedName("MinCartQty")
        private int mincartqty;
        @Expose
        @SerializedName("ProductAvailabilityRange")
        private int productavailabilityrange;
        @Expose
        @SerializedName("LowStockActivity")
        private int lowstockactivity;
        @Expose
        @SerializedName("DisplayStockQuantity")
        private boolean displaystockquantity;
        @Expose
        @SerializedName("ReservedQuantity")
        private int reservedquantity;
        @Expose
        @SerializedName("StockQuantity")
        private int stockquantity;
        @Expose
        @SerializedName("InventoryMethod")
        private int inventorymethod;
        @Expose
        @SerializedName("ProductSkuId")
        private String productskuid;
        @Expose
        @SerializedName("ProductId")
        private int productid;
        @Expose
        @SerializedName("ProductInventoryId")
        private int productinventoryid;

        public ArrayList<String> getJrelatedproducts() {
            return jrelatedproducts;
        }

        public void setJrelatedproducts(ArrayList<String> jrelatedproducts) {
            this.jrelatedproducts = jrelatedproducts;
        }

        public ArrayList<String> getJreviews() {
            return jreviews;
        }

        public void setJreviews(ArrayList<String> jreviews) {
            this.jreviews = jreviews;
        }

        public ArrayList<JImages> getJimages() {
            return jimages;
        }

        public void setJimages(ArrayList<JImages> jimages) {
            this.jimages = jimages;
        }

        public ArrayList<JProductVariant> getJproductvariant() {
            return jproductvariant;
        }

        public void setJproductvariant(ArrayList<JProductVariant> jproductvariant) {
            this.jproductvariant = jproductvariant;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public int getUserreviews() {
            return userreviews;
        }

        public void setUserreviews(int userreviews) {
            this.userreviews = userreviews;
        }

        public int getPurchasecount() {
            return purchasecount;
        }

        public void setPurchasecount(int purchasecount) {
            this.purchasecount = purchasecount;
        }

        public String getRelatedproducts() {
            return relatedproducts;
        }

        public void setRelatedproducts(String relatedproducts) {
            this.relatedproducts = relatedproducts;
        }

        public String getReviews() {
            return reviews;
        }

        public void setReviews(String reviews) {
            this.reviews = reviews;
        }

        public String getDescriptionar() {
            return descriptionar;
        }

        public void setDescriptionar(String descriptionar) {
            this.descriptionar = descriptionar;
        }

        public String getDescriptionen() {
            return descriptionen;
        }

        public void setDescriptionen(String descriptionen) {
            this.descriptionen = descriptionen;
        }

        public int getAvailablequantity() {
            return availablequantity;
        }

        public void setAvailablequantity(int availablequantity) {
            this.availablequantity = availablequantity;
        }

        public int getCartquantity() {
            return cartquantity;
        }

        public void setCartquantity(int cartquantity) {
            this.cartquantity = cartquantity;
        }

        public int getProductcountingno() {
            return productcountingno;
        }

        public void setProductcountingno(int productcountingno) {
            this.productcountingno = productcountingno;
        }

        public int getCondition() {
            return condition;
        }

        public void setCondition(int condition) {
            this.condition = condition;
        }

        public String getUpcbarcode() {
            return upcbarcode;
        }

        public void setUpcbarcode(String upcbarcode) {
            this.upcbarcode = upcbarcode;
        }

        public float getSellingprice() {
            return sellingprice;
        }

        public void setSellingprice(float sellingprice) {
            this.sellingprice = sellingprice;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public int getDiscountvalue() {
            return discountvalue;
        }

        public void setDiscountvalue(int discountvalue) {
            this.discountvalue = discountvalue;
        }

        public int getDiscounttype() {
            return discounttype;
        }

        public void setDiscounttype(int discounttype) {
            this.discounttype = discounttype;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getProductvariants() {
            return productvariants;
        }

        public void setProductvariants(String productvariants) {
            this.productvariants = productvariants;
        }

        public boolean getIsvatapplicable() {
            return isvatapplicable;
        }

        public void setIsvatapplicable(boolean isvatapplicable) {
            this.isvatapplicable = isvatapplicable;
        }

        public String getProductnamear() {
            return productnamear;
        }

        public void setProductnamear(String productnamear) {
            this.productnamear = productnamear;
        }

        public String getProductnameen() {
            return productnameen;
        }

        public void setProductnameen(String productnameen) {
            this.productnameen = productnameen;
        }

        public boolean getNotreturnable() {
            return notreturnable;
        }

        public void setNotreturnable(boolean notreturnable) {
            this.notreturnable = notreturnable;
        }

        public int getAllowedqty() {
            return allowedqty;
        }

        public void setAllowedqty(int allowedqty) {
            this.allowedqty = allowedqty;
        }

        public int getMaxcartqty() {
            return maxcartqty;
        }

        public void setMaxcartqty(int maxcartqty) {
            this.maxcartqty = maxcartqty;
        }

        public int getMincartqty() {
            return mincartqty;
        }

        public void setMincartqty(int mincartqty) {
            this.mincartqty = mincartqty;
        }

        public int getProductavailabilityrange() {
            return productavailabilityrange;
        }

        public void setProductavailabilityrange(int productavailabilityrange) {
            this.productavailabilityrange = productavailabilityrange;
        }

        public int getLowstockactivity() {
            return lowstockactivity;
        }

        public void setLowstockactivity(int lowstockactivity) {
            this.lowstockactivity = lowstockactivity;
        }

        public boolean getDisplaystockquantity() {
            return displaystockquantity;
        }

        public void setDisplaystockquantity(boolean displaystockquantity) {
            this.displaystockquantity = displaystockquantity;
        }

        public int getReservedquantity() {
            return reservedquantity;
        }

        public void setReservedquantity(int reservedquantity) {
            this.reservedquantity = reservedquantity;
        }

        public int getStockquantity() {
            return stockquantity;
        }

        public void setStockquantity(int stockquantity) {
            this.stockquantity = stockquantity;
        }

        public int getInventorymethod() {
            return inventorymethod;
        }

        public void setInventorymethod(int inventorymethod) {
            this.inventorymethod = inventorymethod;
        }

        public String getProductskuid() {
            return productskuid;
        }

        public void setProductskuid(String productskuid) {
            this.productskuid = productskuid;
        }

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public int getProductinventoryid() {
            return productinventoryid;
        }

        public void setProductinventoryid(int productinventoryid) {
            this.productinventoryid = productinventoryid;
        }
    }

    public static class JImages implements Serializable {
        @Expose
        @SerializedName("ImageId")
        private int imageid;
        @Expose
        @SerializedName("Image")
        private String image;
        @Expose
        @SerializedName("ProductInventoryId")
        private int productinventoryid;

        public int getImageid() {
            return imageid;
        }

        public void setImageid(int imageid) {
            this.imageid = imageid;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getProductinventoryid() {
            return productinventoryid;
        }

        public void setProductinventoryid(int productinventoryid) {
            this.productinventoryid = productinventoryid;
        }
    }

    public static class JProductVariant implements Serializable {
        @Expose
        @SerializedName("ValueAr")
        private String valuear;
        @Expose
        @SerializedName("ValueEn")
        private String valueen;
        @Expose
        @SerializedName("OptionId")
        private int optionid;
        @Expose
        @SerializedName("ProductInventoryId")
        private int productinventoryid;

        public String getValuear() {
            return valuear;
        }

        public void setValuear(String valuear) {
            this.valuear = valuear;
        }

        public String getValueen() {
            return valueen;
        }

        public void setValueen(String valueen) {
            this.valueen = valueen;
        }

        public int getOptionid() {
            return optionid;
        }

        public void setOptionid(int optionid) {
            this.optionid = optionid;
        }

        public int getProductinventoryid() {
            return productinventoryid;
        }

        public void setProductinventoryid(int productinventoryid) {
            this.productinventoryid = productinventoryid;
        }
    }
}
