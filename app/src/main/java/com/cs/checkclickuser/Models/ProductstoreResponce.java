package com.cs.checkclickuser.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public  class ProductstoreResponce implements Serializable {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("Advertisements")
        private ArrayList<Advertisements> Advertisements;
        @Expose
        @SerializedName("SubCategory")
        private ArrayList<SubCategory> SubCategory;
        @Expose
        @SerializedName("MainCategory")
        private ArrayList<MainCategory> MainCategory;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public ArrayList<Advertisements> getAdvertisements() {
            return Advertisements;
        }

        public void setAdvertisements(ArrayList<Advertisements> Advertisements) {
            this.Advertisements = Advertisements;
        }

        public ArrayList<SubCategory> getSubCategory() {
            return SubCategory;
        }

        public void setSubCategory(ArrayList<SubCategory> SubCategory) {
            this.SubCategory = SubCategory;
        }

        public ArrayList<MainCategory> getMainCategory() {
            return MainCategory;
        }

        public void setMainCategory(ArrayList<MainCategory> MainCategory) {
            this.MainCategory = MainCategory;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class Advertisements implements Serializable{
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int BranchSubCategoryId;
        @Expose
        @SerializedName("BranchCategoryId")
        private int BranchCategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("DiscountPrice")
        private int DiscountPrice;
        @Expose
        @SerializedName("Discount")
        private int Discount;
        @Expose
        @SerializedName("Price")
        private int Price;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("Percentage")
        private int Percentage;
        @Expose
        @SerializedName("BannerImageAr")
        private String BannerImageAr;
        @Expose
        @SerializedName("BannerImageEn")
        private String BannerImageEn;
        @Expose
        @SerializedName("OfferType")
        private int OfferType;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getBranchSubCategoryId() {
            return BranchSubCategoryId;
        }

        public void setBranchSubCategoryId(int BranchSubCategoryId) {
            this.BranchSubCategoryId = BranchSubCategoryId;
        }

        public int getBranchCategoryId() {
            return BranchCategoryId;
        }

        public void setBranchCategoryId(int BranchCategoryId) {
            this.BranchCategoryId = BranchCategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getDiscountPrice() {
            return DiscountPrice;
        }

        public void setDiscountPrice(int DiscountPrice) {
            this.DiscountPrice = DiscountPrice;
        }

        public int getDiscount() {
            return Discount;
        }

        public void setDiscount(int Discount) {
            this.Discount = Discount;
        }

        public int getPrice() {
            return Price;
        }

        public void setPrice(int Price) {
            this.Price = Price;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getPercentage() {
            return Percentage;
        }

        public void setPercentage(int Percentage) {
            this.Percentage = Percentage;
        }

        public String getBannerImageAr() {
            return BannerImageAr;
        }

        public void setBannerImageAr(String BannerImageAr) {
            this.BannerImageAr = BannerImageAr;
        }

        public String getBannerImageEn() {
            return BannerImageEn;
        }

        public void setBannerImageEn(String BannerImageEn) {
            this.BannerImageEn = BannerImageEn;
        }

        public int getOfferType() {
            return OfferType;
        }

        public void setOfferType(int OfferType) {
            this.OfferType = OfferType;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class SubCategory implements Serializable {
        @Expose
        @SerializedName("ProductCount")
        private int ProductCount;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getProductCount() {
            return ProductCount;
        }

        public void setProductCount(int ProductCount) {
            this.ProductCount = ProductCount;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class MainCategory implements Serializable{
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
