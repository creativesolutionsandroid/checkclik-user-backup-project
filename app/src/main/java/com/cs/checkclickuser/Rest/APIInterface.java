package com.cs.checkclickuser.Rest;


import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.Catresponce;
import com.cs.checkclickuser.Models.ChangeEmailRespomce;
import com.cs.checkclickuser.Models.ChangePasswordResponce;
import com.cs.checkclickuser.Models.CouponResponce;
import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.Models.PushNotificationResponce;
import com.cs.checkclickuser.Models.ResetpasswordResponce;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.Models.Signinresponce;
import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.Models.VerifyEmailResponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;


import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("api/CustomerRegistrationAPI/NewGetMobileOTP")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUserRegistration")
    Call<Signupresponse> userRegistration(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUserValidation")
    Call<Signinresponce> userLogin(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewVerifyEmail")
    Call<VerifyEmailResponse> VerifyEmail (@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewResetPassword")
    Call<ResetpasswordResponce> resetPassword(@Body RequestBody body);


    @POST("api/CustomerAPI/NewGetMainCategoryAndStore")
    Call<ProductlistResponce> getstorslist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetMainCategoryAndStore")
    Call<Catresponce> catlist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetBranchMainSubCatgory")
    Call<ProductstoreResponce> getstorelist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetStoreBranchDeals")
    Call<DealsResponce> dealsresponce(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetHomePageDetails")
    Call<HomePageResponse> getHomePageDetails(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewResetPassword")
    Call<ChangePasswordResponce> getchagepassword(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetDeleteUserFavorite")
    Call<FavStoresResponce> getfavorites(@Body RequestBody body);

    @POST("api/CustomerAPI/NewAddUpdateCustomerAddress")
    Call<ManageAdressResponce> getaddress(@Body RequestBody body);
    @POST("api/CustomerRegistrationAPI/GetUpdatePNConfiguration")
    Call<PushNotificationResponce> getpushnotification(@Body RequestBody body);

    @POST("api/VoucherManagementAPI/GetCoupons")
    Call<CouponResponce> getcoupons(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsListForCustomers")
    Call<ProductItemResponse> getProductList(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsDetailsForCustomers")
    Call<ProductVariantResponse> getProductVariants(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/UserHistoryOrders")
    Call<OrderPendingResponce> getorderpending(@Body RequestBody body);

  @POST("api/OrdersAPI/NewAddGetUserReviews")
    Call<ReviewResponce> getreview(@Body RequestBody body);

  @POST("api/OrdersAPI/NewCancelOrder")
    Call<OrderCancelResponce> getcancl(@Body RequestBody body);

 @POST("api/OrdersAPI/NewOrderPartialAccept")
    Call<OrderCancelResponce> getaccepet(@Body RequestBody body);

  @POST("api/CountryAPI/NewGetCountryCity")
    Call<AdressCountryResponce> getcounrylist();

    @POST("api/ProductsInfoAPI/NewGetCartItems")
    Call<CartResponce> getcart(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUpdateEmail")
    Call<ChangeEmailRespomce> getchangeemail(@Body RequestBody body);




}
